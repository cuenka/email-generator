eLab Apps
========================
This App requires php7+ MySQl5.7+ and basic php modules, an updated version of npm

Repository: 
   * git@bitbucket.org:jc-aswatson/email-generator.git

Fresh install
--------------

Once the repo is downloaded you need to install the vendors and set up your config,
execute following commands on the root of the project, **your php command, for instance php7**, 
It may change depending of OS and config:

    *   php7 composer.phar install
 

If you do not have DB created you can created with the 2 folowing comands:

    *   php7 bin/console doctrine:database:create
    *   php7 bin/console doctrine:schema:create

Update code
-----------
First you download latest changes
    *   git pull
    
Update vendors and new config
    
    *   php7 composer.phar install
    
Update schema or DB tables.    

    *   php7 bin/console doctrine:schema:update --force


Clear cache
-----------
    *   php7 c:c
    
    
To see if everything is working the webroot is: web/app_dev.php

SCSS AND JS
-----------
Files can be found in app/Resources
to install dependencies:
    
    *   npm install

Gulp is is charge of creating a latest version and compiles files css and js with all libraries and vendors.

JS files are auto discovered so if you add a new file will be added automatically.

If you add a new scss file you will need to import it on universal.scss otherwise will be ignore.

For compile use:

    *   gulp default

for watch use:

    *   gulp watch

CKeditor is not working
-----------------------

It this happen most probably need to install it locally.
execute this 2 commands:
   
    *   php7 bin/console ckeditor:install
    *   php7 bin/console assets:install web



Enjoy!

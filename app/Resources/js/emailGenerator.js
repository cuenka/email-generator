$(document).ready(function () {
    // VALIDATE UTM SO FIRST CHARACTER IS ALWAYS ?
    $('#emailgeneratorbundle_email_utm').on("keyup", function(e){
        if ($(this).val().substr(0, 1) != '?') {
            $(this).val(null);
            alert('UTM must start with ? character');
        }
    });
    // STYLE CTA SUBMIT
    $(document).find('[type="submit"]').prepend('<i class="fa fa-save"></i> ');

    // START SORTABLE jQueryUI on List Block page
    $(document).on("Refreshing#sort-enabled", function () {
        $("#blocks-selected").sortable();
    });
    $("#blocks-selected").sortable({
        stop: function () {
            updateBlock();
        }
    });

    // Submit content on page /email/block/list/{{id}} path="email_block_list"
    $(document).on("click", "#blocks-submit", function () {
        updateBlock();
    });

    var updateBlock = function () {
        var blocks = [];
        var $blocks = $('#blocks-selected').find('li');
        var editURL = $('#blocks-selected').data('url');
        $blocks.each(function (i, block) {
            blocks.push(
                {
                    'id': $(this).data('id'),
                    'order': i,
                    'name': $(this).data('block')
                });
        });
        $.ajax({
            type: "POST",
            url: editURL,
            data: {'data': blocks},
            context: $('body'),
            beforeSend: function (xhr) {
                $('#blocks-submit').attr('disabled', true);
                $('#blocks-submit').html('<i class="fa fa-cog fa-spin" aria-hidden="true"></i> Saving...');
                $("#blocks-selected").sortable("disable");

            }
        }).done(function (data) {
            $('#blocks-go-block-page').removeClass('hidden');
            $('#blocks-submit').html('<i class="fa fa-cog fa-spin" aria-hidden="true"></i> Refresing page...');
            location.reload();
        }).fail(function (data) {
            $('#blocks-submit').attr('disabled', false);
            $('#blocks-submit').html(' Update');
        });
    }
    // END



    // Add a block on the right side part or bellow
    $(document).on("click", ".block-add", function () {
        var $blockSelected = $('#blocks-selected');
        var block = $(this).data('block');
        var img = $(this).data('img');
        var html = '<li class="list-group-item" data-block="' + block + '"><img src="' + img + '" class="img-fluid" style="max-width: calc(100% - 40px);">' +
            '<span class="pull-right">' +
            '<button class="btn btn-sm btn-danger block-remove ml-1 mb-2 btn-block"><i class="fa fa-close"></i></button>' +
            '</span></li>';
        $blockSelected.append(html);
        //Re-initialise sort
        $("#blocks-selected").sortable();

    });

    // Remove a selected block
    $(document).on("click", ".block-remove", function () {
        var $block = $(this).parents('li.list-group-item');
        if ($block.data('delete')) {
            $.ajax({
                url: $block.data('delete'),
                beforeSend: function (xhr) {
                    $(this).attr('disabled', true);
                }
            }).done(function (data) {
            }).fail(function (data) {
                $(this).attr('disabled', false);
            });
        }

        $block.remove();
        //Re-initialise sort
        $("#blocks-selected").sortable();
    });

    $(document).on("click", ".edit-block-form", function () {
        var blockId = $(this).data('block');
        $('.block-form').hide();
        $('#' + blockId).show();
    });

    $(document).on("click", ".block-edit", function () {
        $.ajax({
            url: $(this).data('url'),
            context: $('.modal-body'),
            beforeSend: function (xhr) {
                $(this).html('<div class="text-center"><i class="fa fa-refresh fa-spin fa-4x fa-fw"></i><br><span>Loading...</span></div>');
            }
        }).done(function (data) {
            $(this).html(data);
            // Reset select2
            $("select.select2").select2({
                tags: true
            });
        }).fail(function (data) {
            var html = '<div class="alert alert-danger" role="alert"><strong>ERROR!</strong> ' + data.message + '</div>';
            $(this).html(html).fadeIn(500, 0).delay(5000).fadeOut(500);
        });
    });

});

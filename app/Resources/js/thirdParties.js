$(document).ready(function () {
    //WHEELCOLORPICKER
    $(".pickAColor").wheelColorPicker({
        format: 'css',
        preview: true
    });

    //BOOTSTRAP
    $(".btn").popover();
    $("#sortable").sortable();
    $("#sortable").disableSelection();

    //DATATABLES
    $('table.table').DataTable({
            "pageLength": 25,
        "aoColumns": [
            { "bSearchable": false },
            null,
            null,
            null,
            null,
            { "bSearchable": false }
        ]}
    );

    //DATEPICKER
    $(".js-datepicker").datepicker(
        {
            dateFormat: 'dd-mm-yy'
        }
    );
});
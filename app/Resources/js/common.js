$(document).ready(function () {
    $("#uploadToCdn").on("click", function () {
        $.ajax({
            url: $(this).data('url'),
            context: document.body
        }).done(function (data) {
            $('.ajax-response').text(data.data).parent().show();
        });
    });

    $("#showJson").on("click", function () {
        var text = $(this).text();

        if (~text.indexOf("Show")) {
            $(this).text("Hide JSON FILE");
        }
        if (~text.indexOf("Hide")) {
            $(this).text("Show JSON FILE");
        }
    });

    $('input,textarea,select').filter('[required]:visible').siblings('label').append(' *');
});
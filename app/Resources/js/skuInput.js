// Using the module pattern for a jQuery feature
$( document ).ready(function() {

    var skuInput = (function() {
        var $product1 = $( ".product1" );
        var $product2 = $( ".product2" );
        var $product3 = $( ".product3" );
        var dataContext =

        //SKU SECTION
        $(document).on("click", ".sku-discover", function (e) {
            e.preventDefault();
            var $btn = $(this);
            var input = $(document).find('.' + $(this).data('input')).val();
            var emailId = $(document).find('.' + $(this).data('input')).data('email');
            var blockId = $(document).find('.' + $(this).data('input')).data('block');
            var dataTarget = $(this).data('target');
            var $dataTargetBody =  $(document).find(dataTarget + ' .modal-body');
            dataContext = $(document).find('.' + $(this).data('input')).data('context');

            var endpoint = $(this).data('endpoint');
            if ( typeof(input) === "undefined" || input === '' ) {
                $dataTargetBody.html('<span class="text-danger">Product SKU input is empty, please type SKU!</span>');
            } else {
                $.ajax({
                    url: endpoint,
                    method: "POST",
                    async: true,
                    cache: false,
                    context: $dataTargetBody,
                    data: {'skuId': input, 'emailID': emailId, 'blockID': blockId, 'dataContext': dataContext},
                    beforeSend: function (xhr) {
                        $btn.addClass("disabled");
                        $(this).html('<div class="text-center"><i class="fa fa-refresh fa-spin fa-4x fa-fw"></i><br><span>Loading...</span></div>');
                    },
                    success: function (data) {
                        $(this).html(data);
                        $btn.removeClass("disabled");
                    },
                    error: function (data) {
                        $btn.removeClass("disabled");
                        $dataTargetBody.html('<div class="alert alert-danger" role="alert"><strong>ERROR!</strong> I could not find that product, are you sure is the right ID? <br>Close and try again</div>');
                    }
                });
            }
        });


        $(document).on("click", ".saveSKU", function () {
            var $dataTargetBody =  $(document).find($(this).data('modal'));
            var dataContext = $(this).data('context');
            var dataBlockID = $(this).data('block');
            var $block = $('#block-' + dataBlockID);
            console.log(dataContext);
            console.log($block);
            $block.find('.' + dataContext + 'Brand').val(($dataTargetBody.find('.skuProductBrand').val()));
            $block.find('.' + dataContext + 'Name').val(($dataTargetBody.find('.skuProductName').val()));
            $block.find('.' + dataContext + 'Image').val(($dataTargetBody.find('.skuProductImage').val()));
            $block.find('.' + dataContext + 'Price').val(($dataTargetBody.find('.skuProductPrice').val()));
            $block.find('.' + dataContext + 'PromoPrice').val(($dataTargetBody.find('.skuProductOldPrice').val()));
            showContext(dataContext, $block);
        });

        var showContext = function (dataContext, $block) {
            switch (dataContext) {
                case 'product1':
                    $block.find('.product1Brand').parent().slideDown();
                    $block.find('.product1Name').parent().slideDown();
                    $block.find('.product1Image').parent().slideDown();
                    $block.find('.product1Price').parent().slideDown();
                    $block.find('.product1PromoPrice').parent().slideDown();
                    break;
                case 'product2':
                    $block.find('.product2Brand').parent().slideDown();
                    $block.find('.product2Name').parent().slideDown();
                    $block.find('.product2Image').parent().slideDown();
                    $block.find('.product2Price').parent().slideDown();
                    $block.find('.product2PromoPrice').parent().slideDown();
                    break;
                case 'product3':
                    $block.find('.product3Brand').parent().slideDown();
                    $block.find('.product3Name').parent().slideDown();
                    $block.find('.product3Image').parent().slideDown();
                    $block.find('.product3Price').parent().slideDown();
                    $block.find('.product3PromoPrice').parent().slideDown();
                    break;
            }

        }


        var hideAll = function() {
            if (typeof $product1 !== 'undefined') {
                $('.product1Brand').parent().hide();
                $('.product1Name').parent().hide();
                $('.product1Image').parent().hide();
                $('.product1Price').parent().hide();
                $('.product1PromoPrice').parent().hide();
            }
            if (typeof $product2 !== 'undefined') {
                $('.product2Brand').parent().hide();
                $('.product2Name').parent().hide();
                $('.product2Image').parent().hide();
                $('.product2Price').parent().hide();
                $('.product2PromoPrice').parent().hide();
            }
            if (typeof $product3 !== 'undefined') {
                $('.product3Brand').parent().hide();
                $('.product3Name').parent().hide();
                $('.product3Image').parent().hide();
                $('.product3Price').parent().hide();
                $('.product3PromoPrice').parent().hide();
            }
        };
        hideAll();

    })();
});
$(document).ready(function () {
    // Prevent User to submit twice
    $(document).on("submit", "form[name='giftfinderbundle_finderFilter']", function () {
        $('.submit-action').attr('disabled', true).append('<i class="fa fa-refresh fa-spin fa-fw"></i>');
    });


    // Step2 page

    $("#sortable").on("sortstop", function (event, ui) {
        var $messageSection = $('.ui-sortable-messages');
        var position = ui.item.index() + 1;
        var url = ui.item.data('url').slice(0, -1) + position;
        var $badge = ui.item.find('.badge');
        $.ajax({
            url: url,
            context: $messageSection
        }).done(function (data) {
            var html = '<div class="alert alert-success" role="alert"><strong>SUCCESS!</strong> ' + data.message + '</div>';
            $(this).html(html).fadeIn(500, 0).delay(5000).fadeOut(500);
            $badge.text(position);

        }).fail(function (data) {
            var html = '<div class="alert alert-danger" role="alert"><strong>ERROR!</strong> It did not update, please refresh page, if problem continues check with developer</div>';
            $(this).html(html).fadeIn(500, 0).delay(5000).fadeOut(500);
        });
    });
    // Call to update position
    $("button.edit-filter").on("click", function () {
        $.ajax({
            url: $(this).data('url'),
            context: $('.modal-body'),
            beforeSend: function (xhr) {
                $(this).html('<div class="text-center"><i class="fa fa-refresh fa-spin fa-4x fa-fw"></i><br><span>Loading...</span></div>');
            }
        }).done(function (data) {
            $(this).html(data);
            // Reset select2
            $("select.select2").select2({
                tags: true
            });
        }).fail(function (data) {
            var html = '<div class="alert alert-danger" role="alert"><strong>ERROR!</strong> ' + data.message + '</div>';
            $(this).html(html).fadeIn(500, 0).delay(5000).fadeOut(500);
        });
    });

    // Filter form logic
    $(document).on("change", "form #giftfinderbundle_finderFilter_last", function (e) {
        var value = $(this).val();
        if (value == 1) {
            $('.last-yes select').prop("disabled", false).val([])
                .find('.select2-selection__choice').remove();
            $('.last-no select').prop("disabled", true).val([])
                .find('.select2-selection__choice').remove();
        }
        if (value == 0) {
            $('.last-yes select').prop("disabled", true).val([])
                .find('.select2-selection__choice').remove();
            $('.last-no select').prop("disabled", false).val([])
                .find('.select2-selection__choice').remove();

        }
    });

    // Step 3 page
    $('.filter-selection').on("click", function (event) {
        $('.filter-selection').each(function (index) {
            $(this).removeClass('active').find('i').removeClass('fa-check-circle');
        });
        $(this).addClass('active').find('i').addClass('fa-check-circle');
        var link = $(this).data('link');
        $('.filter-info').hide();
        $('.filter-info.' + link).fadeIn(300);


    })
    $("select.select2").select2({
        tags: true
    });
    $("select.select2-basic").select2({
        tags: false,
        allowClear: true
    });
    $("select.select2-basic").select2({
        tags: false,
        allowClear: true
    });
});

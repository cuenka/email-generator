<?php

namespace AppBundle\Service;

use Ijanki\Bundle\FtpBundle\DependencyInjection\IjankiFtpExtension;
use Ijanki\Bundle\FtpBundle\Ftp;
use Ijanki\Bundle\FtpBundle\IjankiFtpBundle;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class Filer
{
    const DS = DIRECTORY_SEPARATOR;

    /**
     * @var string cachePath
     */
    private $cachePath;

    /**
     * @var string uploadPath
     */
    private $uploadPath;

    /**
     * @var string CachePath
     */
    private $imagePath;

    /**
     * @var boolean
     */
    private $iseLab;

    /**
     * Uploader constructor.
     */
    public function __construct($cachePath, $uploadPath, $imagePath, $iseLab)
    {
        $this->cachePath = realpath($cachePath);
        $this->uploadPath = realpath($uploadPath);
        $this->imagePath = realpath($imagePath);
        $this->iseLab = $iseLab;
    }

    /**
     * @return bool|string
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }

    /**
     * @param bool|string $cachePath
     */
    public function setCachePath($cachePath)
    {
        $this->cachePath = $cachePath;
    }

    /**
     * @return bool|string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param bool|string $imagePath
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
    }

    /**
     * @return bool|string
     */
    public function getUploadPath()
    {
        return $this->uploadPath;
    }

    /**
     * @param bool|string $uploadPath
     */
    public function setUploadPath($uploadPath)
    {
        $this->uploadPath = $uploadPath;
    }

    /**
     * @return bool
     */
    public function isIseLab()
    {
        return $this->iseLab;
    }

    /**
     * @param bool $iseLab
     */
    public function setIseLab($iseLab)
    {
        $this->iseLab = $iseLab;
    }



    /**
     * Duplicate asset folder of email to a new email, used on Duplicate functionality
     * @param string $sourceId source ID
     * @param string $destinationID destination ID
     */
    public function duplicateAssetFolder($sourceId, $destinationID)
    {
        if ($this->iseLab == false) {
            $src = $this->imagePath . self::DS . $sourceId;
            $dst = $this->imagePath . self::DS . $destinationID;
            $this->recurseCopy($src, $dst);
        }
    }
    /**
     * @param string $src source
     * @param string $dst destination
     */
    private function recurseCopy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recurseCopy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }

        closedir($dir);
    }


}
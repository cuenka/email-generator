<?php

namespace AppBundle\Service;


use Symfony\Component\DomCrawler\Crawler;

class ProductCrawler
{
    private $specialCharacters = ['\n', '\t', ' ', '\r'];

    /**
     * @var string
     *
     * @ORM\Column(name="crawler", type="string", length=255)
     */
    private $crawler;

    /**
     * Crawler constructor.
     * @param boole $isElab
     */
    public function __construct($isElab = false)
    {
        if ($isElab) {
            if (isset($_SERVER['HTTP_CLIENT_IP'])
                || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
                || (in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'], true) || PHP_SAPI === 'cli-server')
            ) {
                $PROXY_HOST = "internal-proxy.ho.superdrug.aswatson.com"; // Proxy server address
                $PROXY_PORT = "8080";    // Proxy server port
                $PROXY_USER = "cuencaj";    // Username
                $PROXY_PASS = "Superdrug8";   // Password
// Username and Password are required only if your proxy server needs basic authentication

                $auth = base64_encode("$PROXY_USER:$PROXY_PASS");
                stream_context_set_default(
                    array(
                        'http' => array(
                            'proxy' => "tcp://$PROXY_HOST:$PROXY_PORT",
                            'request_fulluri' => true,
                            'header' => "Proxy-Authorization: Basic $auth"
                            // Remove the 'header' option if proxy authentication is not required
                        )
                    )
                );
            }
        }
    }

    public function setUrl($url)
    {
        try {
            $html = file_get_contents($url);
            $this->crawler = new Crawler($html);
            unset($html);
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function marionnaudProduct()
    {
        $product['PRODUCT_BRAND'] = $this->cleanText($this->marionnaudGetBrand());
        $product['PRODUCT_NAME'] = $this->cleanText($this->marionnaudGetName());
        $product['PRODUCT_IMAGE_URL'] = $this->marionnaudGetImageURL();
        $product['PRODUCT_PRICE'] = $this->marionnaudGetPrice();
        $product['PRODUCT_PROMOPRICE'] = empty($this->marionnaudGetPromoPrice()) ? null : $this->marionnaudGetPromoPrice();

        if  ($product['PRODUCT_PROMOPRICE'] == $product['PRODUCT_PRICE']) {
            $product['PRODUCT_PROMOPRICE'] = null;
        }

        return $product;
    }

    private function marionnaudGetBrand()
    {
        // New template Rich created MRD 2.0
        $brand = $this->crawler
            ->filter('body h1 .product-section-info-naming__brand')
            ->first();

        if ($brand->count() == 1) {
            return $brand->text();
        }

        //Normal design
        $brand = $this->crawler
            ->filter('body h1 > a')
            ->first();

        if ($brand->count() == 1) {
            return $brand->text();
        }

        $brand = $this->crawler
            ->filter('body h1.prod-name')
            ->first();

        if ($brand->count() == 1) {
            return $brand->text();
        }
        $brand = $this->crawler
            ->filter('.detailed_product__info span[itemprop="name"]')
            ->first();
        if ($brand->count() == 1) {
            return $brand->text();
        }

        return null;
    }


    private function marionnaudGetName()
    {
        // New template Rich created MRD 2.0
        $name = $this->crawler
            ->filter('body h1 .product-section-info-naming__text')
            ->first();

        if ($name->count() == 1) {
            return $name->text();
        }

        $name = $this->crawler
            ->filter('body h1 > span')
            ->first();

        if ($name->count() == 1) {
            return $name->text();
        }

        $name = $this->crawler
            ->filter('body .detailed_product__product_name')
            ->first();
        if ($name->count() == 1) {
            return $name->text();
        }

        return null;
    }

    private function marionnaudGetPrice()
    {
        // New template Rich created MRD 2.0
        $price = $this->crawler
            ->filter('body .product-section-info .product-section-cost__price')
            ->first();

        if ($price->count() == 1) {
            return str_replace($this->specialCharacters, '', $price->text());
        }

        $price = $this->crawler
            ->filter('body #fullPrice .price-value')
            ->first();

        if ($price->count() == 1) {
            return str_replace($this->specialCharacters, '', $price->text());
        }
        //MCH variation
        $price = $this->crawler
            ->filter('body span#fullPrice>span>span');
        if ($price->count() > 1) {
            $amount = number_format(str_replace(array("\r\n", "\r", "\n", "\t", " "), '', $price->first()->text()), 2);
            $currency = str_replace(array("\r\n", "\r", "\n", "\t", " "), '', $price->last()->text());
            return $currency . " " . $amount;
        }

        // TPS variation
        $price = $this->crawler
            ->filter('body label.selected .priceFormattedValue')
            ->first();

        if ($price->count() == 1) {
            return str_replace($this->specialCharacters, '', $price->text());
        }

        // DROGAS
        $price = $this->crawler
            ->filter('body .detailed_product__variant_non_actual_price_val')
            ->first();

        if ($price->count() == 1) {
            $price  = str_replace($this->specialCharacters, '', $price->text());
            $price  = explode('€',$price);
            if (count($price)>  1 ) return trim($price[0]). '€';
        }

        // DROGAS NO PROMO
        $price = $this->crawler
            ->filter('body .detailed_product__variant_price')
            ->first();

        if ($price->count() == 1) {
            $price  = str_replace($this->specialCharacters, '', $price->text());
            $price  = explode('€',$price);
            if (count($price)>  1 ) return trim($price[0]). '€';
        }


        return null;
    }

    private function marionnaudGetPromoPrice()
    {
        // New template Rich created MRD 2.0
        $promoPrice = $this->crawler
            ->filter('body .product-section-info .product-section-cost__price--new')
            ->first();

        if ($promoPrice->count() == 1) {
            return str_replace($this->specialCharacters, '', $promoPrice->text());
        }

        $promoPrice = $this->crawler
            ->filter('body #discountPrice .price-value')
            ->first();

        if ($promoPrice->count() == 1) {
            return str_replace($this->specialCharacters, '', $promoPrice->text());
        }
        //MCH variation
        $price = $this->crawler
            ->filter('body .promo-discounts #discountPrice');
        if ($price->count() == 1) {
            return str_replace(array("\r\n", "\r", "\n", "\t", " "), '', $price->text());
        }
        // DROGAS
        $price = $this->crawler
            ->filter('body .detailed_product__variant_price#pricespan')
            ->first();

        if ($price->count() == 1) {
            $price  = str_replace($this->specialCharacters, '', $price->text());
            $price  = explode('€',$price);
            return trim($price[0]). '€';
        }


        return null;
    }

    private function marionnaudGetImageURL()
    {
        // New template Rich created MRD 2.0
        $url = $this->crawler
            ->filter('body .glide__slides .product-section__image')
            ->first();

        if ($url->count() == 1) {
            if (!is_null($url->attr('src')))  return $url->attr('src');
        }

        // DROGAS
        $url = $this->crawler
            ->filter('body img.j-detailed_product_image')->first();

        if ($url->count() == 1) {
            if (!is_null($url->attr('src')))  return $url->attr('src');
        }

        // MCH
        $url = $this->crawler
            ->filter('body #product_image');

        if ($url->count() == 1) {
            if (!is_null($url->attr('data-src')))  return $url->attr('data-src');
        }

        // TPS Variation
        $url = $this->crawler
            ->filter('body .product-images img')->first();

        if ($url->count() == 1) {
            return $url->attr('src');
        }

        return null;
    }
    private function cleanText($text)
    {
        $text = trim($text);
        $text = str_replace(array("\r\n", "\r", "\n", "\t"), '', $text);
        return $text;
    }
}
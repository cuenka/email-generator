<?php

namespace AppBundle\Service;

use GiftFinderBundle\Entity\Finder;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class Converter
{
    /**
     * @var Kernel
     */
    private $kernel;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var string
     */
    private $cacheDir;
    /**
     * CsvCreator constructor.
     */
    public function __construct($kernel)
    {
        $this->serializer =  new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        $this->kernel = $kernel;

        $this->cacheDir = $this->kernel->getCacheDir();

    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return $this->cacheDir;
    }

    /**
     *
     * @param string $file includes absolute path and filename
     * @return array
     */
    public function saveOnDisk($file)
    {
        $filename = 'list-' . time() . ".csv";
        file_put_contents($this->cacheDir. DIRECTORY_SEPARATOR. $filename, $file);

        return $filename;
    }
    /**
     *
     * @param string $file includes absolute path and filename
     * @return array
     */
    public function saveFile($file)
    {
        $data = $this->serializer->decode( file_get_contents($file), 'csv');

        return $data;
    }

    /**
     *
     * @param string $file includes absolute path and filename
     * @return array
     */
    public function csvToArray($filePath)
    {
        $path = $this->cacheDir. $filePath;
        $data = $this->serializer->decode( file_get_contents($path), 'csv');

        return $data;
    }

    /**
     *
     * @param string $data includes absolute path and filename
     * @return Serializer
     */
    public function arrayToCsv($data)
    {
        $csv = $this->serializer->encode( $data, 'csv');

        return $csv;
    }

    /**
     *
     * @param string $data includes absolute path and filename
     * @return Serializer
     */
    public function arrayToJson($data)
    {
        $json = $this->safeJsonEncode($data);

        return $json;
    }

    private function safeJsonEncode($value)
    {
        if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
            $encoded = json_encode($value, JSON_PRETTY_PRINT);
        } else {
            $encoded = json_encode($value);
        }
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $encoded;
            case JSON_ERROR_DEPTH:
                return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_STATE_MISMATCH:
                return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found';
            case JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_UTF8:
                $clean = $this->utf8ize($value);
                return $this->safeJsonEncode($clean);
            default:
                return 'Unknown error'; // or trigger_error() or throw new Exception()

        }
    }

    private function utf8ize($mixed)
    {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = $this->utf8ize($value);
            }
        } else if (is_string($mixed)) {
            return utf8_encode($mixed);
        }
        return $mixed;
    }

}
<?php

namespace AppBundle\Service;


use AuthenticationBundle\Entity\BusinessUnit;
use EmailGeneratorBundle\Entity\Block;
use EmailGeneratorBundle\Entity\Email;
use Leafo\ScssPhp\Compiler;
use Symfony\Component\HttpKernel\Kernel;

class ImageManager
{
    const DS = DIRECTORY_SEPARATOR;

    /**
     * FinderGenerator constructor.
     */
    public function __construct()
    {

    }


    /**
     * returns array with image width and height, otherwise will return false
     * If $imagePath does not exist catch an Exception
     * @param string $imagePath
     * @return array|bool
     */
    public function getImageDimensions(string $imagePath)
    {
        try {
            $imageSize = getimagesize($imagePath);

            return
                [
                    'width' => $imageSize[0],
                    'height' => $imageSize[1]
                ];
        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * It compares 2 arrays that should be passes as
     * [
     * 'width' => XXX,
     * 'height' => XXX
     * ];
     *
     * If is empty means dimensions are perfect, if width is not set means that at lease the width is OK
     *
     * @param array $imageDimensions
     * @param array $rightDimensions
     * @return bool
     */
    public function hasRightDimensions(array $imageDimensions, array $rightDimensions)
    {
        $result = array_diff($imageDimensions, $rightDimensions);
        // if is empty means dimensions are perfect, if width is not set means that at lease the width is OK
        if (empty($result) || isset($result['width']) == false) {
            return true;
        }

        return false;
    }

    /**
     * Resize image by width given a path and dimension
     * @param string $imagePath
     * @param $width
     * @return bool
     */
    public function resizeByWidth(string $imagePath,string $imageName, $width)
    {
        try {
            if (is_array($width)) {
                $width = (isset($width['width']) ? $width['width'] : 640);
            }
            $width = intval($width);
            $upload = new \upload($imagePath. DIRECTORY_SEPARATOR. $imageName);

            if ($upload->uploaded) {
                $upload->file_new_name_body= $imageName;
                $upload->file_dst_name_ext = null;
                $upload->file_dst_name = $imageName;
                $upload->image_resize = true;
                $upload->image_x = $width;
                $upload->image_ratio_y = true;
                $upload->file_overwrite =true;
                $upload->process($imagePath);
                if ($upload->processed) {
                    $upload->clean();
                    rename($imagePath. DIRECTORY_SEPARATOR. $upload->file_dst_name, $imagePath. DIRECTORY_SEPARATOR. $imageName);
                    return true;
                } else {
                    return false;
                }
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $file
     */
    public function validateImage($path, $file, Block $block)
    {
        $filePath = $path . DIRECTORY_SEPARATOR . $file;
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        $tempFilePath = $path . DIRECTORY_SEPARATOR . 'temp.' . $ext;
        $blockHelper = new BlockHelper();
        $imageInfo = getimagesize($filePath);
        $rightDimensions = $blockHelper->getCorrectImageDimensions($block->getCode());
        if (!isset($imageInfo[0]) ||
            !isset($imageInfo[1]) ||
            ($imageInfo[0] != $rightDimensions['width']) ||
            ($imageInfo[1] != $rightDimensions['height'])) {
            $this->addFlash('warning', '<strong>Easy there!</strong> The dimensions of the image are: ' .
                $imageInfo[0] . 'px by ' . $imageInfo[1] . 'px, and should be ' . $rightDimensions['width'] . 'px by ' .
                $rightDimensions['height'] . 'px');
            $handle = new \upload($filePath);
            if ($handle->uploaded) {
                $handle->file_new_name_body = 'temp';
                $handle->image_resize = true;
                $handle->image_x = $rightDimensions['width'];
                $handle->image_ratio_y = true;
                $handle->process($path);
                if ($handle->processed) {
                    $this->addFlash('success', '<strong>Resized!</strong> The upload image has been resized and optimised!');
                    $handle->clean();
                    rename($tempFilePath, $filePath);
                } else {
                    $this->addFlash('danger', '<strong>Error!</strong>' . $handle->error);
                }
            }
        }
    }

}
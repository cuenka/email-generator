<?php

namespace AppBundle\Service;

use Ijanki\Bundle\FtpBundle\DependencyInjection\IjankiFtpExtension;
use Ijanki\Bundle\FtpBundle\Exception\FtpException;
use Ijanki\Bundle\FtpBundle\Ftp;
use Ijanki\Bundle\FtpBundle\IjankiFtpBundle;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class Uploader
{
    const DS = DIRECTORY_SEPARATOR;
    /**
     * Path to cache
     * @var string $cacheDir
     */
    private $cacheDir;

    /**
     * Path to cache
     * @var Ftp $ftp
     */
    private $ftp;

    /**
     * @var string $ftpHost
     */
    private $ftpHost;

    /**
     * @var string $ftpUsername
     */
    private $ftpUsername;

    /**
     * @var string $ftpPassword
     */
    private $ftpPassword;

    /***
     * @var string $ftpDestination
     */
    private $ftpDestination;

    /**
     * @var array
     */
    private $whitelist =
        [
            '127.0.0.1',
            '::1'
        ];

    /**
     * Uploader constructor.
     */
    public function __construct($cacheDir, $ftp, $host, $username, $password, $destination)
    {
        $this->cacheDir = $cacheDir;
        $this->ftp = $ftp;
        $this->ftpHost = $host;
        $this->ftpUsername = $username;
        $this->ftpPassword = $password;
        $this->ftpDestination = $destination;
    }

    /**
     * @param string $ftpHost
     */
    public function setFtpHost($ftpHost)
    {
        $this->ftpHost = $ftpHost;
    }

    /**
     * @param string $ftpUsername
     */
    public function setFtpUsername($ftpUsername)
    {
        $this->ftpUsername = $ftpUsername;
    }

    /**
     * @param string $ftpPassword
     */
    public function setFtpPassword($ftpPassword)
    {
        $this->ftpPassword = $ftpPassword;
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return $this->cacheDir;
    }

    /**
     * @param string $ftpDestination
     */
    public function setFtpDestination($ftpDestination)
    {
        $this->ftpDestination = $ftpDestination;
    }


    public function saveSubmitFileOnCache(File $file, $filename, $relativePath = null)
    {
        $path = $this->cacheDir . $relativePath . self::DS;
        if (is_null($relativePath)) {
            $path = $this->cacheDir . self::DS;
        }
        $file->move($path, $filename);

    }


    public function saveFileOnCache($filePath, $data)
    {
        $path = $this->cacheDir . $filePath;
        file_put_contents($path, $data);

    }

    private function connect()
    {
        $this->ftp->connect($this->ftpHost);
        return $this->ftp->login($this->ftpUsername, $this->ftpPassword);
    }


    /**
     * @TODO create this functionality, it looks like it needs to be done the download of assets and the re-upload on the new path
     * duplicate folder and all content inside on ftp
     * @param string $sourceID ID of
     * @param string $newID
     * @param string $relativePath if it is /email_generator/ or /pdp_generator/ or other app
     * @return bool
     */
    public function duplicateFolderOnFTP(string $sourceID, string $newID, string $relativePath = "")
    {
        try {
//            $fullSourcePath  = $this->ftpDestination. $relativePath. $sourceID;
//            $destinationPath =$this->ftpDestination. $relativePath. $newID;
//            $this->connect();
//            $assets = $this->ftp->nlist($fullSourcePath);
//            foreach ($assets as $asset) {
//
//            }
//            $this->ftp->close();

        } catch (FtpException $e) {
            $this->ftp->close();
            return false;
        }
    }

    /**
     * Upload to FTP a file pased on parameters, no validations are passed here
     * @param string $fullSourcePath Include path and filename
     * @param string $destinationPath Just the destination path
     * @param string $destinationFilename just the filename of the file to be saved on the FTP
     *
     * @return boolean
     */
    public function uploadToFTP(string $fullSourcePath, string $destinationPath, string $destinationFilename )
    {
        try {
            if ($this->existOnDestination($destinationPath, $destinationFilename)) {
                $destinationFilename = $destinationPath . self::DS . $destinationFilename;
            } else {
                $this->createFolderOnDestination($destinationPath);
                $destinationFilename = $destinationPath . self::DS . $destinationFilename;
            }
            $this->connect();
            $this->ftp->put($destinationFilename, $fullSourcePath, FTP_BINARY);
            $this->ftp->close();
            return true;
        }catch (FtpException $e) {
            $this->ftp->close();
            return false;
        }
    }

    /**
     * Check if server is localhost
     * @return bool
     */
    public function isLocalhost() {
        if(!in_array($_SERVER['REMOTE_ADDR'], $this->whitelist)){
            return false;
        }

        return true;
    }

    /**
     * Check if a file or folder already exists on server
     * @param $directory
     * @param $file
     * @return bool
     */
    public function existOnDestination(string $directory, string $file = null)
    {

        // files is null so assume that is a folder the thing to check
        if (is_null($file)) {
            $this->connect();
            $result = $this->ftp->chdir($directory);
            $this->ftp->close();

            return $result;
        }
        $this->connect();
        $filesOnServer = $this->ftp->nlist($directory);
        if (in_array($directory. self::DS.$file, $filesOnServer)) {
            $this->ftp->close();
            return true;
        }

        $this->ftp->close();
        return false;
    }

    /**
     * @param $directory
     * @param $file
     * @return bool
     */
    public function deleteOnDestination($directory, $file)
    {
        if ($this->existOnDestination($directory, $file)) {
            $this->connect();
            if ($this->ftp->delete($directory. self::DS. $file)) {
                $this->ftp->close();
                return true;
            }
        }
        $this->ftp->close();
        return false;
    }

    /**
     * It deletes folder on FTP, if the folder is not empty, IT will delete all the files inside
     * IMPORTANT: No Way to undo this!!
     * @TODO
     * @param string $directory FTP path
     *
     * @return boolean True if OK, False if Error
     */
    public function deleteFolderOnDestination(string $directory)
    {
        $this->connect();

    }

    /**
     * It creates a folder if does not exist, if exist does nothing It does not return nothing
     * @param string $directory
     */
    public function createFolderOnDestination(string $directory)
    {
        try {
            $this->connect();
            $this->ftp->mkdir($directory);
            $this->ftp->close();


        } catch (FtpException $e) {
            $this->ftp->close();
        }
    }
}
<?php

namespace PDPGeneratorBundle\Service;


use AuthenticationBundle\Entity\BusinessUnit;
use AppBundle\service\ProductCrawler;
use Leafo\ScssPhp\Compiler;
use PDPGeneratorBundle\Entity\Block;
use PDPGeneratorBundle\Entity\Page;
use Symfony\Component\HttpKernel\Kernel;

class PDPGenerator
{
    const DS = DIRECTORY_SEPARATOR;
    const COMMON = 'common';
    /**
     * @var Kernel
     */
    private $kernel;
    /**
     * @var
     */
    private $twig;

    /**
     * @var Compiler
     */
    private $scss;

    /**
     * @var string CachePath
     */
    private $cachePath;

    /**
     * @var string CachePath
     */
    private $imagePath;

    /**
     * @var ProductCrawler
     */
    private $productCrawler;

    /**
     * @var string CachePath
     */
    private $isElab;

    /**
     * @var Page $page
     */
    private $page;

    /**
     * FinderGenerator constructor.
     */
    /**
     * PDPGenerator constructor.
     * @param $kernel
     * @param $twig
     * @param $cachePath
     * @param $imagePath
     * @param $productCrawler
     * @param $isElab
     */
    public function __construct($kernel, $twig, $cachePath, $imagePath, $productCrawler, $isElab)
    {
        $this->kernel = $kernel;
        $this->twig = $twig;
        $this->cachePath = realpath($cachePath);
        $this->imagePath = realpath($imagePath);
        $this->scss = new Compiler();
        $this->productCrawler = $productCrawler;
        $this->isElab = $isElab;
    }


    /**
     * @return Kernel
     */
    public function getKernel()
    {
        return $this->kernel;
    }


    /**
     * @param Page $page
     * @return $this
     */
    public function setPage(Page $page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return string
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @return string
     */
    public function getIsElab()
    {
        return $this->isElab;
    }


    /**
     * We do this in case the layout change by user and we want to keep records on cache...
     * @return array
     */
    public function getTemplatePath()
    {
        return [
            'common'     => $this->kernel->locateResource('@PDPGeneratorBundle/Resources/templates/blocks')
        ];
    }

    /**
     * @param integer $pageID Page ID
     * @param string $type Standard, vip...
     * @param string $bu Business unit
     * @return bool
     */
    public function preview($pageID = 0, $type = null, $bu = null)
    {
        try {
            $templatePath = $this->getTemplatePath($type, $bu);
            return true;
        } catch (\Exception $exception) {
            echo 'Code: ' . $exception->getCode() . 'Message: ' . $exception->getMessage();
        }
    }



    public function generateHtml(Block $block, string $blocksPath, $parameters = null)
    {
        $pathToHtml = $blocksPath . self::DS . $block->getCode() . '.html.twig';
        $html = $this->twig->render($pathToHtml,
            [
                'block' => $block,
                'parameters' => $parameters
            ]
        );

        return $html;
    }

    public function getParameters(Page $page)
    {
        $params =
            [
                'domain' => BusinessUnit::getDomain($page->getBusinessUnit()->getCode()),
                'imagespath' => '/images/'. $page->getId(). self::DS
            ];

        // CDN PATH
        if ($this->getKernel()->getContainer()->getParameter('is_elab')) {
            $params['imagespath'] =  BusinessUnit::getDomain($page->getBusinessUnit()->getCode()).'/elab' .
                $this->getKernel()->getContainer()->getParameter('ftp_destination')  . self::DS . 'pdp_generator' . self::DS . $page->getId(). self::DS;
        }

        return $params;
    }

    /**
     * @deprecated No need of check this anymore as no extraparams are needed
     * @param Block $block
     * @return boolean
     */
    public function requireExtraInfo(Block $block = null)
    {
        $isTrue = false;

        if (is_null($block) === false) {
            if (property_exists($block, 'product1')) {
                $isTrue = true;
            }
            if (property_exists($block, 'product2')) {
                $isTrue = true;
            }
            if (property_exists($block, 'product3')) {
                $isTrue = true;
            }
        }

        return $isTrue;
    }

    /**
     * @deprecated Now it crawl the website before rendering, It will be removed soon
     * @param Block $block
     * @return boolean
     */
    public function getExtraInfo(Block $block = null, string $bu)
    {
        $extraParams = [];
        if (is_null($block) === false) {
            if (property_exists($block, 'product1')) {
                $extraParams['product1'] = $this->crawlProduct($block->getProduct1(), $bu);
            }
            if (property_exists($block, 'product2')) {
                $extraParams['product2'] = $this->crawlProduct($block->getProduct2(), $bu);
            }
            if (property_exists($block, 'product3')) {
                $extraParams['product3'] = $this->crawlProduct($block->getProduct3(), $bu);
            }
        }
        return $extraParams;
    }

    private function crawlProduct($id, $bu)
    {
        $url = BusinessUnit::amendURL($id, $bu);

        if ($this->productCrawler->setUrl($url)) {
            return $this->productCrawler->marionnaudProduct();
        }

        return false;

    }

    /**
     * @return string
     */
    public function createHtml()
    {
        $html = $this->twig->render($this->kernel->locateResource('@PDPGeneratorBundle/Resources/templates/blocks/head.html.twig'),
            [
                'parameters' => $this->getParameters($this->getPage())
            ]
        );        $blocksPath = $this->getTemplatePath();
        foreach ($this->getPage()->getBlocks() as $block) {
                $html .= $this->generateHtml($block, $blocksPath['common'], $this->getParameters($this->getPage()));
        }
        $html .= $this->twig->render($this->kernel->locateResource('@PDPGeneratorBundle/Resources/templates/blocks/footer.html.twig'),
            [
                'parameters' => $this->getParameters($this->getPage())
            ]
        );
        return $html;
    }
}
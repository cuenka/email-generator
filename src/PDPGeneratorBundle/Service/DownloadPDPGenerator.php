<?php

namespace PDPGeneratorBundle\Service;

use AuthenticationBundle\Entity\BusinessUnit;
use PDPGeneratorBundle\Entity\Block;
use Leafo\ScssPhp\Compiler;
use PDPGeneratorBundle\Entity\Page;

class DownloadPDPGenerator extends PDPGenerator
{

    /**
     * DownloadGenerator constructor.
     */
    public function __construct($kernel, $twig, $cachePath, $imagePath, $productCrawler, $isElab)
    {
        parent::__construct($kernel, $twig, $cachePath, $imagePath, $productCrawler, $isElab);
    }

    /**
     * @return string
     */
    public function getZipPath()
    {
        return $this->getCachePath(). self::DS. 'pdp_generator_assets'. self::DS. $this->getPage()->getId(). self::DS;
    }


    public function MoveFilesToCache()
    {
        // Variables
        $cachePath = $this->getCachePath(). self::DS.'pdp_generator_assets';
        //creating and making directores
        if (!is_dir($cachePath)) {
            mkdir($cachePath);
        }
        $cachePath = $this->getCachePath(). self::DS.'pdp_generator_assets'. self::DS. $this->getPage()->getId();
        $imagePath = $this->getImagePath(). self::DS.'pdp_generator_assets'. self::DS. $this->getPage()->getId();
        //creating and making directores
        if (!is_dir($cachePath)) {
            mkdir($cachePath);
        }
        if (!is_dir($cachePath. self::DS. 'images')) {
            mkdir($cachePath. self::DS. 'images');
        }
        // Scanning Files
        $images = array_diff(scandir($imagePath), array('..', '.', '.DS_Store'));
        // Coping files to cache
        foreach($images as $image) {
            copy($imagePath. self::DS. $image, $cachePath. self::DS. 'images'. self::DS. $image);
        }
        file_put_contents($cachePath. self::DS. 'index.html', $this->createHtml());

        return $this;
    }
    
    public function createZip()
    {
        // Variables
        $pagePath = $this->getCachePath(). self::DS.'pdp_generator_assets'. self::DS. $this->getPage()->getId();
        $zip = new \ZipArchive();
        $zipName = 'page-' . time() . ".zip";
        $zipPath = $pagePath . self::DS . $zipName;
        $zip->open($zipPath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $zip->addFile($pagePath. self::DS. 'index.html', 'index.html');
        $zip->addEmptyDir('images');
        $images = array_diff(scandir($pagePath. self::DS. 'images'), array('..', '.', '.DS_Store'));
        foreach ($images as $image) {
            $zip->addFile(
                $pagePath. self::DS. 'images'. self::DS. $image,
                'images'. self::DS. $image
            );
        }
        $zip->close();

        return $zipName;
    }

    public function getParameters(Page $page)
    {
        $params =
            [
                'fontFamily' => $page->getFontFamily(),
                'domain' => BusinessUnit::getDomain($page->getBusinessUnit()->getCode()),
                'imagespath' => 'images/'
            ];

        // CDN PATH
        if ($this->getKernel()->getContainer()->getParameter('is_elab')) {
            $params['imagespath'] =  BusinessUnit::getDomain($page->getBusinessUnit()->getCode()).'/elab' .
                $this->getKernel()->getContainer()->getParameter('ftp_destination')  . self::DS . 'pdp_generator' . self::DS . $page->getId(). self::DS;
        }

        return $params;
    }
}
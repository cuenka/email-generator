<?php

namespace PDPGeneratorBundle\Service;


use AuthenticationBundle\Entity\BusinessUnit;
use PDPGeneratorBundle\Entity\Block;
use Leafo\ScssPhp\Compiler;
use PDPGeneratorBundle\Entity\Page;
use Symfony\Component\HttpKernel\Kernel;

class PreviewPDPGenerator extends PDPGenerator
{
    /**
     * PreviewGenerator constructor.
     */
    public function __construct($kernel, $twig, $cachePath, $imagePath, $productCrawler, $isElab)
    {
        parent::__construct($kernel, $twig, $cachePath, $imagePath, $productCrawler, $isElab);
    }

    public function getParameters(Page $page)
    {
        $params =
            [
                'domain' => BusinessUnit::getDomain($page->getBusinessUnit()->getCode()),
                'fontFamily' => $page->getFontFamily(),
                'imagespath' => '/images/pdp_generator_assets/'. $page->getId(). self::DS
            ];

        // CDN PATH
        if ($this->getKernel()->getContainer()->getParameter('is_elab')) {
            $params['imagespath'] =  BusinessUnit::getDomain($page->getBusinessUnit()->getCode()).'/elab' .
                $this->getKernel()->getContainer()->getParameter('ftp_destination')  . self::DS . 'pdp_generator' . self::DS . $page->getId(). self::DS;
        }

        return $params;
    }
}
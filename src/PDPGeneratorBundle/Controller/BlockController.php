<?php

namespace PDPGeneratorBundle\Controller;

use AuthenticationBundle\Entity\BusinessUnit;
use PDPGeneratorBundle\Entity\Block;
use PDPGeneratorBundle\Entity\Page;
use PDPGeneratorBundle\Helper\BlockHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Block controller. This controller is in charge of display blocks and CRUD blocks
 *
 * @Route("page/block")
 */
class BlockController extends Controller
{
    /**
     * List all page blocks avaiables and selected
     *
     * @Route("/list/{id}", name="page_block_list")
     * @Method({"GET","POST"})
     */
    public function listAction(Request $request, Page $page)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');
        $availableBlocks = BlockHelper::getAvaiableBlocks();

        $selectedBlocks = $page->getBlocks();
        return $this->render('PDPGeneratorBundle:Block:list.html.twig',
            [
                'page' => $page,
                'availableBlocks' => $availableBlocks,
                'selectedBlocks' => $selectedBlocks,
            ]);
    }

    /**
     * Add blocks, and re-order blocks from list page
     *
     * @Route("/edit/{id}", name="page_block_edit_list")
     * @Method({"GET","POST"})
     */
    public function editListAction(Request $request, Page $page)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');
        $availableBlocks = BlockHelper::getAvaiableBlocks();
        $em = $this->getDoctrine()->getManager();
        $selectedBlocks = $page->getBlocks();
        $blockHelper = new BlockHelper();
        $data = $request->get('data', null);
        foreach ($data as $key => $block) {
            if (isset($block['id'])) {
                foreach ($selectedBlocks as $selectedBlock) {
                    if ($selectedBlock->getId() === intval($block['id'])) {
                        $selectedBlock->setOrder($block['order']);
                    }
                }
            } else {
                $newBlock = $blockHelper->newInstance($block['name'], $block['order']);
                $page->addBlock($newBlock);
                $em->persist($newBlock);
            }
        }

        $em->persist($page);
        $em->flush();
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse('OK', 200);
        }

        return $this->render('PDPGeneratorBundle:Block:list.html.twig',
            [
                'page' => $page,
                'availableBlocks' => $availableBlocks,
                'selectedBlocks' => $selectedBlocks,
            ]);
    }

    /**
     * Edit block
     *
     * @Route("/individual/{id}", name="page_block_edit_individual_list")
     * @Method({"GET","POST"})
     */
    public function editIndividualListAction(Request $request, Page $page)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');

        return $this->render('PDPGeneratorBundle:Block:edit.html.twig', array(
            'page' => $page,
        ));
    }

    /**
     * Edit block
     *
     * @Route("/{page}/individual/block/{block}", name="page_block_edit_individual_block")
     * @Method({"GET","POST"})
     */
    public function editIndividualBlockAction(Request $request, Page $page, Block $block)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $blockHelper = new BlockHelper();
        $imageManager = $this->get('app.image_manager');
        $cdnUploader = $this->get('email_generator.cdn_manager');
        $ftpURl = BusinessUnit::getDomain($page->getBusinessUnit()->getCode()).'/elab' . $this->getParameter('ftp_destination') . DIRECTORY_SEPARATOR . 'pdp_generator' . DIRECTORY_SEPARATOR . $page->getId();
        // some block does not need form
        if (is_null($blockHelper->getFormType($block->getCode()))) {
            return $this->render('PDPGeneratorBundle:Block:_emptyBlock.html.twig', array(
                'blockId' => $block->getId()
            ));
        }

        $form = $this->createForm($blockHelper->getFormType($block->getCode()), $block,
            [
                'action' => $this->generateUrl('page_block_edit_individual_block',
                    [
                        'page' => $page->getId(),
                        'block' => $block->getId()
                    ]
                ),
                'method' => 'POST',
                'dataBlock' => $block->getId(),
                'dataPage' => $page->getId(),
            ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $ftpPath = $this->getParameter('ftp_destination') . DIRECTORY_SEPARATOR . 'pdp_generator' . DIRECTORY_SEPARATOR . $page->getId();
                if (method_exists($block, 'getFile') && (!is_null($block->getFile()))) {
                    $file = $block->getFile();
                    $fileName = $block->generateFileName($file->getClientOriginalName(), '.' . $file->guessExtension());

                    if ($this->getParameter('is_elab') == true) {

                        // Image manipulation and validation
                        if ($imageManager->hasRightDimensions($imageManager->getImageDimensions($file->getPathname()), $blockHelper->getCorrectImageDimensions($block->getCode())) !== true) {
                            $imageManager->resizeByWidth($file->getPath(), $file->getFilename(), $blockHelper->getCorrectImageDimensions($block->getCode()));
                            $this->addFlash('success', '<strong>Resized!</strong> The upload image has been resized and optimised!');
                        } else {
                            $this->addFlash('danger', '<strong>Image could not be resized!</strong> Check if your image is OK, try again, if problem persist, ask administrator');
                        }

                        // Image upload to FTP
                        if (
                            $cdnUploader->deletePreviousUpload($ftpPath, $block->getImage()) == false ||
                            $cdnUploader->uploadToCDN($file->getPathname(), null, $ftpPath, $fileName ) == false ) {
                            $this->addFlash('danger', '<strong>Issue on the cloud!</strong> Upload of the File return an error, try again or contact administrator if this issue persist');
                        }
                    } else {
                        // Image save on server
                        $file = $block->getFile();
                        $fileName = $block->generateFileName() . '.' . $file->guessExtension();
                        $cachePath = $this->getParameter('images_path');
                        $file->move($cachePath . DIRECTORY_SEPARATOR . $page->getId(), $fileName);
                        $this->validateImage($cachePath . DIRECTORY_SEPARATOR . $page->getId() . DIRECTORY_SEPARATOR . $fileName, $block);
                    }
                    $block->setImage($fileName);

                }

                if (method_exists($block, 'getFile2') && (!is_null($block->getFile2()))) {
                    $file = $block->getFile2();
                    $fileName = $block->generateFileName($file->getClientOriginalName(), '.' . $file->guessExtension());

                    if ($this->getParameter('is_elab') == true) {

                        // Image manipulation and validation
                        if ($imageManager->hasRightDimensions($imageManager->getImageDimensions($file->getPathname()), $blockHelper->getCorrectImageDimensions($block->getCode())) !== true) {
                            $imageManager->resizeByWidth($file->getPath(), $file->getFilename(), $blockHelper->getCorrectImageDimensions($block->getCode()));
                            $this->addFlash('success', '<strong>Resized!</strong> The upload image has been resized and optimised!');
                        } else {
                            $this->addFlash('danger', '<strong>Image could not be resized!</strong> Check if your image is OK, try again, if problem persist, ask administrator');
                        }

                        // Image upload to FTP
                        if (
                            $cdnUploader->deletePreviousUpload($ftpPath, $block->getImage2()) == false ||
                            $cdnUploader->uploadToCDN($file->getPathname(), null, $ftpPath, $fileName ) == false ) {
                            $this->addFlash('danger', '<strong>Issue on the cloud!</strong> Upload of the File return an error, try again or contact administrator if this issue persist');
                        }
                    } else {
                        // Image save on server
                        $file = $block->getFile2();
                        $fileName = $block->generateFileName() . '.' . $file->guessExtension();
                        $cachePath = $this->getParameter('images_path');
                        $file->move($cachePath . DIRECTORY_SEPARATOR . $page->getId(), $fileName);
                        $this->validateImage($cachePath . DIRECTORY_SEPARATOR . $page->getId() . DIRECTORY_SEPARATOR . $fileName, $block);
                    }
                    $block->setImage2($fileName);

                }

                if (method_exists($block, 'getFile3') && (!is_null($block->getFile3()))) {
                    $file = $block->getFile3();
                    $fileName = $block->generateFileName($file->getClientOriginalName(), '.' . $file->guessExtension());

                    if ($this->getParameter('is_elab') == true) {

                        // Image manipulation and validation
                        if ($imageManager->hasRightDimensions($imageManager->getImageDimensions($file->getPathname()), $blockHelper->getCorrectImageDimensions($block->getCode())) !== true) {
                            $imageManager->resizeByWidth($file->getPath(), $file->getFilename(), $blockHelper->getCorrectImageDimensions($block->getCode()));
                            $this->addFlash('success', '<strong>Resized!</strong> The upload image has been resized and optimised!');
                        } else {
                            $this->addFlash('danger', '<strong>Image could not be resized!</strong> Check if your image is OK, try again, if problem persist, ask administrator');
                        }

                        // Image upload to FTP
                        if (
                            $cdnUploader->deletePreviousUpload($ftpPath, $block->getImage3()) == false ||
                            $cdnUploader->uploadToCDN($file->getPathname(), null, $ftpPath, $fileName ) == false ) {
                            $this->addFlash('danger', '<strong>Issue on the cloud!</strong> Upload of the File return an error, try again or contact administrator if this issue persist');
                        }
                    } else {
                        // Image save on server
                        $file = $block->getFile3();
                        $fileName = $block->generateFileName() . '.' . $file->guessExtension();
                        $cachePath = $this->getParameter('images_path');
                        $file->move($cachePath . DIRECTORY_SEPARATOR . $page->getId(), $fileName);
                        $this->validateImage($cachePath . DIRECTORY_SEPARATOR . $page->getId() . DIRECTORY_SEPARATOR . $fileName, $block);
                    }
                    $block->setImage3($fileName);

                }
                $em->persist($block);
                $em->flush();
                $this->addFlash('success', '<strong>Excellent!</strong> The block that just tried to save (' .
                    str_replace('_', ' ', $block->getCode()) . ') has been updated!');
                return $this->redirectToRoute('page_block_edit_individual_list', array('id' => $page->getId()));
            } else {
                $this->addFlash('warning', '<strong>Upps!</strong> The block that just tried to save, (' .
                    str_replace('_', ' ', $block->getCode()) . '), contain errors');
                $errors = $form->getErrors(true, true);
                foreach ($errors as $error) {
                    $this->addFlash('danger', '<strong>Upps!</strong> ' . $error->getMessage());
                }
                return $this->redirectToRoute('page_block_edit_individual_list', array('id' => $page->getId()));
            }
        }

        return $this->render('PDPGeneratorBundle:Block:_editIndividual.html.twig', array(
            'form' => $form->createView(),
            'block' => $block,
            'pageId' => $page->getId(),
            'ftpURL' => $ftpURl
        ));
    }


    /**
     * Edit block
     *
     * @Route("/edit/{page}/customise", name="page_block_customise")
     * @Method({"GET","POST"})
     */
    public function customiseAction(Request $request, Page $page)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');

        $blocks = $request->get('data', []);
        $em = $this->getDoctrine()->getManager();
        $page->ammendBlocks($blocks);
        $em->persist($page);
        $em->flush();

        return new JsonResponse('OK', 200);

    }


    /**
     * Edit block
     *
     * @Route("/{page}/delete/{block}", name="page_block_delete")
     * @Method({"GET","POST"})
     */
    public function deleteAction(Request $request, Page $page, Block $block)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');

        if ($page->deleteBlock($block)) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush();
            return new JsonResponse('OK', 200);

        } else {
            return new JsonResponse('ERROR', 200);
        }

    }

    /**
     * Edit block
     *
     * @Route("/get/sku", name="page_block_sku")
     * @Method({"POST"})
     *    */
    public function skuAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');

        $variables = $request->request->all();
        $pageID = isset($variables['pageID']) ? $variables['pageID'] : 0;
        $blockID = isset($variables['blockID']) ? $variables['blockID'] : 0;
        $dataContext = isset($variables['dataContext']) ? $variables['dataContext'] : 0;
        $id = isset($variables['skuId']) ? $variables['skuId'] : 0;

        if (($id == 0) || ($pageID == 0)) {
            return $this->render('PDPGeneratorBundle:Block:_skuValues.html.twig', ['data' => false]);
        }

        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('PDPGeneratorBundle:Page')->find($pageID);
        $block = $em->getRepository('PDPGeneratorBundle:Block')->find($blockID);
        $bu = $page->getBusinessUnit()->getCode();
        $url = BusinessUnit::amendURL($id, $bu);
        $domain = BusinessUnit::getDomain($bu);
        $productCrawler = $this->get('app.product_crawler');
        $productCrawler->setUrl($url);
        $product = $productCrawler->marionnaudProduct();

        return $this->render('PDPGeneratorBundle:Block:_skuValues.html.twig',
            [
                'data' => true,
                'product' => $product,
                'block' => $block,
                'skuInfo' => $block->getSkuInfo($dataContext),
                'domain' => substr($domain, 0, -1)
            ]
        );

    }

    /**
     * @param $file
     */
    private function validateImage($file, Block $block)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');

        $blockHelper = new BlockHelper();
        $imageInfo = getimagesize($file);
        $rightDimensions = $blockHelper->getCorrectImageDimensions($block->getCode());
        if (!isset($imageInfo[0]) ||
            !isset($imageInfo[1]) ||
            ($imageInfo[0] != $rightDimensions['width']) ||
            ($imageInfo[1] != $rightDimensions['height'])) {
            $this->addFlash('warning', '<strong>Easy there!</strong> The dimensions of the image are: ' .
                $imageInfo[0] . 'px by ' . $imageInfo[1] . 'px, and should be ' . $rightDimensions['width'] . 'px by ' .
                $rightDimensions['height'] . 'px');
        }
    }
}
<?php

namespace PDPGeneratorBundle\Controller;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use PDPGeneratorBundle\Entity\Page;
use PDPGeneratorBundle\Form\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Page controller. This Controller is in charge of CRUD page entity, preview, download, send test pages. everything about the entity Page.
 *
 * @Route("page")
 */
class PageController extends Controller
{
    /**
     * Lists all pages entities.
     *
     * @Route("", name="page_index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');
        $user = $this->getUser();
        $bu = $user->getBusinessUnit()->getCode();

        $em = $this->getDoctrine()->getManager();
        $pages = $em->getRepository('PDPGeneratorBundle:Page')->getInfoForTable($bu);

        $actions =
            [
                'edit' =>
                    [
                        'path' => 'page_edit',
                        'name' => 'Edit information',
                        'btn' => 'info',
                    ],
                'block_list' =>
                    [
                        'path' => 'page_block_list',
                        'name' => 'Edit Layout',
                        'btn' => 'info',
                    ],
                'block_edit_individual_list' =>
                    [
                        'path' => 'page_block_edit_individual_list',
                        'name' => 'Edit content',
                        'btn' => 'info',
                    ],
                'archive' =>
                    [
                        'path' => 'page_archive_page',
                        'name' => 'Archive page',
                        'btn' => 'dark',
                    ],
                'duplicate' =>
                    [
                        'path' => 'page_duplicate',
                        'name' => 'Duplicate page',
                        'btn' => 'warning',
                    ],
            ];


        return $this->render('PDPGeneratorBundle:Page:index.html.twig', array(
            'data' => $pages,
            'actions' => $actions
        ));
    }

    /**
     * Creates a new page entity.
     *
     * @Route("/new", name="page_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');

        $user = $this->getUser();
        $page = new Page();
        $form = $this->createForm(PageType::class, $page,
            [
                'businessUnit' => $user->getBusinessUnit(),
                'user' => $user
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $page->setDateModified(new \DateTime());
            // Set up date requested as NOW, user and BU
            if ($user->getBusinessUnit()->getCode() != BusinessUnit::ASW) {
                $page->setBusinessUnit($user->getBusinessUnit())
                    ->setUser($user);
            } else {
                $page->setUser($user);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush();
            $this->addFlash('success', '<strong>Excellent!</strong> Data saved successfully, now you can work on the layout!');
            if ($form->get('saveAndGoToBuild')->isClicked()) {
                return $this->redirectToRoute('page_block_list', array('id' => $page->getId()));
            }
            if ($form->get('saveAndGoToEdit')->isClicked()) {
                return $this->redirectToRoute('page_block_edit_individual_list', array('id' => $page->getId()));
            }
            return $this->redirectToRoute('page_edit', array('id' => $page->getId()));
        }
        $actions =
            [
                'list' =>
                    [
                        'path' => 'page_index',
                        'name' => 'Check pages',
                        'btn' => 'primary',
                    ],
                'edit' =>
                    [
                        'path' => 'page_edit',
                        'name' => 'Edit information',
                        'btn' => 'info',
                    ]
            ];


        return $this->render('PDPGeneratorBundle:Page:new.html.twig', array(
            'page' => $page,
            'form' => $form->createView(),
            'actions' => $actions,
        ));
    }


    /**
     * Displays a form to edit an existing page entity.
     *
     * @Route("/{id}/edit", name="page_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Page $page)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');
        $user = $this->getUser();
        if ($user->getBusinessUnit()->getCode() != BusinessUnit::ASW) {
            $page->setDateRequest(new \DateTime())
                ->setBusinessUnit($user->getBusinessUnit())
                ->setUser($user);
        }
        $editForm = $this->createForm(PageType::class, $page,
            [
                'businessUnit' => $user->getBusinessUnit(),
                'user' => $user
            ]
        );
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            // Set up date requested as NOW, user and BU
            if ($user->getBusinessUnit()->getCode() != BusinessUnit::ASW) {
                $page->setDateModified(new \DateTime())
                    ->setBusinessUnit($user->getBusinessUnit())
                    ->setUser($user);
            }
            $page->setUser($user);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', '<strong>Excellent!</strong> Data updated successfully!');
            if ($editForm->get('saveAndGoToBuild')->isClicked()) {
                return $this->redirectToRoute('page_block_list', array('id' => $page->getId()));
            }
            if ($editForm->get('saveAndGoToEdit')->isClicked()) {
                return $this->redirectToRoute('page_block_edit_individual_list', array('id' => $page->getId()));
            }
            return $this->redirectToRoute('page_edit', array('id' => $page->getId()));

        }

        $actions =
            [
                'list' =>
                    [
                        'path' => 'page_index',
                        'name' => 'Check pages',
                        'btn' => 'primary',
                    ],
                'edit' =>
                    [
                        'path' => 'page_edit',
                        'name' => 'Edit information',
                        'btn' => 'info',
                    ]
            ];


        return $this->render('PDPGeneratorBundle:Page:edit.html.twig', array(
            'page' => $page,
            'form' => $editForm->createView(),
            'actions' => $actions,
        ));
    }


    /**
     * Lists all archive pages entities.
     *
     * @Route("/archives", name="page_archive_index")
     * @Method({"GET"})
     */
    public function archiveAction()
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');
        $user = $this->getUser();
        $bu = $user->getBusinessUnit()->getCode();

        $em = $this->getDoctrine()->getManager();
        $pages = $em->getRepository('PDPGeneratorBundle:Page')->getInfoForTable($bu, true);

        $actions['archive'] = [
            'path' => 'page_unarchived_page',
            'name' => 'Un-archive  page',
            'btn' => 'warning',
        ];

        return $this->render('PDPGeneratorBundle:Page:archiveIndex.html.twig', array(
            'data' => $pages,
            'actions' => $actions
        ));
    }

    /**
     * Unarchived a page entity.
     *
     * @Route("/{id}/unarchived", name="page_unarchived_page")
     * @Method("GET")
     */
    public function unarchivedPageAction(Request $request, Page $page)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $page->setArchive(false);
        $em->persist($page);
        $em->flush();

        return $this->redirectToRoute('page_index');
    }

    /**
     * Archive a page entity.
     *
     * @Route("/{id}/archive", name="page_archive_page")
     * @Method("GET")
     */
    public function archivePageAction(Request $request, Page $page)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $page->setArchive(true);
        $em->persist($page);
        $em->flush();

        return $this->redirectToRoute('page_index');
    }

    /**
     * Download page
     *
     * @Route("/{page}/download", name="page_download")
     * @Method({"GET"})
     */
    public function downloadAction(Request $request, Page $page)
    {
        $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');

        try {

            $downloadGenerator = $this->get('pdp_generator.download_pdpgenerator');
            $downloadGenerator
                ->setPage($page)
                ->MoveFilesToCache();
            $zipName = $downloadGenerator->createZip();
            $zipPath = $downloadGenerator->getZipPath();

            $response = new Response(file_get_contents($zipPath . $zipName));
            $response->headers->set('Content-Type', 'application/zip');
            $response->headers->set('Content-Disposition', 'attachment;filename="' . $zipName . '"');
            $response->headers->set('Content-length', filesize($zipPath . $zipName));

            return $response;
        } catch (\Exception $e) {
            $this->addFlash('danger', '<strong>Uppss!</strong> It looks like it could not generate the zip, please make sure that <strong>you have filled</strong> in the important data on the forms!');
            $this->addFlash('danger', '<strong>Error</strong> ' . $e->getMessage());
            return $this->redirectToRoute('page_block_edit_individual_list', array('id' => $page->getId()));

        }
    }

    /**
     * Displays a preview of a page entity.
     *
     * @Route("/{page}/preview", name="page_preview")
     * @Method({"GET"})
     */
    public function previewAction(Request $request, Page $page)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_PDP_GENERATOR', null, 'Unable to access this page!');

            $previewPDPGenerator = $this->get('pdp_generator.preview_pdpgenerator');
            $previewPDPGenerator->setPage($page);
            $html = $previewPDPGenerator->createHtml();

            return new Response($html, 200);
        } catch (\Exception $e) {
            $this->addFlash('danger', '<strong>Uppss!</strong> It looks like it could not generate the preview, please make sure that you filled in the mandatory fields for each block');
            $this->addFlash('danger', '<strong>Error</strong> ' . $e->getMessage());
            return $this->redirectToRoute('page_block_edit_individual_list', array('id' => $page->getId()));

        }
    }

    /**
     * Unarchived a email entity.
     *
     * @Route("/{id}/duplicate", name="page_duplicate")
     * @Method("GET")
     */
    public function duplicateAction(Request $request, Page $page)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $filer = $this->get('app.filer');
        $em = $this->getDoctrine()->getManager();
        // Copy Email
        $newEmail = clone $page;
        // clean up sensitive fields Email
        $newEmail->clearId()->setTitle('TempName_'.rand(1000, 9999))->setBlocks(null);
        // Duplicate blocks and assign to new page
        foreach ($page->getBlocks() as $block) {
            $newBlock = clone $block;
            $newBlock->clearId();
            $newEmail->addBlock($newBlock);
        }
        $em->persist($newEmail);
        $em->flush();
        // Copy asset folder
        $filer->duplicateAssetFolder($page->getId(), $newEmail->getId());
        return $this->redirectToRoute('page_index');
    }
}

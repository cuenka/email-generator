<?php

namespace PDPGeneratorBundle\Form;

use PDPGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockTwoColumnsImagesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', FileType::class, array(
            'required' => true,
            'label' => 'Image to upload',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt1',
            TextType::class,
            array(
                'label' => 'Alt text',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of  image',
                ),
            )
        )->add('file2', FileType::class, array(
            'required' => true,
            'label' => 'Image to upload',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt2',
            TextType::class,
            array(
                'label' => 'Alt text image 2',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of  image',
                ),
            )
        )->add(
            'save',
            SubmitType::class,
            [
                'label' => 'Save',
                'attr' =>
                    [
                        'class' => 'btn btn-success mt-1'
                    ]
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'PDPGeneratorBundle\Entity\BlockTwoColumnsImages',
                'dataBlock' => null,
                'dataPage' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'PDPgeneratorbundle_BlockTwoColumnsImagesType';
    }


}

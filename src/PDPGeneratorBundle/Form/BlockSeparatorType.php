<?php

namespace PDPGeneratorBundle\Form;

use PDPGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockSeparatorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'gap',
            ChoiceType::class,
            array(
                'choices'  => BlockHelper::getHeights(),
                'required' => true,
                'label' => 'Gap (in pixels)',
                'attr' => array(
                    'class' => 'mt-1',
                ),
            )
        )->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' =>
                        [
                            'class' => 'btn btn-success mt-1'
                        ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'PDPGeneratorBundle\Entity\BlockSeparator',
                'dataBlock' => null,
                'dataPage' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'PDPgeneratorbundle_BlockSeparatorType';
    }


}

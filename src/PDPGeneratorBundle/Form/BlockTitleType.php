<?php

namespace PDPGeneratorBundle\Form;

use PDPGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockTitleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, array(
            'required' => true,
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'ctaTextColour',
            TextType::class,
            array(
                'required' => true,
                'label' => 'Title text colour',

                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'title text colour Hex code',
                ),
            )
        )->add(
            'fontFamily',
            ChoiceType::class,
            array(
                'choices' => BlockHelper::getFonts(),
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                ),
            )
        )->add(
            'fontSize',
            IntegerType::class,
            array(
                'required' => true,
                'label' => 'font size (in pixels)',
                'attr' => array(
                    'class' => 'mt-1',
                    'min' => 11
                ),
            )
        )->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' =>
                        [
                            'class' => 'btn btn-success mt-1'
                        ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'PDPGeneratorBundle\Entity\BlockTitle',
                'dataBlock' => null,
                'dataPage' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'PDPgeneratorbundle_BlockTitleType';
    }


}

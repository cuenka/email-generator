<?php

namespace PDPGeneratorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockYoutubeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'youtubeID',
            TextType::class,
            array(
                'label' => 'Youtube ID (NO URL)',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Youtube ID only',
                ),
            )
        )->add(
            'save',
            SubmitType::class,
            [
                'label' => 'Save',
                'attr' =>
                    [
                        'class' => 'btn btn-success mt-1'
                    ]
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'PDPGeneratorBundle\Entity\BlockYoutube',
                'dataBlock' => null,
                'dataPage' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'PDPgeneratorbundle_BlockYoutubeType';
    }


}

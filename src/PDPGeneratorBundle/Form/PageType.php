<?php

namespace PDPGeneratorBundle\Form;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use PDPGeneratorBundle\Helper\BlockHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, array(
            'label' => 'Page title',
            'required' => true,
            'attr' => array(
                'class' => '',
                'placeholder' => 'Write your title or PDP name',
            )
        ))->add('description', TextareaType::class, array(
            'required' => false,
            'attr' => array(
                'class' => '',
                'placeholder' => 'What is this PDP about?',
            )
        ))->add(
            'fontFamily',
            ChoiceType::class,
            array(
                'label' => 'Page main font family',
                'choices'  => BlockHelper::getFonts(),
                'required' => true,
                'attr' => array(
                    'class' => 'form-control mt-1',
                ),
            )
        );
        if ($options['businessUnit']->getCode() == BusinessUnit::ASW) {
            $builder->add('businessUnit', EntityType::class,
                [
                    'required' => true,
                    'class' => 'AuthenticationBundle:BusinessUnit',
                    'choice_label' => function (BusinessUnit $entity = null) {
                        return $entity ? $entity->getName() : '';
                    },
                    'choice_value' => function (BusinessUnit $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr' => array(
                        'class' => ''
                    )
                ]);
            $builder->add('user', EntityType::class,
                [
                    'class' => 'AuthenticationBundle:User',
                    'choice_label' => function (User $entity = null) {
                        return $entity ? $entity->getName() : '';
                    },
                    'choice_value' => function (User $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'data' => $options['user'],
                    'attr' =>
                        [
                            'class' => 'disabled',
                            'disabled' => 'disabled'
                        ]
                ]);
        }

        $builder->add('saveAndReturn', SubmitType::class,
            array(
                'label' => 'Save and return',
                'attr' => array(
                    'class' => 'btn btn-success mt-3'
                )
            ))->add('saveAndGoToBuild', SubmitType::class,
            array(
                'label' => 'Save go to Edit Layout',
                'attr' => array(
                    'class' => 'btn btn-success mt-3'
                )
            ))->add('saveAndGoToEdit', SubmitType::class,
            array(
                'label' => 'Save go to Edit content',
                'attr' => array(
                    'class' => 'btn btn-success mt-3 '
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PDPGeneratorBundle\Entity\Page',
            'businessUnit' => null,
            'user' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'pdpgeneratorbundle_page';
    }


}

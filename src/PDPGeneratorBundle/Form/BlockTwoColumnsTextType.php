<?php

namespace PDPGeneratorBundle\Form;

use PDPGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockTwoColumnsTextType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('textLeft', TextareaType::class, array(
            'required' => true,
            'label' => "Text Area left column",
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block text left (You can type HTML...)'
            )
        ))->add('textRight', TextareaType::class, array(
            'required' => false,
            'label' => "Text Area right column",

            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block text right (You can type HTML...)'
                )
        ))->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' =>
                        [
                            'class' => 'btn btn-success mt-1'
                        ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'PDPGeneratorBundle\Entity\BlockTwoColumnsText',
                'dataBlock' => null,
                'dataPage' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function GetId($code = null)
    {
        return $this->getBlockPrefix() . "_". $code. "_text_" . random_int(10, 9999);

    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'PDPgeneratorbundle_BlockTwoColumnsTextType';
    }


}

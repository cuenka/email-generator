<?php

namespace PDPGeneratorBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use PDPGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockTwoColumnsImageLeftOrRightType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, array(
            'required' => true,
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Block title',
            )
        ))->add('text', CKEditorType::class, array(
            'required' => true,
            'config_name' => 'my_config',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block text',
                'id' => $this->GetId()
            )
        ))->add('file', FileType::class, array(
            'required' => true,
            'label' => 'Image to upload',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt1',
            TextType::class,
            array(
                'label' => 'Alt text',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of  image',
                ),
            )
        )->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' =>
                        [
                            'class' => 'btn btn-success mt-1'
                        ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'PDPGeneratorBundle\Entity\BlockTwoColumnsImageLeftOrRight',
                'dataBlock' => null,
                'dataPage' => null
            ]
        );
    }
    /**
     * {@inheritdoc}
     */
    public function GetId()
    {
        return $this->getBlockPrefix() . '_twocolumnsimageleftorright_' . random_int(10, 9999);

    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'PDPgeneratorbundle_BlockTwoColumnsImageLeftOrRightType';
    }


}

<?php

namespace PDPGeneratorBundle\Helper;


use PDPGeneratorBundle\Entity\Block;
use PDPGeneratorBundle\Entity\BlockGap;
use PDPGeneratorBundle\Entity\BlockHero;
use PDPGeneratorBundle\Entity\BlockQuoteText;
use PDPGeneratorBundle\Entity\BlockSeparator;
use PDPGeneratorBundle\Entity\BlockText;
use PDPGeneratorBundle\Entity\BlockThreeColumnsImages;
use PDPGeneratorBundle\Entity\BlockThreeColumnsText;
use PDPGeneratorBundle\Entity\BlockTitle;
use PDPGeneratorBundle\Entity\BlockTwoColumnsImageLeftOrRight;
use PDPGeneratorBundle\Entity\BlockTwoColumnsImages;
use PDPGeneratorBundle\Entity\BlockTwoColumnsText;
use PDPGeneratorBundle\Entity\BlockYoutube;
use PDPGeneratorBundle\Form\BlockGapType;
use PDPGeneratorBundle\Form\BlockHeroType;
use PDPGeneratorBundle\Form\BlockQuoteTextType;
use PDPGeneratorBundle\Form\BlockSeparatorType;
use PDPGeneratorBundle\Form\BlockTextType;
use PDPGeneratorBundle\Form\BlockThreeColumnsImagesType;
use PDPGeneratorBundle\Form\BlockThreeColumnsTextType;
use PDPGeneratorBundle\Form\BlockTitleType;
use PDPGeneratorBundle\Form\BlockTwoColumnsImageLeftOrRightType;
use PDPGeneratorBundle\Form\BlockTwoColumnsImagesType;
use PDPGeneratorBundle\Form\BlockTwoColumnsTextType;
use PDPGeneratorBundle\Form\BlockYoutubeType;


/**
 * Class BlockHelper
 * @package PDPGeneratorBundle\Helper
 */
class BlockHelper
{
    const BLOCK_GAP = 'just_gap';
    const BLOCK_HERO = 'hero';
    const BLOCK_YOUTUBE = 'youtube';
    const BLOCK_TEXT = 'just_text';
    const BLOCK_TITLE = 'just_title';
    const BLOCK_QUOTE_TEXT = 'quote_text';
    const BLOCK_SEPARATOR = 'separator';
    const BLOCK_THREE_COLUMNS_IMAGES = 'three_columns_images';
    const BLOCK_TWO_COLUMNS_IMAGES = 'two_columns_images';
    const BLOCK_TWO_COLUMNS_IMAGE_LEFT = 'two_columns_image_left';
    const BLOCK_TWO_COLUMNS_IMAGE_RIGHT = 'two_columns_image_right';
    const BLOCK_TWO_COLUMNS_TEXT = 'two_columns_text';
    const BLOCK_THREE_COLUMNS_TEXT = 'three_columns_text';

    /**
     * @param $block
     * @param int $order
     * @return Block|BlockGap
     */
    public static function newInstance($block, $order = 1)
    {

        switch ($block) {
            case self::BLOCK_GAP:
                $instance = new BlockGap($block, $order);
                break;
            case self::BLOCK_HERO:
                $instance = new BlockHero($block, $order);
                break;
            case self::BLOCK_YOUTUBE:
                $instance = new BlockYoutube($block, $order);
                break;
            case self::BLOCK_TEXT:
                $instance = new BlockText($block, $order);
                break;
            case self::BLOCK_TITLE:
                $instance = new BlockTitle($block, $order);
                break;
            case self::BLOCK_QUOTE_TEXT:
                $instance = new BlockQuoteText($block, $order);
                break;
            case self::BLOCK_SEPARATOR:
                $instance = new BlockSeparator($block, $order);
                break;
            case self::BLOCK_THREE_COLUMNS_IMAGES:
                $instance = new BlockThreeColumnsImages($block, $order);
                break;
            case self::BLOCK_TWO_COLUMNS_IMAGES:
                $instance = new BlockTwoColumnsImages($block, $order);
                break;
            case self::BLOCK_TWO_COLUMNS_IMAGE_LEFT:
            case self::BLOCK_TWO_COLUMNS_IMAGE_RIGHT:
                $instance = new BlockTwoColumnsImageLeftOrRight($block, $order);
                break;
            case self::BLOCK_TWO_COLUMNS_TEXT:
                $instance = new BlockTwoColumnsText($block, $order);
                break;
            case self::BLOCK_THREE_COLUMNS_TEXT:
                $instance = new BlockThreeColumnsText($block, $order);
                break;
            default:
                $instance = new Block($block, $order);
        }

        return $instance;
    }

    /**
     * @param string $code
     * @return string or null
     */
    public function getFormType(string $code)
    {
        switch ($code) {
            case self::BLOCK_GAP:
                return BlockGapType::class;
                break;
            case self::BLOCK_HERO:
                return BlockHeroType::class;
                break;
            case self::BLOCK_YOUTUBE:
                return BlockYoutubeType::class;
                break;
            case self::BLOCK_TEXT:
                return BlockTextType::class;
                break;
            case self::BLOCK_TITLE:
                return BlockTitleType::class;
                break;
            case self::BLOCK_QUOTE_TEXT:
                return BlockQuoteTextType::class;
                break;
            case self::BLOCK_SEPARATOR:
                return BlockSeparatorType::class;
                break;
            case self::BLOCK_THREE_COLUMNS_IMAGES:
                return BlockThreeColumnsImagesType::class;
                break;
            case self::BLOCK_TWO_COLUMNS_IMAGES:
                return BlockTwoColumnsImagesType::class;
                break;
            case self::BLOCK_TWO_COLUMNS_IMAGE_LEFT:
            case self::BLOCK_TWO_COLUMNS_IMAGE_RIGHT:
                return BlockTwoColumnsImageLeftOrRightType::class;
                break;
            case self::BLOCK_TWO_COLUMNS_TEXT:
                return BlockTwoColumnsTextType::class;
                break;
            case self::BLOCK_THREE_COLUMNS_TEXT:
                return BlockThreeColumnsTextType::class;
                break;
            default:
                return null;
        }

    }


    public static function getFonts()
    {
        $fonts =
            [
                'Arial, Helvetica, sans-serif' => 'Arial, Helvetica, sans-serif',
                '\'Raleway\', sans-serif' => '\'Raleway\', sans-serif',
                '"Times New Roman", Times, serif' => '"Times New Roman", Times, serif'
            ];

        return $fonts;
    }

    public static function getAlign()
    {
        $align =
            [
                'left' => 'left',
                'right' => 'right',
                'center' => 'center',
                'justify' => 'justify'
            ];

        return $align;
    }

    public static function getHeights()
    {
        $heights =
            [
                'Very Small  (10px)' => '10',
                'Small (30px)' => '30',
                'Medium (50px)' => '50',
                'Large  (80px)' => '80',
                'Extra Large  (100px)' => '100',
            ];

        return $heights;
    }

    public static function getAvaiableBlocks()
    {
        $blocks =
            [
                BlockHelper::BLOCK_GAP => 'Gap',
                BlockHelper::BLOCK_HERO => 'Hero block',
                BlockHelper::BLOCK_YOUTUBE => 'Youtube block',
                BlockHelper::BLOCK_TEXT => 'Just Text',
                BlockHelper::BLOCK_TITLE => 'Title',
                BlockHelper::BLOCK_QUOTE_TEXT => 'Quote Text',
                BlockHelper::BLOCK_SEPARATOR => 'Separator',
                BlockHelper::BLOCK_THREE_COLUMNS_IMAGES => '3 columns of images',
                BlockHelper::BLOCK_TWO_COLUMNS_IMAGES => '2 columns of images',
                BlockHelper::BLOCK_TWO_COLUMNS_IMAGE_LEFT => '2 columns with an image on the left',
                BlockHelper::BLOCK_TWO_COLUMNS_IMAGE_RIGHT => '2 columns with an image on the right',
                BlockHelper::BLOCK_TWO_COLUMNS_TEXT => '2 columns with text on both',
                BlockHelper::BLOCK_THREE_COLUMNS_TEXT => '3 columns with text on all columns',
            ];

        return $blocks;
    }

    /**
     * It return the array with width and height
     * @param string $code
     * @return array
     *
     */
    public function getCorrectImageDimensions($code)
    {
        $dimensions['width'] = null;
        $dimensions['height'] = null;
        switch ($code) {
            case self::BLOCK_HERO:
                $dimensions['width'] = 620;
                $dimensions['height'] = 350;
                break;
            case self::BLOCK_TWO_COLUMNS_IMAGES:
                $dimensions['width'] = 300;
                $dimensions['height'] = 220;
                break;
            case self::BLOCK_TWO_COLUMNS_IMAGE_LEFT:
            case self::BLOCK_TWO_COLUMNS_IMAGE_RIGHT:
                $dimensions['width'] = 300;
                $dimensions['height'] = 350;
                break;
            case self::BLOCK_THREE_COLUMNS_IMAGES:
                $dimensions['width'] = 200;
                $dimensions['height'] = 185;
                break;
        }

        return $dimensions;

    }
}
<?php

namespace PDPGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockTwoColumnsText
 * @package PDPGeneratorBundle\Entity
 * @ORM\Table(name="pdp_generator_block_two_columns_text")
 * @ORM\Entity(repositoryClass="PDPGeneratorBundle\Repository\BlockRepository")
 */
class BlockTwoColumnsText extends Block
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="text_one", type="text", unique=false)
     */
    private $textLeft;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="text_two", type="text", unique=false)
     */
    private $textRight;

    /**
     * @return string
     */
    public function getTextLeft()
    {
        return $this->textLeft;
    }

    /**
     * @param string $textLeft
     */
    public function setTextLeft($textLeft)
    {
        $this->textLeft = $textLeft;
    }

    /**
     * @return string
     */
    public function getTextRight()
    {
        return $this->textRight;
    }

    /**
     * @param string $textRight
     */
    public function setTextRight($textRight)
    {
        $this->textRight = $textRight;
    }



}
<?php

namespace PDPGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockText
 * @package PDPGeneratorBundle\Entity
 * @ORM\Table(name="pdp_generator_block_text")
 * @ORM\Entity(repositoryClass="PDPGeneratorBundle\Repository\BlockRepository")
 */
class BlockText extends Block
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="text_one", type="text", unique=false)
     */
    private $text;

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

}
<?php

namespace PDPGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockTitle
 * @package PDPGeneratorBundle\Entity
 * @ORM\Table(name="pdp_generator_block_title")
 * @ORM\Entity(repositoryClass="PDPGeneratorBundle\Repository\BlockRepository")
 */
class BlockTitle extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_text_colour", type="string", length=7, unique=false)
     */
    private $ctaTextColour;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="font_family", type="string", length=60, unique=false)
     */
    private $fontFamily;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="font_size", type="integer", unique=false)
     */
    private $fontSize;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCtaTextColour()
    {
        return $this->ctaTextColour;
    }

    /**
     * @param string $ctaTextColour
     */
    public function setCtaTextColour($ctaTextColour)
    {
        $this->ctaTextColour = $ctaTextColour;
    }

    /**
     * @return string
     */
    public function getFontFamily()
    {
        return $this->fontFamily;
    }

    /**
     * @param string $fontFamily
     */
    public function setFontFamily($fontFamily)
    {
        $this->fontFamily = $fontFamily;
    }

    /**
     * @return string
     */
    public function getFontSize()
    {
        return $this->fontSize;
    }

    /**
     * @param string $fontSize
     */
    public function setFontSize($fontSize)
    {
        $this->fontSize = $fontSize;
    }



}
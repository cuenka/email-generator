<?php

namespace PDPGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockYoutube
 * @package PDPGeneratorBundle\Entity
 * @ORM\Table(name="pdp_generator_block_youtube")
 * @ORM\Entity(repositoryClass="PDPGeneratorBundle\Repository\BlockRepository")
 */
class BlockYoutube extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="youtube_id", type="string", length=40, unique=false)
     */
    private $youtubeID;

    /**
     * @return string
     */
    public function getYoutubeID()
    {
        return $this->youtubeID;
    }

    /**
     * @param string $youtubeID
     */
    public function setYoutubeID($youtubeID)
    {
        $this->youtubeID = $youtubeID;
    }


}
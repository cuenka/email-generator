<?php

namespace PDPGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockSeparator
 * @package PDPGeneratorBundle\Entity
 * @ORM\Table(name="pdp_generator_block_separator")
 * @ORM\Entity(repositoryClass="PDPGeneratorBundle\Repository\BlockRepository")
 */
class BlockSeparator extends Block
{
    /**
     * @var integer
     *
     * @ORM\Column(name="gap", type="integer", unique=false)
     */
    private $gap;

    /**
     * @return int
     */
    public function getGap()
    {
        return $this->gap;
    }

    /**
     * @param int $gap
     */
    public function setGap($gap)
    {
        $this->gap = $gap;
    }
}
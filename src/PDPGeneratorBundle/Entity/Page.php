<?php

namespace PDPGeneratorBundle\Entity;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use PDPGeneratorBundle\Helper\BlockHelper;

/**
 * Email, Is the main entity of an email, An email has a template, blocks, BU...
 *
 * @ORM\Table(name="pdp_generator_email",indexes={@ORM\Index(name="search_idx", columns={"id"})})
 * @ORM\Entity(repositoryClass="PDPGeneratorBundle\Repository\PageRepository")
 * @UniqueEntity("title",errorPath="title",
 *     message="This title has been use in other page, this must be unique"
 * )
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * One Email has One Business unit.
     * @ORM\ManyToOne(targetEntity="AuthenticationBundle\Entity\BusinessUnit")
     * @ORM\JoinColumn(name="business_unit", referencedColumnName="id")
     */
    private $businessUnit;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=100, unique=true)
     */
    private $title;

    /**
     * One Email has One user.
     * @ORM\ManyToOne(targetEntity="AuthenticationBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modified", type="datetime", nullable=true)
     */
    private $dateModified;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;



    /**
     * One Email has One Business unit.
     * @ORM\ManyToMany(targetEntity="PDPGeneratorBundle\Entity\Block", inversedBy="email", cascade={"persist", "remove" })
     * @ORM\JoinTable(name="pdp_generator_email_blocks")
     * @ORM\OrderBy({"order" = "ASC", "id" = "ASC"})
     */
    private $blocks;


    /**
     * @var boolean archive
     *
     * @ORM\Column(name="archive", type="boolean", nullable=false,  options={"default" : 0})
     */
    private $archive = 0;

    /**
     * @var boolean archive
     *
     * @ORM\Column(name="font_family", type="string", length=100, options={"default" : "Arial, Helvetica, sans-serif"})
     */
    private $fontFamily;



    /**
     * Email constructor.
     */
    public function __construct()
    {
        $this->dateCreated = new \DateTime();
        $this->dateModified = new \DateTime();
        $this->blocks = new ArrayCollection();
        $this->archive = 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * clear id
     *
     * @return Block
     */
    public function clearId()
    {
        $this->id = null;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBusinessUnit()
    {
        return $this->businessUnit;
    }

    /**
     * @param mixed $businessUnit
     */
    public function setBusinessUnit($businessUnit)
    {
        $this->businessUnit = $businessUnit;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @param $dateModified
     * @return $this Page
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isArchive()
    {
        return $this->archive;
    }

    /**
     * @param bool $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * @return string
     */
    public function getFontFamily()
    {
        return $this->fontFamily;
    }

    /**
     * @param string $fontFamily
     */
    public function setFontFamily($fontFamily)
    {
        $this->fontFamily = $fontFamily;
    }

    /**
     * @param array $blocks
     * @return array
     */
    public function initialiseBlocks(Array $blocks)
    {
        return [];
    }

    /**
     * @param Block $block
     * @return $this
     */
    public function addBlock(Block $block)
    {
        $this->blocks[] = $block;
        return $this;
    }

    /**
     * @param mixed $blocks
     */
    public function setBlocks($blocks)
    {
        $this->blocks = $blocks;
    }
    /**
     * @return mixed
     */
    public function getBlocks()
    {
        return $this->blocks;
    }


    /**
     * @param Block $block
     * @return bool
     */
    public function deleteBlock(Block $block)
    {
        return $this->blocks->removeElement($block);
    }

    /**
     * @return boolean
     */
    public function hasBlocks()
    {
        if ($this->getBlocks()) {
            return true;
        }

        return false;
    }

    /**
     * @param array $updatedBlocks
     * @return array
     */
    public function ammendBlocks(Array $updatedBlocks)
    {
        $blocks = $this->getBlocks();
        foreach ($updatedBlocks as $updatedBlock) {
            // If is set means that the block exist already so is going to be updated,
            // otherwise it is a new block added to the email
            if (isset($updatedBlock['id'])) {
                foreach ($blocks as $block) {
                    if ($block instanceof Block) {
                    }
                }
            } else {
                $newBlock = BlockHelper::newInstance($updatedBlock['name']);
                $newBlock
                    ->setOrder($updatedBlock['order'])
                    ->setCode($updatedBlock['name']);
                $this->addBlock($newBlock);
            }
        }


        return [];
    }
}


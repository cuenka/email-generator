<?php

namespace EmailGeneratorBundle\Controller;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use EmailGeneratorBundle\Entity\Layout;
use EmailGeneratorBundle\Form\LayoutType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Layout controller. This Controller is in charge of CRUD email Layout entity, so people can edit header and footer of layouts.
 *
 * @Route("email/layout")
 */
class LayoutController extends Controller
{


    /**
     * New layout potentially available entities.
     * @param Request $request
     * @return Response
     * @Route("/list", name="email_layout_index")
     * @Method({"GET", "POST"})
     */
    public function indexLayoutAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');
        $user = $this->getUser();
        $bu = $user->getBusinessUnit()->getCode();

        $em = $this->getDoctrine()->getManager();
        $layouts = $em->getRepository('EmailGeneratorBundle:Layout')->getInfoForTable();

        $actions = $this->getActions();

        return $this->render('EmailGeneratorBundle:Layout:index.html.twig',
            [
                'data' => $layouts,
                'actions' => $actions,
            ]
        );
    }

    /**
     * New layout potentially available entities.
     * @TODO validate that BU+template do not already exist, otherwise it will be duplicated header and footers
     * @param Request $request
     * @return Response
     * @Route("/new", name="email_layout_new")
     * @Method({"GET", "POST"})
     */
    public function newLayoutAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $layout = new Layout();

        $form = $this->createForm(LayoutType::class, $layout, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($layout);
            $em->flush();
            $this->addFlash('success', '<strong>Excellent!</strong> Data saved successfully, now you can work on the layout!');
            return $this->redirectToRoute('email_layout_edit', array('layout' => $layout->getId()));
        }
        $actions = $this->getActions();

        return $this->render('EmailGeneratorBundle:Layout:new.html.twig',
            [
                'layout' => $layout,
                'form' => $form->createView(),
                'actions' => $actions,
            ]
        );
    }

    /**
     * Edit all layouts potentially available entities.
     * @param Request $request
     * @return Response
     * @Route("/{layout}/edit", name="email_layout_edit")
     * @Method({"GET", "POST"})
     */
    public function editLayoutAction(Request $request, Layout $layout)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $form = $this->createForm(LayoutType::class, $layout, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($layout);
            $em->flush();
            $this->addFlash('success', '<strong>Excellent!</strong> Data saved successfully, now you can work on the layout!');
            return $this->redirectToRoute('email_layout_edit', array('layout' => $layout->getId()));
        }

        $actions = $this->getActions();

        return $this->render('EmailGeneratorBundle:Layout:new.html.twig',
            [
                'layout' => $layout,
                'form' => $form->createView(),
                'actions' => $actions,

            ]);
    }

    /**
     * Deletes a email entity.
     *
     * @Route("/{layout}/delete", name="email_layout_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Layout $layout)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $em->remove($layout);
        $em->flush();

        return $this->redirectToRoute('email_layout_index');
    }


    /**
     * This function is normally used to get the endpoints for actions for table display
     * @return array
     */
    private function getActions()
    {
        $actions =
            [
                'list' =>
                    [
                        'path' => 'email_layout_index',
                        'name' => 'Check layouts',
                        'btn' => 'primary',
                    ],
                'edit' =>
                    [
                        'path' => 'email_layout_edit',
                        'name' => '<i class="fa fa-map"></i> Edit header & footer',
                        'btn' => 'info btn-sm',
                    ],
                'delete' =>
                    [
                        'path' => 'email_layout_delete',
                        'name' => 'Delete layout',
                        'btn' => 'danger',
                    ]
            ];

        return $actions;

    }
}

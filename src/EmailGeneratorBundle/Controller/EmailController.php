<?php

namespace EmailGeneratorBundle\Controller;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use EmailGeneratorBundle\Entity\Email;
use EmailGeneratorBundle\Form\EmailType;
use EmailGeneratorBundle\Form\PreHeaderType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Email controller. This Controller is in charge of CRUD email entity, preview, download, send test emails. everything about the entity Email.
 *
 * @Route("email")
 */
class EmailController extends Controller
{
    /**
     * Lists all email entities.
     *
     * @Route("", name="email_index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $user = $this->getUser();
        $bu = $user->getBusinessUnit()->getCode();

        $em = $this->getDoctrine()->getManager();
        $emails = $em->getRepository('EmailGeneratorBundle:Email')->getInfoForTable($bu, false);

        $actions =
            [
                'edit' =>
                    [
                        'path' => 'email_edit',
                        'name' => 'Edit information',
                        'btn'  => 'info',
                    ],
                'block_list' =>
                    [
                        'path' => 'email_block_list',
                        'name' => 'Edit Layout',
                        'btn' => 'info',
                    ],
                'block_edit_individual_list' =>
                    [
                        'path' => 'email_block_edit_individual_list',
                        'name' => 'Edit content',
                        'btn' => 'info',
                    ],
                'archive' =>
                    [
                        'path' => 'email_archive_email',
                        'name' => 'Archive email',
                        'btn' => 'dark',
                    ],
                'duplicate' =>
                    [
                        'path' => 'email_duplicate',
                        'name' => 'Duplicate',
                        'btn' => 'warning',
                    ]
            ];


        return $this->render('EmailGeneratorBundle:Email:index.html.twig', array(
            'data' => $emails,
            'actions' => $actions
        ));
    }



    /**
     * Lists all archive email entities.
     *
     * @Route("/archives", name="email_archive_index")
     * @Method({"GET"})
     */
    public function archiveAction()
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $user = $this->getUser();
        $bu = $user->getBusinessUnit()->getCode();

        $em = $this->getDoctrine()->getManager();
        $emails = $em->getRepository('EmailGeneratorBundle:Email')->getInfoForTable($bu, true);

        $actions['archive'] = [
            'path' => 'email_unarchived_email',
            'name' => 'Un-archive  email',
            'btn' => 'warning',
        ];

        return $this->render('EmailGeneratorBundle:Email:archiveIndex.html.twig', array(
            'data' => $emails,
            'actions' => $actions
        ));
    }

    /**
     * Creates a new email entity.
     *
     * @Route("/new", name="email_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $email = new Email();
        $templates = $em->getRepository('EmailGeneratorBundle:Template')->getTemplatesByBU($user->getBusinessUnit()->getCode());

        $form = $this->createForm(EmailType::class, $email,
            [
                'businessUnit' => $user->getBusinessUnit(),
                'user' => $user,
                'templates' => $templates
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email->setDateRequest(new \DateTime());
            // Set up date requested as NOW, user and BU
            if ($user->getBusinessUnit()->getCode() != BusinessUnit::ASW) {
                $email->setBusinessUnit($user->getBusinessUnit())
                    ->setUser($user);
            } else {
                $email->setUser($user);
            }

            $em->persist($email);
            $em->flush();
            $this->addFlash('success', '<strong>Excellent!</strong> Data saved successfully, now you can work on the layout!');
            if ($form->get('saveAndGoToBuild')->isClicked()) {
                return $this->redirectToRoute('email_block_list', array('id' => $email->getId()));
            }
            if ($form->get('saveAndGoToEdit')->isClicked()) {
                return $this->redirectToRoute('email_block_edit_individual_list', array('id' => $email->getId()));
            }
            return $this->redirectToRoute('email_edit', array('id' => $email->getId()));
        }
        $actions = $this->getActions();

        return $this->render('EmailGeneratorBundle:Email:new.html.twig', array(
            'email' => $email,
            'form' => $form->createView(),
            'actions' => $actions,
        ));
    }


    /**
     * Displays a form to edit an existing email entity.
     *
     * @Route("/{id}/edit", name="email_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Email $email)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $user = $this->getUser();
        if ($user->getBusinessUnit()->getCode() != BusinessUnit::ASW) {
            $email->setDateRequest(new \DateTime())
                ->setBusinessUnit($user->getBusinessUnit())
                ->setUser($user);
        }
        $editForm = $this->createForm(EmailType::class, $email,
            [
                'businessUnit' => $user->getBusinessUnit(),
                'user' => $user
            ]
        );
        $dateRequest = $email->getDateRequest();
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            // Set up date requested as NOW, user and BU
            if ($user->getBusinessUnit()->getCode() != BusinessUnit::ASW) {
                $email->setDateRequest(new \DateTime())
                    ->setBusinessUnit($user->getBusinessUnit())
                    ->setUser($user);
            }
            $email->setDateRequest($dateRequest);
            $email->setUser($user);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', '<strong>Excellent!</strong> Data updated successfully!');
            if ($editForm->get('saveAndGoToBuild')->isClicked()) {
                return $this->redirectToRoute('email_block_list', array('id' => $email->getId()));
            }
            if ($editForm->get('saveAndGoToEdit')->isClicked()) {
                return $this->redirectToRoute('email_block_edit_individual_list', array('id' => $email->getId()));
            }
            return $this->redirectToRoute('email_edit', array('id' => $email->getId()));

        }

        $actions = $this->getActions();

        return $this->render('EmailGeneratorBundle:Email:edit.html.twig', array(
            'email' => $email,
            'form' => $editForm->createView(),
            'actions' => $actions,
        ));
    }

    /**
     * Unarchived a email entity.
     *
     * @Route("/{id}/duplicate", name="email_duplicate")
     * @Method("GET")
     */
    public function duplicateAction(Request $request, Email $email)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $filer = $this->get('app.filer');
        $ftp = $this->get('app.uploader');
        $em = $this->getDoctrine()->getManager();
        // Copy Email
        $newEmail = clone $email;
        // clean up sensitive fields Email
        $newEmail->clearId()->setUtm(null)->setJobTitle('TempJobNumber_'.rand(1000, 9999))->setBlocks(null);
        // Duplicate blocks and assign to new email
        foreach ($email->getBlocks() as $block) {
            $newBlock = clone $block;
            $newBlock->clearId();
            $newEmail->addBlock($newBlock);
        }
        $em->persist($newEmail);
        $em->flush();
        // Copy asset folder
        if ($this->getParameter('is_elab') == false) {
            $filer->duplicateAssetFolder($email->getId(), $newEmail->getId());
        } else {
//            @TODO $ftp->duplicateFolderOnFTP($email->getId(), $newEmail->getId(), '/email_generator/');
            $this->addFlash('warning', '<strong>Warning!</strong> Assets have not been duplicated as it is not possible to copy folders on the CDN at the moment');

        }
        return $this->redirectToRoute('email_index');
    }

    /**
     * Unarchived a email entity.
     *
     * @Route("/{id}/unarchived", name="email_unarchived_email")
     * @Method("GET")
     */
    public function unarchivedEmailAction(Request $request, Email $email)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $email->setArchive(false);
        $em->persist($email);
        $em->flush();

        return $this->redirectToRoute('email_index');
    }

    /**
     * Archive a email entity.
     *
     * @Route("/{id}/archive", name="email_archive_email")
     * @Method("GET")
     */
    public function archiveEmailAction(Request $request, Email $email)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $email->setArchive(true);
        $em->persist($email);
        $em->flush();

        return $this->redirectToRoute('email_index');
    }

    /**
     * Deletes a email entity.
     *
     * @Route("/{id}/delete", name="email_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Email $email)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $em->remove($email);
        $em->flush();

        return $this->redirectToRoute('email_index');
    }


    /**
     * Download email
     *
     * @Route("/{email}/download", name="email_download")
     * @Method({"GET"})
     */
    public function downloadAction(Request $request, Email $email)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $downloadGenerator = $this->get('email_generator.download_generator');
            $downloadGenerator
                ->setEmail($email)
                ->MoveFilesToCache();
            $zipName = $downloadGenerator->createZip();
            $zipPath = $downloadGenerator->getZipPath();

            $response = new Response(file_get_contents($zipPath . $zipName));
            $response->headers->set('Content-Type', 'application/zip');
            $response->headers->set('Content-Disposition', 'attachment;filename="' . $zipName . '"');
            $response->headers->set('Content-length', filesize($zipPath . $zipName));

            return $response;
    }

    /**
     * Displays a form to edit an existing email entity.
     *
     * @Route("/{email}/preview", name="email_preview")
     * @Method({"GET"})
     */
    public function previewAction(Request $request, Email $email)
    {
            $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');

            $previewGenerator = $this->get('email_generator.preview_generator');
            $previewGenerator->setEmail($email);
            $html = $previewGenerator->createHtml();

            return new Response($html, 200);
    }


    /**
     * send email
     *
     * @Route("/{email}/send-email", name="email_send-email")
     * @Method({"GET"})
     */
    public function emailAction(Request $request, Email $email)
    {
        try {
            $user = $this->getUser();
            $emails = [];
            if ($user->getIsActive()) {
                $emails  = explode(",", $user->getExtraEmails());
                array_push($emails, $user->getEmail());
            }
            $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
            $mailer = $this->get('mailer');

            $downloadGenerator = $this->get('email_generator.download_generator');
            $downloadGenerator->setEmail($email);
            $html = $downloadGenerator->createHtml();

            $message = (new \Swift_Message($email->getSubject()))
                ->setFrom('no-reply@apps.elabeurope.co.uk')
                ->setTo($emails)
                ->setBody($html, 'text/html');

            $mailer->send($message);
            foreach ($emails as $e){
                $this->addFlash('success', '<strong>yeah!</strong> Email sent to ' . $e);
            }

            return $this->redirectToRoute('email_block_edit_individual_list', array('id' => $email->getId()));

        } catch (\Exception $e) {
            $this->addFlash('danger', '<strong>Error</strong> ' . $e->getMessage());
            return $this->redirectToRoute('email_block_edit_individual_list', array('id' => $email->getId()));

        }
    }

    /**
     * This function is normally used to get the endpoints for actions for table display
     * @return array
     */
    private function getActions()
    {
        $actions =
            [
                'list' =>
                    [
                        'path' => 'email_index',
                        'name' => 'Check emails',
                        'btn' => 'primary',
                    ],
                'edit' =>
                    [
                        'path' => 'email_edit',
                        'name' => '<i class="fa fa-envelope"></i> Edit information',
                        'btn' => 'info btn-sm',
                    ],
                'block' =>
                    [
                        'path' => 'email_block_list',
                        'name' => '<i class="fa fa-list-ol"></i> Edit Layout',
                        'btn' => 'info',
                    ],
                'blockEdit' =>
                    [
                        'path' => 'email_block_edit_individual_list',
                        'name' => '<i class="fa fa-list-alt"></i> Edit email data',
                        'btn' => 'warning',
                    ],
                'duplicate' =>
                    [
                        'path' => 'email_duplicate',
                        'name' => '<i class="fa fa-clone"></i> Duplicate',
                        'btn' => 'warning disabled',
                    ],
                'delete' =>
                    [
                        'path' => 'email_delete',
                        'name' => 'Delete email',
                        'btn' => 'danger',
                    ]
            ];

        return $actions;

    }
}

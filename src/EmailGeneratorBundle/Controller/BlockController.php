<?php

namespace EmailGeneratorBundle\Controller;

use AuthenticationBundle\Entity\BusinessUnit;
use EmailGeneratorBundle\EmailGeneratorBundle;
use EmailGeneratorBundle\Entity\Block;
use EmailGeneratorBundle\Entity\Email;
use EmailGeneratorBundle\Form\BlockTitleFullLineType;
use EmailGeneratorBundle\Helper\BlockHelper;
use EmailGeneratorBundle\Helper\TemplateHelper;
use EmailGeneratorBundle\Repository\BlockRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Block controller. This controller is in charge of display blocks and CRUD blocks
 *
 * @Route("email/block")
 */
class BlockController extends Controller
{
    /**
     * List all email blocks avaiables and selected
     *
     * @Route("/list/{id}", name="email_block_list")
     * @Method({"GET","POST"})
     */
    public function listAction(Request $request, Email $email)
    {
        $availableBlocks = TemplateHelper::getAvaiableBlocks($email->getTemplate());
        $selectedBlocks = $email->getBlocks();
        return $this->render('EmailGeneratorBundle:Block:list.html.twig',
            [
                'email' => $email,
                'availableBlocks' => $availableBlocks,
                'selectedBlocks' => $selectedBlocks,
            ]);
    }

    /**
     * Add blocks, and re-order blocks from list page
     *
     * @Route("/edit/{id}", name="email_block_edit_list")
     * @Method({"GET","POST"})
     */
    public function editListAction(Request $request, Email $email)
    {
        $availableBlocks = TemplateHelper::getAvaiableBlocks($email->getTemplate());
        $em = $this->getDoctrine()->getManager();
        $selectedBlocks = $email->getBlocks();
        $blockHelper = new BlockHelper();
        $data = $request->get('data', null);
        foreach ($data as $key => $block) {
            if (isset($block['id'])) {
                foreach ($selectedBlocks as $selectedBlock) {
                    if ($selectedBlock->getId() === intval($block['id'])) {
                        $selectedBlock->setOrder($block['order']);
                    }
                }
            } else {
                $newBlock = $blockHelper->newInstance($block['name'], $block['order']);
                $email->addBlock($newBlock);
                $em->persist($newBlock);
            }
        }

        $em->persist($email);
        $em->flush();
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse('OK', 200);
        }

        return $this->render('EmailGeneratorBundle:Block:list.html.twig',
            [
                'email' => $email,
                'availableBlocks' => $availableBlocks,
                'selectedBlocks' => $selectedBlocks,
            ]);
    }

    /**
     * Edit block
     *
     * @Route("/individual/{id}", name="email_block_edit_individual_list")
     * @Method({"GET","POST"})
     */
    public function editIndividualListAction(Request $request, Email $email)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');

        $this->checkWarnings($email);
        return $this->render('EmailGeneratorBundle:Block:edit.html.twig', array(
            'email' => $email,
        ));
    }

    /**
     * Edit block
     *
     * @Route("/{email}/individual/block/{block}", name="email_block_edit_individual_block")
     * @Method({"GET","POST"})
     */
    public function editIndividualBlockAction(Request $request, Email $email, Block $block)
    {
        $this->denyAccessUnlessGranted('ROLE_EMAIL_GENERATOR', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $blockHelper = new BlockHelper();
        $ftpURl = BusinessUnit::getDomain($email->getBusinessUnit()->getCode()).'/elab' . $this->getParameter('ftp_destination') . DIRECTORY_SEPARATOR . 'email_generator' . DIRECTORY_SEPARATOR . $email->getId();
        $cdnUploader = $this->get('email_generator.cdn_manager');
        $imageManager = $this->get('app.image_manager');
        // some block does not need form
        if (is_null($blockHelper->getFormType($block->getCode()))) {
            return $this->render('EmailGeneratorBundle:Block:_emptyBlock.html.twig', array(
                'blockId' => $block->getId()
            ));
        }

        $form = $this->createForm($blockHelper->getFormType($block->getCode()), $block,
            [
                'action' => $this->generateUrl('email_block_edit_individual_block',
                    [
                        'email' => $email->getId(),
                        'block' => $block->getId()
                    ]
                ),
                'method' => 'POST',
                'dataBlock' => $block->getId(),
                'dataEmail' => $email->getId(),
            ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $ftpPath = $this->getParameter('ftp_destination') . DIRECTORY_SEPARATOR . 'email_generator' . DIRECTORY_SEPARATOR . $email->getId();
                if (method_exists($block, 'getFile') && (!is_null($block->getFile()))) {
                    $file = $block->getFile();
                    $fileName = $block->generateFileName($file->getClientOriginalName(), '.' . $file->guessExtension());

                    if ($this->getParameter('is_elab') == true) {

                        // Image manipulation and validation
                        if ($imageManager->hasRightDimensions($imageManager->getImageDimensions($file->getPathname()), $blockHelper->getCorrectImageDimensions($block->getCode())) !== true) {
                            $imageManager->resizeByWidth($file->getPath(), $file->getFilename(), $blockHelper->getCorrectImageDimensions($block->getCode()));
                            $this->addFlash('success', '<strong>Resized!</strong> The upload image has been resized and optimised!');
                        } else {
                            $this->addFlash('danger', '<strong>Image could not be resized!</strong> Check if your image is OK, try again, if problem persist, ask administrator');
                        }

                        // Image upload to FTP
                        if (
                            $cdnUploader->deletePreviousUpload($ftpPath, $block->getImage()) == false ||
                            $cdnUploader->uploadToCDN($file->getPathname(), null, $ftpPath, $fileName ) == false ) {
                            $this->addFlash('danger', '<strong>Issue on the cloud!</strong> Upload of the File return an error, try again or contact administrator if this issue persist');
                        }
                    } else {
                        // Image save on server
                        $file = $block->getFile();
                        $fileName = $block->generateFileName() . '.' . $file->guessExtension();
                        $cachePath = $this->getParameter('images_path');
                        $file->move($cachePath . DIRECTORY_SEPARATOR . $email->getId(), $fileName);
                        $this->validateImage($cachePath . DIRECTORY_SEPARATOR . $email->getId() . DIRECTORY_SEPARATOR . $fileName, $block);
                    }
                    $block->setImage($fileName);

                }

                if (method_exists($block, 'getFile2') && (!is_null($block->getFile2()))) {
                    $file = $block->getFile2();
                    $fileName = $block->generateFileName($file->getClientOriginalName(), '.' . $file->guessExtension());

                    if ($this->getParameter('is_elab') == true) {

                        // Image manipulation and validation
                        if ($imageManager->hasRightDimensions($imageManager->getImageDimensions($file->getPathname()), $blockHelper->getCorrectImageDimensions($block->getCode())) !== true) {
                            $imageManager->resizeByWidth($file->getPath(), $file->getFilename(), $blockHelper->getCorrectImageDimensions($block->getCode()));
                            $this->addFlash('success', '<strong>Resized!</strong> The upload image has been resized and optimised!');
                        } else {
                            $this->addFlash('danger', '<strong>Image could not be resized!</strong> Check if your image is OK, try again, if problem persist, ask administrator');
                        }

                        // Image upload to FTP
                        if (
                            $cdnUploader->deletePreviousUpload($ftpPath, $block->getImage2()) == false ||
                            $cdnUploader->uploadToCDN($file->getPathname(), null, $ftpPath, $fileName ) == false ) {
                            $this->addFlash('danger', '<strong>Issue on the cloud!</strong> Upload of the File return an error, try again or contact administrator if this issue persist');
                        }
                    } else {
                        // Image save on server
                        $file = $block->getFile2();
                        $fileName = $block->generateFileName() . '.' . $file->guessExtension();
                        $cachePath = $this->getParameter('images_path');
                        $file->move($cachePath . DIRECTORY_SEPARATOR . $email->getId(), $fileName);
                        $this->validateImage($cachePath . DIRECTORY_SEPARATOR . $email->getId() . DIRECTORY_SEPARATOR . $fileName, $block);
                    }
                    $block->setImage2($fileName);

                }

                if (method_exists($block, 'getFile3') && (!is_null($block->getFile3()))) {
                    $file = $block->getFile3();
                    $fileName = $block->generateFileName($file->getClientOriginalName(), '.' . $file->guessExtension());

                    if ($this->getParameter('is_elab') == true) {

                        // Image manipulation and validation
                        if ($imageManager->hasRightDimensions($imageManager->getImageDimensions($file->getPathname()), $blockHelper->getCorrectImageDimensions($block->getCode())) !== true) {
                            $imageManager->resizeByWidth($file->getPath(), $file->getFilename(), $blockHelper->getCorrectImageDimensions($block->getCode()));
                            $this->addFlash('success', '<strong>Resized!</strong> The upload image has been resized and optimised!');
                        } else {
                            $this->addFlash('danger', '<strong>Image could not be resized!</strong> Check if your image is OK, try again, if problem persist, ask administrator');
                        }

                        // Image upload to FTP
                        if (
                            $cdnUploader->deletePreviousUpload($ftpPath, $block->getImage3()) == false ||
                            $cdnUploader->uploadToCDN($file->getPathname(), null, $ftpPath, $fileName ) == false ) {
                            $this->addFlash('danger', '<strong>Issue on the cloud!</strong> Upload of the File return an error, try again or contact administrator if this issue persist');
                        }
                    } else {
                        // Image save on server
                        $file = $block->getFile3();
                        $fileName = $block->generateFileName() . '.' . $file->guessExtension();
                        $cachePath = $this->getParameter('images_path');
                        $file->move($cachePath . DIRECTORY_SEPARATOR . $email->getId(), $fileName);
                        $this->validateImage($cachePath . DIRECTORY_SEPARATOR . $email->getId() . DIRECTORY_SEPARATOR . $fileName, $block);
                    }
                    $block->setImage3($fileName);

                }

                $em->persist($block);
                $em->flush();
                $this->addFlash('success', '<strong>Excellent!</strong> The block that just tried to save (' .
                    str_replace('_', ' ', $block->getCode()) . ') has been updated!');
                return $this->redirectToRoute('email_block_edit_individual_list', array('id' => $email->getId()));
            } else {
                $this->addFlash('warning', '<strong>Upps!</strong> The block that just tried to save, (' .
                    str_replace('_', ' ', $block->getCode()) . '), contain errors');
                $errors = $form->getErrors(true, true);
                foreach ($errors as $error) {
                    $this->addFlash('danger', '<strong>Upps!</strong> ' . $error->getMessage());
                }
                return $this->redirectToRoute('email_block_edit_individual_list', array('id' => $email->getId()));
            }
        }

        return $this->render('EmailGeneratorBundle:Block:_editIndividual.html.twig', array(
            'form' => $form->createView(),
            'block' => $block,
            'emailId' => $email->getId(),
            'ftpURL' => $ftpURl
        ));
    }


    /**
     * Edit block
     *
     * @Route("/edit/{email}/customise", name="email_block_customise")
     * @Method({"GET","POST"})
     */
    public
    function customiseAction(Request $request, Email $email)
    {
        $blocks = $request->get('data', []);
        $em = $this->getDoctrine()->getManager();
        $email->ammendBlocks($blocks);
        $em->persist($email);
        $em->flush();

        return new JsonResponse('OK', 200);

    }


    /**
     * Edit block
     *
     * @Route("/{email}/delete/{block}", name="email_block_delete")
     * @Method({"GET","POST"})
     */
    public
    function deleteAction(Request $request, Email $email, Block $block)
    {
        if ($email->deleteBlock($block)) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($email);
            $em->flush();
            return new JsonResponse('OK', 200);

        } else {
            return new JsonResponse('ERROR', 200);
        }

    }

    /**
     * Edit block
     *
     * @Route("/get/sku", name="email_block_sku")
     * @Method({"POST"})
     *    */
    public function skuAction(Request $request)
    {
        $variables = $request->request->all();
        $emailID = isset($variables['emailID']) ? $variables['emailID'] : 0;
        $blockID = isset($variables['blockID']) ? $variables['blockID'] : 0;
        $dataContext = isset($variables['dataContext']) ? $variables['dataContext'] : 0;
        $id = isset($variables['skuId']) ? $variables['skuId'] : 0;

        if (($id == 0) || ($emailID == 0)) {
            return $this->render('EmailGeneratorBundle:Block:_skuValues.html.twig', ['data' => false]);
        }

        $em = $this->getDoctrine()->getManager();
        $email = $em->getRepository('EmailGeneratorBundle:Email')->find($emailID);
        $block = $em->getRepository('EmailGeneratorBundle:Block')->find($blockID);
        $bu = $email->getBusinessUnit()->getCode();
        $url = BusinessUnit::amendURL($id, $bu);
        $productCrawler = $this->get('app.product_crawler');
        $productCrawler->setUrl($url);
        $product = $productCrawler->marionnaudProduct();

        return $this->render('EmailGeneratorBundle:Block:_skuValues.html.twig',
            [
                'data' => true,
                'product' => $product,
                'block' => $block,
                'skuInfo' => $block->getSkuInfo($dataContext),
                'domain' => BusinessUnit::getDomain($bu)
            ]
        );

    }

    /**
     * @param $file
     */
    private function validateImage($file, Block $block)
    {
        $blockHelper = new BlockHelper();
        $imageInfo = getimagesize($file);
        $rightDimensions = $blockHelper->getCorrectImageDimensions($block->getCode());
        if (!isset($imageInfo[0]) ||
            !isset($imageInfo[1]) ||
            ($imageInfo[0] != $rightDimensions['width']) ||
            ($imageInfo[1] != $rightDimensions['height'])) {
            $this->addFlash('warning', '<strong>Easy there!</strong> The dimensions of the image are: ' .
                $imageInfo[0] . 'px by ' . $imageInfo[1] . 'px, and should be ' . $rightDimensions['width'] . 'px by ' .
                $rightDimensions['height'] . 'px');
        }
    }

    /**
     * @param Email $email
     */
    private
    function checkWarnings(Email $email)
    {
        if (is_null($email->getUtm())) {
            $this->addFlash('warning', '<strong>WOW!</strong> Nothing wrong but keep in mind that I do not have an UTM!');

        }
    }
}
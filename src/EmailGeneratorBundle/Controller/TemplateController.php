<?php

namespace EmailGeneratorBundle\Controller;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use EmailGeneratorBundle\Entity\Template;
use EmailGeneratorBundle\Form\TemplateType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Template controller. This Controller is in charge of CRUD email Template entity, so people can edit header and footer of templates.
 *
 * @Route("email/template")
 */
class TemplateController extends Controller
{


    /**
     * New template potentially available entities.
     * @param Request $request
     * @return Response
     * @Route("/list", name="email_template_index")
     * @Method({"GET", "POST"})
     */
    public function indexTemplateAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');
        $user = $this->getUser();
        $bu = $user->getBusinessUnit()->getCode();

        $em = $this->getDoctrine()->getManager();
        $templates = $em->getRepository('EmailGeneratorBundle:Template')->getInfoForTable();

        $actions = $this->getActions();

        return $this->render('EmailGeneratorBundle:Template:index.html.twig',
            [
                'data' => $templates,
                'actions' => $actions,
            ]
        );
    }

    /**
     * New template potentially available entities.
     * @TODO validate that BU+template do not already exist, otherwise it will be duplicated header and footers
     * @param Request $request
     * @return Response
     * @Route("/new", name="email_template_new")
     * @Method({"GET", "POST"})
     */
    public function newTemplateAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $template = new Template();

        $form = $this->createForm(TemplateType::class, $template, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($template);
            $em->flush();
            $this->addFlash('success', '<strong>Excellent!</strong> Data saved successfully, now you can work on the template!');
            return $this->redirectToRoute('email_template_edit', array('template' => $template->getId()));
        }
        $actions = $this->getActions();

        return $this->render('EmailGeneratorBundle:Template:new.html.twig',
            [
                'template' => $template,
                'form' => $form->createView(),
                'actions' => $actions,
            ]
        );
    }

    /**
     * Edit all templates potentially available entities.
     * @param Request $request
     * @return Response
     * @Route("/{template}/edit", name="email_template_edit")
     * @Method({"GET", "POST"})
     */
    public function editTemplateAction(Request $request, Template $template)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $form = $this->createForm(TemplateType::class, $template, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($template);
            $em->flush();
            $this->addFlash('success', '<strong>Excellent!</strong> Data saved successfully, now you can work on the template!');
            return $this->redirectToRoute('email_template_edit', array('template' => $template->getId()));
        }

        $actions = $this->getActions();

        return $this->render('EmailGeneratorBundle:Template:new.html.twig',
            [
                'template' => $template,
                'form' => $form->createView(),
                'actions' => $actions,

            ]);
    }

    /**
     * Deletes a email entity.
     *
     * @Route("/{template}/delete", name="email_template_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Template $template)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();
        $em->remove($template);
        $em->flush();

        return $this->redirectToRoute('email_template_index');
    }


    /**
     * This function is normally used to get the endpoints for actions for table display
     * @return array
     */
    private function getActions()
    {
        $actions =
            [
                'list' =>
                    [
                        'path' => 'email_template_index',
                        'name' => 'Check templates',
                        'btn' => 'primary',
                    ],
                'edit' =>
                    [
                        'path' => 'email_template_edit',
                        'name' => '<i class="fa fa-newspaper-o"></i> Edit template',
                        'btn' => 'info btn-sm',
                    ],
                'delete' =>
                    [
                        'path' => 'email_template_delete',
                        'name' => 'Delete template',
                        'btn' => 'danger',
                    ]
            ];

        return $actions;

    }
}

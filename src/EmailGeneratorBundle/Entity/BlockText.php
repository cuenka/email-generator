<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class BlockHero
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_block_title_full_line")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockText extends Block
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="text_one", type="text", unique=false)
     */
    private $text;

    /**
     * @var string
     * @ORM\Column(name="background_colour_block", length=7, type="text", unique=false, nullable=true)
     */
    private $backgroundColourBlock;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_text_colour", type="string", length=7, unique=false)
     */
    private $ctaTextColour;

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getBackgroundColourBlock()
    {
        return $this->backgroundColourBlock;
    }

    /**
     * @param string $backgroundColourBlock
     */
    public function setBackgroundColourBlock($backgroundColourBlock)
    {
        $this->backgroundColourBlock = $backgroundColourBlock;
    }

    /**
     * @return string
     */
    public function getCtaTextColour()
    {
        return $this->ctaTextColour;
    }

    /**
     * @param string $ctaTextColour
     */
    public function setCtaTextColour($ctaTextColour)
    {
        $this->ctaTextColour = $ctaTextColour;
    }


    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->getText()) || is_null($this->getText())) {
            return true;
        }
        return false;
    }

}
<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockHeaderOptionOne
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_header_Option_one")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockHeaderOptionOne extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_text_colour", type="string", length=7, unique=false)
     */
    private $textColour;

    /**
     * @var string
     * @ORM\Column(name="text_one", type="text", unique=false, nullable=true)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="image_1", type="string", length=255, unique=false)
     */
    private $image1;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_1", type="string", length=255, unique=false)
     */
    private $imageAlt1;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file;

    /**
     * @var string
     * @ORM\Column(name="background_colour_block", length=7, type="text", unique=false, nullable=true)
     */
    private $backgroundColourBlock;

    /**
     * @var string
     * @ORM\Column(name="cta_one_text", type="string", length=200, unique=false, nullable=true)
     */
    private $ctaText;

    /**
     * @var string
     * @Assert\Url()
     * @ORM\Column(name="cta_one_url", type="string", length=255, unique=false, nullable=true)
     */
    private $ctaUrl;

    /**
     * @var string
     * @ORM\Column(name="cta_one_url_title", type="string", length=255, unique=false, nullable=true)
     */
    private $ctaUrlTitle;

    /**
     * @var string
     * @ORM\Column(name="choice",type="boolean", options={"default":"0"})

     */
    private $needCTa;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * @return string
     */
    public function getAlignBlock()
    {
        return $this->alignBlock;
    }

    /**
     * @param string $alignBlock
     */
    public function setAlignBlock($alignBlock)
    {
        $this->alignBlock = $alignBlock;
    }



    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getBackgroundColourBlock()
    {
        return $this->backgroundColourBlock;
    }

    /**
     * @param string $backgroundColourBlock
     */
    public function setBackgroundColourBlock($backgroundColourBlock)
    {
        $this->backgroundColourBlock = $backgroundColourBlock;
    }


    /**
     * @return string
     */
    public function getCtaText()
    {
        return $this->ctaText;
    }

    /**
     * @param string $ctaText
     */
    public function setCtaText($ctaText)
    {
        $this->ctaText = $ctaText;
    }

    /**
     * @return string
     */
    public function getCtaBackgroundColour()
    {
        return $this->ctaBackgroundColour;
    }

    /**
     * @param string $ctaBackgroundColour
     */
    public function setCtaBackgroundColour($ctaBackgroundColour)
    {
        $this->ctaBackgroundColour = $ctaBackgroundColour;
    }

    /**
     * @return string
     */
    public function getCtaTextColour()
    {
        return $this->ctaTextColour;
    }

    /**
     * @param string $ctaTextColour
     */
    public function setCtaTextColour($ctaTextColour)
    {
        $this->ctaTextColour = $ctaTextColour;
    }

    /**
     * @return string
     */
    public function getCtaUrl()
    {
        return $this->ctaUrl;
    }

    /**
     * @param string $ctaUrl
     */
    public function setCtaUrl($ctaUrl)
    {
        $this->ctaUrl = $ctaUrl;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image1;
    }

    /**
     * @param string $image1
     */
    public function setImage($image1)
    {
        $this->image1 = $image1;
    }


    /**
     * @return string
     */
    public function getImageAlt1()
    {
        return $this->imageAlt1;
    }

    /**
     * @param string $imageAlt1
     */
    public function setImageAlt1($imageAlt1)
    {
        $this->imageAlt1 = $imageAlt1;
    }

    /**
     * @return string
     */
    public function getCtaUrlTitle()
    {
        return $this->ctaUrlTitle;
    }

    /**
     * @param string $ctaUrlTitle
     */
    public function setCtaUrlTitle($ctaUrlTitle)
    {
        $this->ctaUrlTitle = $ctaUrlTitle;
    }

    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->getText()) || is_null($this->getText())) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getNeedCTa()
    {
        return $this->needCTa;
    }

    /**
     * @param string $needCTa
     */
    public function setNeedCTa($needCTa)
    {
        $this->needCTa = $needCTa;
    }

    /**
     * @return string
     */
    public function getTextColour()
    {
        return $this->textColour;
    }

    /**
     * @param string $textColour
     */
    public function setTextColour($textColour)
    {
        $this->textColour = $textColour;
    }


}
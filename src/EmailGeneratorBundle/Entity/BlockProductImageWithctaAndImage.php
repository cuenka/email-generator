<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockHero
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_block_title_full_line")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockProductImageWithctaAndImage extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="image_1", type="string", length=255, unique=false)
     */
    private $image1;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_1", type="string", length=255, unique=false)
     */
    private $imageAlt1;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="image_2", type="string", length=255, unique=false)
     */
    private $image2;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_2", type="string", length=255, unique=false)
     */
    private $imageAlt2;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file2;

    /**
     * @var string
     * @ORM\Column(name="cta_one_text", type="string", length=200, unique=false, nullable=true)
     */
    private $ctaText;

    /**
     * @var string
     * @Assert\Url()
     * @ORM\Column(name="cta_one_url", type="string", length=255, unique=false, nullable=true)
     */
    private $ctaUrl;

    /**
     * @var string
     * @ORM\Column(name="cta_one_url_title", type="string", length=255, unique=false, nullable=true)
     */
    private $ctaUrlTitle;


    /**
     * @var string
     * @ORM\Column(name="choice",type="boolean", options={"default":"0"})

     */
    private $needCTa;

    /**
     * @var string
     * @ORM\Column(name="background_colour_block", length=7, type="text", unique=false, nullable=true)
     */
    private $backgroundColourBlock;

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image1;
    }

    /**
     * @param string $image1
     */
    public function setImage($image1)
    {
        $this->image1 = $image1;
    }

    /**
     * @return string
     */
    public function getImageAlt1()
    {
        return $this->imageAlt1;
    }

    /**
     * @param string $imageAlt1
     */
    public function setImageAlt1($imageAlt1)
    {
        $this->imageAlt1 = $imageAlt1;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * @param string $image2
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    }

    /**
     * @return string
     */
    public function getImageAlt2()
    {
        return $this->imageAlt2;
    }

    /**
     * @param string $imageAlt2
     */
    public function setImageAlt2($imageAlt2)
    {
        $this->imageAlt2 = $imageAlt2;
    }

    /**
     * @return File
     */
    public function getFile2()
    {
        return $this->file2;
    }

    /**
     * @param File $file2
     */
    public function setFile2($file2)
    {
        $this->file2 = $file2;
    }

    /**
     * @return string
     */
    public function getCtaText()
    {
        return $this->ctaText;
    }

    /**
     * @param string $ctaText
     */
    public function setCtaText($ctaText)
    {
        $this->ctaText = $ctaText;
    }

    /**
     * @return string
     */
    public function getCtaUrl()
    {
        return $this->ctaUrl;
    }

    /**
     * @param string $ctaUrl
     */
    public function setCtaUrl($ctaUrl)
    {
        $this->ctaUrl = $ctaUrl;
    }

    /**
     * @return string
     */
    public function getCtaUrlTitle()
    {
        return $this->ctaUrlTitle;
    }

    /**
     * @param string $ctaUrlTitle
     */
    public function setCtaUrlTitle($ctaUrlTitle)
    {
        $this->ctaUrlTitle = $ctaUrlTitle;
    }

    /**
     * @return string
     */
    public function getNeedCTa()
    {
        return $this->needCTa;
    }

    /**
     * @param string $needCTa
     */
    public function setNeedCTa($needCTa)
    {
        $this->needCTa = $needCTa;
    }

    /**
     * @return string
     */
    public function getBackgroundColourBlock()
    {
        return $this->backgroundColourBlock;
    }

    /**
     * @param string $backgroundColourBlock
     */
    public function setBackgroundColourBlock($backgroundColourBlock)
    {
        $this->backgroundColourBlock = $backgroundColourBlock;
    }
}
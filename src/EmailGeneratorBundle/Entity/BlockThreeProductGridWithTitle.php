<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockThreeProductGridWithTitle
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_three_product_grid_with_title")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockThreeProductGridWithTitle extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, unique=false)
     */
    private $title;

    /**
     * @var string
     * @Assert\Regex(pattern="/^[0-9]/", message="Product 1 ID only must contains numbers, information not saved")
     * @ORM\Column(name="product_1", type="string", length=20, unique=false)
     */
    private $product1;

    /**
     * @var string
     * @ORM\Column(name="product_1_brand", type="string", length=255, unique=false)
     */
    private $product1Brand;

    /**
     * @var string
     * @ORM\Column(name="product_1_name", type="string", length=255, unique=false)
     */
    private $product1Name;

    /**
     * @var string
     * @ORM\Column(name="product_1_image", type="text", length=255, unique=false)
     */
    private $product1Image;

    /**
     * @var string Price is string because different Bu use comma or dot and currency
     * @ORM\Column(name="product_1_price", type="string", length=20, unique=false)
     */
    private $product1Price;

    /**
     * @var string Promo price is string because different Bu use comma or dot and currency
     * @ORM\Column(name="product_1_promo_price", type="string", length=20, unique=false)
     */
    private $product1PromoPrice;

    /**
     * @var string
     * @Assert\Regex(pattern="/^[0-9]/", message="Product 2 ID only must contains numbers, information not saved")
     * @ORM\Column(name="product_2", type="string", length=20, unique=false)
     */
    private $product2;

    /**
     * @var string
     * @ORM\Column(name="product_2_brand", type="string", length=255, unique=false)
     */
    private $product2Brand;

    /**
     * @var string
     * @ORM\Column(name="product_2_name", type="string", length=255, unique=false)
     */
    private $product2Name;

    /**
     * @var string
     * @ORM\Column(name="product_2_image", type="text", length=255, unique=false)
     */
    private $product2Image;

    /**
     * @var string Price is string because different Bu use comma or dot and currency
     * @ORM\Column(name="product_2_price", type="string", length=20, unique=false)
     */
    private $product2Price;

    /**
     * @var string Promo price is string because different Bu use comma or dot and currency
     * @ORM\Column(name="product_2_promo_price", type="string", length=20, unique=false)
     */
    private $product2PromoPrice;

    /**
     * @var string
     * @Assert\Regex(pattern="/^[0-9]/", message="Product 3 ID only must contains numbers, information not saved")
     * @ORM\Column(name="product_3", type="string", length=20, unique=false)
     */
    private $product3;

    /**
     * @var string
     * @ORM\Column(name="product_3_brand", type="string", length=255, unique=false)
     */
    private $product3Brand;

    /**
     * @var string
     * @ORM\Column(name="product_3_name", type="string", length=255, unique=false)
     */
    private $product3Name;

    /**
     * @var string
     * @ORM\Column(name="product_3_image", type="text", length=255, unique=false)
     */
    private $product3Image;

    /**
     * @var string Price is string because different Bu use comma or dot and currency
     * @ORM\Column(name="product_3_price", type="string", length=20, unique=false)
     */
    private $product3Price;

    /**
     * @var string Promo price is string because different Bu use comma or dot and currency
     * @ORM\Column(name="product_3_promo_price", type="string", length=20, unique=false)
     */
    private $product3PromoPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_text", type="string", length=200, unique=false)
     */
    private $ctaText;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getProduct1()
    {
        return $this->product1;
    }

    /**
     * @param string $product1
     */
    public function setProduct1($product1)
    {
        $this->product1 = $product1;
    }

    /**
     * @return string
     */
    public function getProduct2()
    {
        return $this->product2;
    }

    /**
     * @param string $product2
     */
    public function setProduct2($product2)
    {
        $this->product2 = $product2;
    }

    /**
     * @return string
     */
    public function getProduct3()
    {
        return $this->product3;
    }

    /**
     * @param string $product3
     */
    public function setProduct3($product3)
    {
        $this->product3 = $product3;
    }

    /**
     * @return string
     */
    public function getProduct1Brand()
    {
        return $this->product1Brand;
    }

    /**
     * @param string $product1Brand
     */
    public function setProduct1Brand($product1Brand)
    {
        $this->product1Brand = $product1Brand;
    }

    /**
     * @return string
     */
    public function getProduct1Name()
    {
        return $this->product1Name;
    }

    /**
     * @param string $product1Name
     */
    public function setProduct1Name($product1Name)
    {
        $this->product1Name = $product1Name;
    }

    /**
     * @return string
     */
    public function getProduct1Image()
    {
        return $this->product1Image;
    }

    /**
     * @param string $product1Image
     */
    public function setProduct1Image($product1Image)
    {
        $this->product1Image = $product1Image;
    }

    /**
     * @return string
     */
    public function getProduct1Price()
    {
        return $this->product1Price;
    }

    /**
     * @param string $product1Price
     */
    public function setProduct1Price($product1Price)
    {
        $this->product1Price = $product1Price;
    }

    /**
     * @return string
     */
    public function getProduct1PromoPrice()
    {
        return $this->product1PromoPrice;
    }

    /**
     * @param string $product1PromoPrice
     */
    public function setProduct1PromoPrice($product1PromoPrice)
    {
        $this->product1PromoPrice = $product1PromoPrice;
    }

    /**
     * @return string
     */
    public function getProduct2Brand()
    {
        return $this->product2Brand;
    }

    /**
     * @param string $product2Brand
     */
    public function setProduct2Brand($product2Brand)
    {
        $this->product2Brand = $product2Brand;
    }

    /**
     * @return string
     */
    public function getProduct2Name()
    {
        return $this->product2Name;
    }

    /**
     * @param string $product2Name
     */
    public function setProduct2Name($product2Name)
    {
        $this->product2Name = $product2Name;
    }

    /**
     * @return string
     */
    public function getProduct2Image()
    {
        return $this->product2Image;
    }

    /**
     * @param string $product2Image
     */
    public function setProduct2Image($product2Image)
    {
        $this->product2Image = $product2Image;
    }

    /**
     * @return string
     */
    public function getProduct2Price()
    {
        return $this->product2Price;
    }

    /**
     * @param string $product2Price
     */
    public function setProduct2Price($product2Price)
    {
        $this->product2Price = $product2Price;
    }

    /**
     * @return string
     */
    public function getProduct2PromoPrice()
    {
        return $this->product2PromoPrice;
    }

    /**
     * @param string $product2PromoPrice
     */
    public function setProduct2PromoPrice($product2PromoPrice)
    {
        $this->product2PromoPrice = $product2PromoPrice;
    }

    /**
     * @return string
     */
    public function getProduct3Brand()
    {
        return $this->product3Brand;
    }

    /**
     * @param string $product3Brand
     */
    public function setProduct3Brand($product3Brand)
    {
        $this->product3Brand = $product3Brand;
    }

    /**
     * @return string
     */
    public function getProduct3Name()
    {
        return $this->product3Name;
    }

    /**
     * @param string $product3Name
     */
    public function setProduct3Name($product3Name)
    {
        $this->product3Name = $product3Name;
    }

    /**
     * @return string
     */
    public function getProduct3Image()
    {
        return $this->product3Image;
    }

    /**
     * @param string $product3Image
     */
    public function setProduct3Image($product3Image)
    {
        $this->product3Image = $product3Image;
    }

    /**
     * @return string
     */
    public function getProduct3Price()
    {
        return $this->product3Price;
    }

    /**
     * @param string $product3Price
     */
    public function setProduct3Price($product3Price)
    {
        $this->product3Price = $product3Price;
    }

    /**
     * @return string
     */
    public function getProduct3PromoPrice()
    {
        return $this->product3PromoPrice;
    }

    /**
     * @param string $product3PromoPrice
     */
    public function setProduct3PromoPrice($product3PromoPrice)
    {
        $this->product3PromoPrice = $product3PromoPrice;
    }


    /**
     * @return string
     */
    public function getCtaText()
    {
        return $this->ctaText;
    }

    /**
     * @param string $ctaText
     */
    public function setCtaText($ctaText)
    {
        $this->ctaText = $ctaText;
    }


    /**
     * @param string $priceColour
     */
    public function setPriceColour($priceColour)
    {
        $this->priceColour = $priceColour;
    }

    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->getProduct1()) || is_null($this->getProduct1())) {
            return true;
        }
        return false;
    }

}
<?php

namespace EmailGeneratorBundle\Entity;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use EmailGeneratorBundle\Helper\BlockHelper;

/**
 * Email, Is the main entity of an email, An email has a template, blocks, BU...
 *
 * @ORM\Table(name="email_generator_email",indexes={@ORM\Index(name="search_idx", columns={"id"})})
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\EmailRepository")
 * @UniqueEntity("utm",errorPath="utm",
 *     message="This utm has been use in other email, this must be unique"
 * )
 * @UniqueEntity("jobTitle",errorPath="jobTitle",
 *     message="This job title has been use in other email, this must be unique"
 * )
 */
class Email
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * One Email has One Business unit.
     * @ORM\ManyToOne(targetEntity="AuthenticationBundle\Entity\BusinessUnit")
     * @ORM\JoinColumn(name="business_unit", referencedColumnName="id")
     */
    private $businessUnit;

    /**
     * @var string
     * @ORM\Column(name="jobTitle", type="string", length=100, unique=true)
     */
    private $jobTitle;

    /**
     * One Email has One user.
     * @ORM\ManyToOne(targetEntity="AuthenticationBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateRequest", type="datetime")
     */
    private $dateRequest;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_delivered", type="datetime", nullable=true)
     */
    private $dateDelivered;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_go_live", type="datetime", nullable=true)
     */
    private $dateGoLive;

    /**
     * @var string
     *
     * @ORM\Column(name="objective", type="string", length=255, nullable=true)
     */
    private $objective;

    /**
     * @var string
     *
     * @ORM\Column(name="utm_URL", type="string", length=255, nullable=true, unique=true)
     */
    private $utm;

    /**
     * @var string
     *
     * @ORM\Column(name="terms_and_conditions", type="text", nullable=true)
     */
    private $termsAndConditions;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * One Email has One Business unit.
     * @ORM\ManyToOne(targetEntity="EmailGeneratorBundle\Entity\Template")
     * @ORM\JoinColumn(name="template", referencedColumnName="id")
     */
    private $template;

    /**
     * One Email has One Business unit.
     * @ORM\ManyToMany(targetEntity="EmailGeneratorBundle\Entity\Block", inversedBy="email", cascade={"persist", "remove" })
     * @ORM\JoinTable(name="email_generator_email_blocks")
     * @ORM\OrderBy({"order" = "ASC", "id" = "ASC"})
     */
    private $blocks;

    /**
     * @var string
     *
     * @ORM\Column(name="pre_header", type="string", length=255, nullable=true)
     */
    private $preHeader;

    /**
     * @var boolean archive
     *
     * @ORM\Column(name="archive", type="boolean", nullable=false,  options={"default" : 0})
     */
    private $archive = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="background_colour", type="string", length=7, unique=false, nullable=true)
     */
    private $backgroundColour;

    /**
     * Email constructor.
     */
    public function __construct()
    {
        $this->dateRequest= new \DateTime();
        $this->blocks = new ArrayCollection();
        $this->archive = 0;
    }

    /**
     * clear id
     *
     * @return Email
     */
    public function clearId()
    {
        $this->id = null;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set businessUnit
     *
     * @param BusinessUnit $businessUnit
     *
     * @return Email
     */
    public function setBusinessUnit(BusinessUnit $businessUnit = null   )
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }

    /**
     * Get businessUnit
     *
     * @return BusinessUnit
     */
    public function getBusinessUnit()
    {
        return $this->businessUnit;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     *
     * @return Email
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Email
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dateRequest
     *
     * @param \DateTime $dateRequest
     *
     * @return Email
     */
    public function setDateRequest($dateRequest)
    {
        $this->dateRequest = $dateRequest;

        return $this;
    }

    /**
     * Get dateRequest
     *
     * @return \DateTime
     */
    public function getDateRequest()
    {
        return $this->dateRequest;
    }

    /**
     * Set dateDelivered
     *
     * @param \DateTime $dateDelivered
     *
     * @return Email
     */
    public function setDateDelivered($dateDelivered)
    {
        $this->dateDelivered = $dateDelivered;

        return $this;
    }

    /**
     * Get dateDelivered
     *
     * @return \DateTime
     */
    public function getDateDelivered()
    {
        return $this->dateDelivered;
    }

    /**
     * Set dateGoLive
     *
     * @param \DateTime $dateGoLive
     *
     * @return Email
     */
    public function setDateGoLive($dateGoLive)
    {
        $this->dateGoLive = $dateGoLive;

        return $this;
    }

    /**
     * Get dateGoLive
     *
     * @return \DateTime
     */
    public function getDateGoLive()
    {
        return $this->dateGoLive;
    }

    /**
     * Set objective
     *
     * @param string $objective
     *
     * @return Email
     */
    public function setObjective($objective)
    {
        $this->objective = $objective;

        return $this;
    }

    /**
     * Get objective
     *
     * @return string
     */
    public function getObjective()
    {
        return $this->objective;
    }

    /**
     * Set utm
     *
     * @param string $utm
     *
     * @return Email
     */
    public function setUtm($utm)
    {
        $this->utm = $utm;

        return $this;
    }

    /**
     * Get utm
     *
     * @return string
     */
    public function getUtm()
    {
        return $this->utm;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Email
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Email
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return Template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param Template $template
     */
    public function setTemplate(Template $template)
    {
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function getTermsAndConditions()
    {
        return $this->termsAndConditions;
    }

    /**
     * @param string $termsAndConditions
     */
    public function setTermsAndConditions($termsAndConditions)
    {
        $this->termsAndConditions = $termsAndConditions;

        return $this;
    }

    /**
     * @return bool
     */
    public function isArchive()
    {
        return $this->archive;
    }

    /**
     * @param bool $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * @return string
     */
    public function getBackgroundColour()
    {
        return $this->backgroundColour;
    }

    /**
     * @param string $backgroundColour
     */
    public function setBackgroundColour($backgroundColour)
    {
        $this->backgroundColour = $backgroundColour;
    }


    /**
     * @return mixed
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @param Block $block
     * @return $this
     */
    public function addBlock(Block $block)
    {
        $this->blocks[] = $block;
        return $this;
    }

    /**
     * @param mixed $blocks
     */
    public function setBlocks($blocks)
    {
        $this->blocks = $blocks;
    }

    /**
     * @param Block $block
     * @return bool
     */
    public function deleteBlock(Block $block)
    {
        return $this->blocks->removeElement($block);
    }

    /**
     * @return boolean
     */
    public function hasBlocks()
    {
        if ($this->getBlocks()) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getPreHeader()
    {
        return $this->preHeader;
    }

    /**
     * @param string $preHeader
     */
    public function setPreHeader($preHeader)
    {
        $this->preHeader = $preHeader;
    }

    /**
     * It returns the BU which allows access to more elements
     * @return string
     */
    public function getAdminBU()
    {
        return BusinessUnit::ASW;
    }

    public function initialiseBlocks(Array $blocks)
    {
        return [];
    }

    public function ammendBlocks(Array $updatedBlocks)
    {
        $blocks = $this->getBlocks();
        foreach ($updatedBlocks as $updatedBlock) {
            // If is set means that the block exist already so is going to be updated,
            // otherwise it is a new block added to the email
            if (isset($updatedBlock['id'])) {
                foreach ($blocks as $block) {
                    if ($block instanceof Block) {
                    }
                }
            } else {
                $newBlock = BlockHelper::newInstance($updatedBlock['name']);
                $newBlock
                    ->setOrder($updatedBlock['order'])
                    ->setCode($updatedBlock['name']);
                $this->addBlock($newBlock);
            }
        }


        return [];
    }
}


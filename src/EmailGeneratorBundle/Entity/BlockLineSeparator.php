<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockLineSeparator
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_block_separate_line")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockLineSeparator extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_text_colour", type="string", length=7, unique=false)
     */
    private $separatorColour;

    /**
     * @return string
     */
    public function getSeparatorColour()
    {
        return $this->separatorColour;
    }

    /**
     * @param string $separatorColour
     */
    public function setSeparatorColour($separatorColour)
    {
        $this->separatorColour = $separatorColour;
    }

    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->getSeparatorColour()) || is_null($this->getSeparatorColour())) {
            return true;
        }
        return false;
    }

}
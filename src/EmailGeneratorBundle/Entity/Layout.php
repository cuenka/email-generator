<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Layout
 *
 * @ORM\Table(name="email_generator_layout", indexes={@ORM\Index(name="search_idx", columns={"id"})})
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\LayoutRepository")
 */
class Layout
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Layouts have 1 Business Unit
     * @ORM\ManyToOne(targetEntity="AuthenticationBundle\Entity\BusinessUnit")
     * @ORM\JoinColumn(name="businessUnit", referencedColumnName="id")
     */
    private $businessUnit;

    /**
     * Many Layouts have 1 Template
     * @ORM\ManyToOne(targetEntity="EmailGeneratorBundle\Entity\Template")
     * @ORM\JoinColumn(name="template", referencedColumnName="id")
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="header", type="text")
     */
    private $header;

    /**
     * @var string
     *
     * @ORM\Column(name="footer", type="text")
     */
    private $footer;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBusinessUnit()
    {
        return $this->businessUnit;
    }

    /**
     * @param $businessUnit
     * @return $this
     */
    public function setBusinessUnit($businessUnit)
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }


    /**
     * Set template.
     *
     * @param string $template
     *
     * @return Layout
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template.
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set header.
     *
     * @param string $header
     *
     * @return Layout
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header.
     *
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set footer.
     *
     * @param string $footer
     *
     * @return Layout
     */
    public function setFooter($footer)
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * Get footer.
     *
     * @return string
     */
    public function getFooter()
    {
        return $this->footer;
    }
}

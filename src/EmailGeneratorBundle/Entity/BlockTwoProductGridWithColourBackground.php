<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockTwoProductGridWithColourBackground
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_block_two_product_grid_with_colour_background")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockTwoProductGridWithColourBackground extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="image_1", type="string", length=255, unique=false)
     */
    private $image1;

    /**
     * @var string
     *
     * @ORM\Column(name="image_2", type="string", length=255, unique=false)
     */
    private $image2;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_1", type="string", length=255, unique=false)
     */
    private $imageAlt1;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_2", type="string", length=255, unique=false)
     */
    private $imageAlt2;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file2;


    /**
     * @var string
     * @ORM\Column(name="product_1_brand", type="string", length=255, unique=false)
     */
    private $product1Title;

    /**
     * @var string
     * @ORM\Column(name="product_1_name", type="string", length=255, unique=false)
     */
    private $product1Description;

    /**
     * @var string
     * @ORM\Column(name="product_1_image", type="text", length=255, unique=false)
     */
    private $product1Label;

    /**
     * @var string
     * @ORM\Column(name="product_2_brand", type="string", length=255, unique=false)
     */
    private $product2Title;

    /**
     * @var string
     * @ORM\Column(name="product_2_name", type="string", length=255, unique=false)
     */
    private $product2Description;

    /**
     * @var string
     * @ORM\Column(name="product_2_image", type="text", length=255, unique=false)
     */
    private $product2Label;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Url()
     * @ORM\Column(name="cta_one_url", type="string", length=255, unique=false)
     */
    private $ctaUrl;


    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Url()
     * @ORM\Column(name="cta_one_url_2", type="string", length=255, unique=false)
     */
    private $ctaUrl2;

    /**
     * @var string
     * @ORM\Column(name="background_colour_block", length=7, type="text", unique=false, nullable=true)
     */
    private $backgroundColourBlock;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image1;
    }

    /**
     * @param string $image1
     */
    public function setImage($image1)
    {
        $this->image1 = $image1;
    }

    /**
     * @return string
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * @param string $image2
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    }

    /**
     * @return string
     */
    public function getImageAlt1()
    {
        return $this->imageAlt1;
    }

    /**
     * @param string $imageAlt1
     */
    public function setImageAlt1($imageAlt1)
    {
        $this->imageAlt1 = $imageAlt1;
    }

    /**
     * @return string
     */
    public function getImageAlt2()
    {
        return $this->imageAlt2;
    }

    /**
     * @param string $imageAlt2
     */
    public function setImageAlt2($imageAlt2)
    {
        $this->imageAlt2 = $imageAlt2;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return File
     */
    public function getFile2()
    {
        return $this->file2;
    }

    /**
     * @param File $file2
     */
    public function setFile2($file2)
    {
        $this->file2 = $file2;
    }

    /**
     * @return string
     */
    public function getProduct1Title()
    {
        return $this->product1Title;
    }

    /**
     * @param string $product1Title
     */
    public function setProduct1Title($product1Title)
    {
        $this->product1Title = $product1Title;
    }

    /**
     * @return string
     */
    public function getProduct1Description()
    {
        return $this->product1Description;
    }

    /**
     * @param string $product1Description
     */
    public function setProduct1Description($product1Description)
    {
        $this->product1Description = $product1Description;
    }

    /**
     * @return string
     */
    public function getProduct1Label()
    {
        return $this->product1Label;
    }

    /**
     * @param string $product1Label
     */
    public function setProduct1Label($product1Label)
    {
        $this->product1Label = $product1Label;
    }

    /**
     * @return string
     */
    public function getProduct2Title()
    {
        return $this->product2Title;
    }

    /**
     * @param string $product2Title
     */
    public function setProduct2Title($product2Title)
    {
        $this->product2Title = $product2Title;
    }

    /**
     * @return string
     */
    public function getProduct2Description()
    {
        return $this->product2Description;
    }

    /**
     * @param string $product2Description
     */
    public function setProduct2Description($product2Description)
    {
        $this->product2Description = $product2Description;
    }

    /**
     * @return string
     */
    public function getProduct2Label()
    {
        return $this->product2Label;
    }

    /**
     * @param string $product2Label
     */
    public function setProduct2Label($product2Label)
    {
        $this->product2Label = $product2Label;
    }

    /**
     * @return string
     */
    public function getCtaUrl()
    {
        return $this->ctaUrl;
    }

    /**
     * @param string $ctaUrl
     */
    public function setCtaUrl($ctaUrl)
    {
        $this->ctaUrl = $ctaUrl;
    }

    /**
     * @return string
     */
    public function getCtaUrl2()
    {
        return $this->ctaUrl2;
    }

    /**
     * @param string $ctaUrl2
     */
    public function setCtaUrl2($ctaUrl2)
    {
        $this->ctaUrl2 = $ctaUrl2;
    }

    /**
     * @return string
     */
    public function getBackgroundColourBlock()
    {
        return $this->backgroundColourBlock;
    }

    /**
     * @param string $backgroundColourBlock
     */
    public function setBackgroundColourBlock($backgroundColourBlock)
    {
        $this->backgroundColourBlock = $backgroundColourBlock;
    }


}
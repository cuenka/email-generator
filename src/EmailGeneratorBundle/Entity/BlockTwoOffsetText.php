<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockTwoOffsetText
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_block_two_offset_text")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockTwoOffsetText extends Block
{
    /**
     * @var string
     * @ORM\Column(name="text_one", type="text", unique=false, nullable=true)
     */
    private $text;

    /**
     * @var string
     * @ORM\Column(name="text_two", type="text", unique=false, nullable=true)
     */
    private $text2;

    /**
     * @var string
     *
     * @ORM\Column(name="image_1", type="string", length=255, unique=false)
     */
    private $image1;

    /**
     * @var string
     *
     * @ORM\Column(name="image_2", type="string", length=255, unique=false)
     */
    private $image2;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_1", type="string", length=255, unique=false)
     */
    private $imageAlt1;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_2", type="string", length=255, unique=false)
     */
    private $imageAlt2;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file2;


    /**
     * @var string
     * @ORM\Column(name="cta_one_text", type="string", length=200, unique=false, nullable=true)
     */
    private $cta1Label;

    /**
     * @var string
     * @Assert\Url()
     * @ORM\Column(name="cta_one_url", type="string", length=255, unique=false, nullable=true)
     */
    private $cta1Url;

    /**
     * @var string
     * @ORM\Column(name="cta_two_text", type="string", length=200, unique=false, nullable=true)
     */
    private $cta2Label;

    /**
     * @var string
     * @Assert\Url()
     * @ORM\Column(name="cta_two_url", type="string", length=255, unique=false, nullable=true)
     */
    private $cta2Url;

    /**
     * @var string
     * @ORM\Column(name="choice",type="boolean", options={"default":"0"})

     */
    private $needCTa1;

    /**
     * @var string
     * @ORM\Column(name="choice2",type="boolean", options={"default":"0"})

     */
    private $needCTa2;

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText2()
    {
        return $this->text2;
    }

    /**
     * @param string $text2
     */
    public function setText2($text2)
    {
        $this->text2 = $text2;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image1;
    }

    /**
     * @param string $image1
     */
    public function setImage($image1)
    {
        $this->image1 = $image1;
    }

    /**
     * @return string
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * @param string $image2
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    }

    /**
     * @return string
     */
    public function getImageAlt1()
    {
        return $this->imageAlt1;
    }

    /**
     * @param string $imageAlt1
     */
    public function setImageAlt1($imageAlt1)
    {
        $this->imageAlt1 = $imageAlt1;
    }

    /**
     * @return string
     */
    public function getImageAlt2()
    {
        return $this->imageAlt2;
    }

    /**
     * @param string $imageAlt2
     */
    public function setImageAlt2($imageAlt2)
    {
        $this->imageAlt2 = $imageAlt2;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return File
     */
    public function getFile2()
    {
        return $this->file2;
    }

    /**
     * @param File $file2
     */
    public function setFile2($file2)
    {
        $this->file2 = $file2;
    }

    /**
     * @return string
     */
    public function getCta1Label()
    {
        return $this->cta1Label;
    }

    /**
     * @param string $cta1Label
     */
    public function setCta1Label($cta1Label)
    {
        $this->cta1Label = $cta1Label;
    }

    /**
     * @return string
     */
    public function getCta1Url()
    {
        return $this->cta1Url;
    }

    /**
     * @param string $cta1Url
     */
    public function setCta1Url($cta1Url)
    {
        $this->cta1Url = $cta1Url;
    }

    /**
     * @return string
     */
    public function getCta2Label()
    {
        return $this->cta2Label;
    }

    /**
     * @param string $cta2Label
     */
    public function setCta2Label($cta2Label)
    {
        $this->cta2Label = $cta2Label;
    }

    /**
     * @return string
     */
    public function getCta2Url()
    {
        return $this->cta2Url;
    }

    /**
     * @param string $cta2Url
     */
    public function setCta2Url($cta2Url)
    {
        $this->cta2Url = $cta2Url;
    }

    /**
     * @return string
     */
    public function getNeedCTa1()
    {
        return $this->needCTa1;
    }

    /**
     * @param string $needCTa1
     */
    public function setNeedCTa1($needCTa1)
    {
        $this->needCTa1 = $needCTa1;
    }

    /**
     * @return string
     */
    public function getNeedCTa2()
    {
        return $this->needCTa2;
    }

    /**
     * @param string $needCTa2
     */
    public function setNeedCTa2($needCTa2)
    {
        $this->needCTa2 = $needCTa2;
    }



    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->getText()) || is_null($this->getText())) {
            return true;
        }
        return false;
    }
}
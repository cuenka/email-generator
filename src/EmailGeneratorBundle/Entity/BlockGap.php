<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockGap
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_block_gap")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockGap extends Block
{
    /**
     * @var integer
     *
     * @ORM\Column(name="gap", type="integer", unique=false)
     */
    private $gap;

    /**
     * @return int
     */
    public function getGap()
    {
        return $this->gap;
    }

    /**
     * @param int $gap
     */
    public function setGap($gap)
    {
        $this->gap = $gap;
    }

    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->getGap()) || is_null($this->getGap())) {
            return true;
        }
        return false;
    }

}
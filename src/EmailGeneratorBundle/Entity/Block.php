<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EmailGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Block, All entities which start with Block are blocks! This is the parent! please check the DiscriminatorMap
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap(
 *     {
 *     "Block" = "Block",
 *     "BlockGap" = "BlockGap",
 *     "BlockLineSeparator" = "BlockLineSeparator",
 *     "BlockText" = "BlockText",
 *     "BlockHero" = "BlockHero",
 *     "BlockTitleFullLine" = "BlockTitleFullLine",
 *     "BlockSimple2BlockTextCta" = "BlockSimple2BlockTextCta",
 *     "BlockSimple2BlockTextCtaWithTitle" = "BlockSimple2BlockTextCtaWithTitle",
 *     "BlockTwoBlockFullProducts" = "BlockTwoBlockFullProducts",
 *     "BlockThreeBlockFullProducts" = "BlockThreeBlockFullProducts",
 *     "BlockFourBlockFullProducts" = "BlockFourBlockFullProducts",
 *     "BlockFullProductImage" = "BlockFullProductImage",
 *     "BlockMlife" = "BlockMlife",
 *     "BlockTwoBlockOnlyCta" = "BlockTwoBlockOnlyCta",
 *     "BlockThreeBlockOnlyCta" = "BlockThreeBlockOnlyCta",
 *     "BlockHeaderOptionOne" = "BlockHeaderOptionOne",
 *     "BlockIntroTextAndCta" = "BlockIntroTextAndCta",
 *     "BlockProductImageWithctaAndImage" = "BlockProductImageWithctaAndImage",
 *     "BlockMlifeImageLeft" = "BlockMlifeImageLeft",
 *     "BlockGwpSection" = "BlockGwpSection",
 *     "BlockThreeProductGridWithTitle" = "BlockThreeProductGridWithTitle",
 *     "BlockTwoProductGridWithColourBackground" = "BlockTwoProductGridWithColourBackground",
 *     "BlockTwoOffsetText" = "BlockTwoOffsetText",
 *     "BlockThreeOffsetText" = "BlockThreeOffsetText",
 * })
 *
 * @ORM\Table(name="email_generator_block", indexes={@ORM\Index(name="search_idx", columns={"id"})})
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class Block
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int order
     *
     * @ORM\Column(name="order_block", type="integer")
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="code_block", type="string", length=100, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=true)
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=20, unique=true, nullable=true)
     */
    private $thumbnail;

    /**
     * One Email has many blocks
     * @ORM\ManyToMany(targetEntity="EmailGeneratorBundle\Entity\Email", mappedBy="blocks")
     */
    private $email;

    /**
     * Block constructor.
     * @param int $order
     */
    public function __construct($code = null,$order = 1)
    {
        $this->code = $code;
        $this->order = $order;
    }

    /**
     * clear id
     *
     * @return Block
     */
    public function clearId()
    {
        $this->id = null;

        return $this;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param string $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function generateFileName($filename = null , $extension = null)
    {
        $filename = basename($filename, $extension);

        // Replaces all spaces with underscores.
        $filename = str_replace(' ', '_', $filename);
        // Removes special chars.
        $filename = preg_replace('/[^A-Za-z0-9\-_]/', '', $filename);
        // Date will help to avoid cache, and, in case 2 same images or same name are uploaded, they will not collide
        return date("dmYHis-",time()). $filename. $extension;
    }

    /**
     * It is used on Block section
     * @param $context
     * @return mixed
     */
    public function getSkuInfo($context) {
        switch($context) {
            case 'product1':
                $skuInfo['productBrand'] = $this->getProduct1Brand();
                $skuInfo['productName'] = $this->getProduct1Name();
                $skuInfo['productImage'] = $this->getProduct1Image();
                $skuInfo['productPrice'] = $this->getProduct1Price();
                $skuInfo['productPromoPrice'] = $this->getProduct1PromoPrice();
                break;
            case 'product2':
                $skuInfo['productBrand'] = $this->getProduct2Brand();
                $skuInfo['productName'] = $this->getProduct2Name();
                $skuInfo['productImage'] = $this->getProduct2Image();
                $skuInfo['productPrice'] = $this->getProduct2Price();
                $skuInfo['productPromoPrice'] = $this->getProduct2PromoPrice();
                break;
            case 'product3':
                $skuInfo['productBrand'] = $this->getProduct3Brand();
                $skuInfo['productName'] = $this->getProduct3Name();
                $skuInfo['productImage'] = $this->getProduct3Image();
                $skuInfo['productPrice'] = $this->getProduct3Price();
                $skuInfo['productPromoPrice'] = $this->getProduct3PromoPrice();
                break;
            case 'product4':
                $skuInfo['productBrand'] = $this->getProduct4Brand();
                $skuInfo['productName'] = $this->getProduct4Name();
                $skuInfo['productImage'] = $this->getProduct4Image();
                $skuInfo['productPrice'] = $this->getProduct4Price();
                $skuInfo['productPromoPrice'] = $this->getProduct4PromoPrice();
                break;
        }

        return $skuInfo;
    }

    /**
     * Some url contains GET parameters and UTM is GET PARAMETERS, so this function
     * join all the GET parameters in a way that the URL is valid
     * @param $url
     * @param $utm
     * @return mixed
     */
    public function validateURLWithUTM($url, $utm)
    {
        $parsedURL= parse_url($url);
        $host = isset($parsedURL['host']) ? $parsedURL['host'] : null;
        $path = isset($parsedURL['path']) ? $parsedURL['path'] : null;
        $query = isset($parsedURL['query']) ? "&". $parsedURL['query'] : null;
        $scheme = isset($parsedURL['scheme']) ? $parsedURL['scheme'].'://' : null;
        $validURL = $scheme. $host. $path. $utm.  $query;

        return $validURL;
    }

    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        return true;
    }

}

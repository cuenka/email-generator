<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockIntroTextAndCta
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_intro_text_and_cta")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockIntroTextAndCta extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_text_colour", type="string", length=7, unique=false)
     */
    private $textColour;

    /**
     * @var string
     * @ORM\Column(name="text_one", type="text", unique=false, nullable=true)
     */
    private $text;

    /**
     * @var string
     * @ORM\Column(name="cta_one_text", type="string", length=200, unique=false, nullable=true)
     */
    private $ctaText;

    /**
     * @var string
     * @Assert\Url()
     * @ORM\Column(name="cta_one_url", type="string", length=255, unique=false, nullable=true)
     */
    private $ctaUrl;

    /**
     * @var string
     * @ORM\Column(name="cta_one_url_title", type="string", length=255, unique=false, nullable=true)
     */
    private $ctaUrlTitle;

    /**
     * @var string
     * @ORM\Column(name="choice",type="boolean", options={"default":"0"})

     */
    private $needCTa;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTextColour()
    {
        return $this->textColour;
    }

    /**
     * @param string $textColour
     */
    public function setTextColour($textColour)
    {
        $this->textColour = $textColour;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getCtaText()
    {
        return $this->ctaText;
    }

    /**
     * @param string $ctaText
     */
    public function setCtaText($ctaText)
    {
        $this->ctaText = $ctaText;
    }

    /**
     * @return string
     */
    public function getCtaUrl()
    {
        return $this->ctaUrl;
    }

    /**
     * @param string $ctaUrl
     */
    public function setCtaUrl($ctaUrl)
    {
        $this->ctaUrl = $ctaUrl;
    }

    /**
     * @return string
     */
    public function getCtaUrlTitle()
    {
        return $this->ctaUrlTitle;
    }

    /**
     * @param string $ctaUrlTitle
     */
    public function setCtaUrlTitle($ctaUrlTitle)
    {
        $this->ctaUrlTitle = $ctaUrlTitle;
    }

    /**
     * @return string
     */
    public function getNeedCTa()
    {
        return $this->needCTa;
    }

    /**
     * @param string $needCTa
     */
    public function setNeedCTa($needCTa)
    {
        $this->needCTa = $needCTa;
    }

}
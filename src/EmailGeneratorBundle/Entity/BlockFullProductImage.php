<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockFullProductImageLeft
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_block_title_full_line")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockFullProductImage extends Block
{
    /**
     * @var string
     * @Assert\Regex(pattern="/^[0-9]/", message="Product 1 ID only must contains numbers, information not saved")
     * @ORM\Column(name="product_1", type="string", length=20, unique=false)
     */
    private $product1;

    /**
     * @var string
     * @ORM\Column(name="product_1_brand", type="string", length=255, unique=false)
     */
    private $product1Brand;

    /**
     * @var string
     * @ORM\Column(name="product_1_name", type="string", length=255, unique=false)
     */
    private $product1Name;

    /**
     * @var string
     * @ORM\Column(name="product_1_image", type="text", length=255, unique=false)
     */
    private $product1Image;

    /**
     * @var string Price is string because different Bu use comma or dot and currency
     * @ORM\Column(name="product_1_price", type="string", length=20, unique=false)
     */
    private $product1Price;

    /**
     * @var string Promo price is string because different Bu use comma or dot and currency
     * @ORM\Column(name="product_1_promo_price", type="string", length=20, unique=false)
     */
    private $product1PromoPrice;

    /**
     * @var string Extra information, normally "price per ML"
     * @ORM\Column(name="product_extra_information", type="string", length=60, unique=false)
     */
    private $productExtraInformation;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="text_one", type="text", unique=false)
     */
    private $text;


    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="cta_one_text", type="string", length=200, unique=false)
     */
    private $ctaText;


    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_background", type="string", length=7, unique=false)
     */
    private $ctaBackgroundColour;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_text_colour", type="string", length=7, unique=false)
     */
    private $ctaTextColour;

    /**
     * @var string
     * @ORM\Column(name="cta_one_url_title", type="string", length=255, unique=false)
     */
    private $ctaUrlTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="price_colour", type="string", length=7, unique=false)
     */
    private $priceColour;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="font_family", type="string", length=60, unique=false)
     */
    private $fontFamily;

    /**
     * @return string
     */
    public function getProduct1()
    {
        return $this->product1;
    }

    /**
     * @param string $product1
     */
    public function setProduct1($product1)
    {
        $this->product1 = $product1;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getCtaText()
    {
        return $this->ctaText;
    }

    /**
     * @param string $ctaText
     */
    public function setCtaText($ctaText)
    {
        $this->ctaText = $ctaText;
    }

    /**
     * @return string
     */
    public function getCtaBackgroundColour()
    {
        return $this->ctaBackgroundColour;
    }

    /**
     * @param string $ctaBackgroundColour
     */
    public function setCtaBackgroundColour($ctaBackgroundColour)
    {
        $this->ctaBackgroundColour = $ctaBackgroundColour;
    }

    /**
     * @return string
     */
    public function getCtaTextColour()
    {
        return $this->ctaTextColour;
    }

    /**
     * @param string $ctaTextColour
     */
    public function setCtaTextColour($ctaTextColour)
    {
        $this->ctaTextColour = $ctaTextColour;
    }

    /**
     * @return string
     */
    public function getCtaUrl()
    {
        return $this->ctaUrl;
    }

    /**
     * @param string $ctaUrl
     */
    public function setCtaUrl($ctaUrl)
    {
        $this->ctaUrl = $ctaUrl;
    }

    /**
     * @return string
     */
    public function getFontFamily()
    {
        return $this->fontFamily;
    }

    /**
     * @param string $fontFamily
     */
    public function setFontFamily($fontFamily)
    {
        $this->fontFamily = $fontFamily;
    }

    /**
     * @return string
     */
    public function getCtaUrlTitle()
    {
        return $this->ctaUrlTitle;
    }

    /**
     * @param string $ctaUrlTitle
     */
    public function setCtaUrlTitle($ctaUrlTitle)
    {
        $this->ctaUrlTitle = $ctaUrlTitle;
    }

    /**
     * @return string
     */
    public function getProduct1Brand()
    {
        return $this->product1Brand;
    }

    /**
     * @param string $product1Brand
     */
    public function setProduct1Brand($product1Brand)
    {
        $this->product1Brand = $product1Brand;
    }

    /**
     * @return string
     */
    public function getProduct1Name()
    {
        return $this->product1Name;
    }

    /**
     * @param string $product1Name
     */
    public function setProduct1Name($product1Name)
    {
        $this->product1Name = $product1Name;
    }

    /**
     * @return string
     */
    public function getProduct1Image()
    {
        return $this->product1Image;
    }

    /**
     * @param string $product1Image
     */
    public function setProduct1Image($product1Image)
    {
        $this->product1Image = $product1Image;
    }

    /**
     * @return string
     */
    public function getProduct1Price()
    {
        return $this->product1Price;
    }

    /**
     * @param string $product1Price
     */
    public function setProduct1Price($product1Price)
    {
        $this->product1Price = $product1Price;
    }

    /**
     * @return string
     */
    public function getProduct1PromoPrice()
    {
        return $this->product1PromoPrice;
    }

    /**
     * @param string $product1PromoPrice
     */
    public function setProduct1PromoPrice($product1PromoPrice)
    {
        $this->product1PromoPrice = $product1PromoPrice;
    }

    /**
     * @return string
     */
    public function getPriceColour()
    {
        return $this->priceColour;
    }

    /**
     * @param string $priceColour
     */
    public function setPriceColour($priceColour)
    {
        $this->priceColour = $priceColour;
    }

    /**
     * @return string
     */
    public function getProductExtraInformation()
    {
        return $this->productExtraInformation;
    }

    /**
     * @param string $productExtraInformation
     */
    public function setProductExtraInformation($productExtraInformation)
    {
        $this->productExtraInformation = $productExtraInformation;
    }



    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->getProduct1()) || is_null($this->getProduct1())) {
            return true;
        }
        return false;
    }
}
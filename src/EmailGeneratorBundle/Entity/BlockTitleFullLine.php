<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class BlockTitleFullLine, It contains a centered title and an underline
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_block_title_full_line")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockTitleFullLine extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_text_colour", type="string", length=7, unique=false)
     */
    private $ctaTextColour;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="font_size", type="integer", unique=false)
     */
    private $fontSize;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCtaTextColour()
    {
        return $this->ctaTextColour;
    }

    /**
     * @param string $ctaTextColour
     */
    public function setCtaTextColour($ctaTextColour)
    {
        $this->ctaTextColour = $ctaTextColour;
    }

    /**
     * @return string
     */
    public function getFontSize()
    {
        return $this->fontSize;
    }

    /**
     * @param string $fontSize
     */
    public function setFontSize($fontSize)
    {
        $this->fontSize = $fontSize;
    }

    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->getTitle()) || is_null($this->getTitle())) {
            return true;
        }
        return false;
    }
    
}
<?php

namespace EmailGeneratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BlockThreeBlockOnlyCta
 * @package EmailGeneratorBundle\Entity
 * @ORM\Table(name="email_generator_three_block_only_cta")
 * @ORM\Entity(repositoryClass="EmailGeneratorBundle\Repository\BlockRepository")
 */
class BlockThreeBlockOnlyCta extends Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="image_1", type="string", length=255, unique=false)
     */
    private $image1;

    /**
     * @var string
     *
     * @ORM\Column(name="image_2", type="string", length=255, unique=false)
     */
    private $image2;

    /**
     * @var string
     *
     * @ORM\Column(name="image_3", type="string", length=255, unique=false)
     */
    private $image3;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_1", type="string", length=255, unique=false)
     */
    private $imageAlt1;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_2", type="string", length=255, unique=false)
     */
    private $imageAlt2;

    /**
     * @var string
     *
     * @ORM\Column(name="image_alt_3", type="string", length=255, unique=false)
     */
    private $imageAlt3;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file2;

    /**
     * @var File
     * @Assert\File(mimeTypes={"image/jpeg", "image/png", "image/jpg", "image/gif"})
     */
    private $file3;


    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="cta_one_text", type="string", length=200, unique=false)
     */
    private $ctaText;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Url()
     * @ORM\Column(name="cta_one_url", type="string", length=255, unique=false)
     */
    private $ctaUrl;


    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Url()
     * @ORM\Column(name="cta_one_url_2", type="string", length=255, unique=false)
     */
    private $ctaUrl2;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Url()
     * @ORM\Column(name="cta_one_url_3", type="string", length=255, unique=false)
     */
    private $ctaUrl3;

    /**
     * @var string
     * @ORM\Column(name="cta_one_url_title", type="string", length=255, unique=false)
     */
    private $ctaUrlTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_background", type="string", length=7, unique=false)
     */
    private $ctaBackgroundColour;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_one_text_colour", type="string", length=7, unique=false)
     */
    private $ctaTextColour;


    /**
     * @return string
     */
    public function getAlignBlock()
    {
        return $this->alignBlock;
    }

    /**
     * @param string $alignBlock
     */
    public function setAlignBlock($alignBlock)
    {
        $this->alignBlock = $alignBlock;
    }



    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }



    /**
     * @return string
     */
    public function getBackgroundColourBlock()
    {
        return $this->backgroundColourBlock;
    }

    /**
     * @param string $backgroundColourBlock
     */
    public function setBackgroundColourBlock($backgroundColourBlock)
    {
        $this->backgroundColourBlock = $backgroundColourBlock;
    }


    /**
     * @return string
     */
    public function getCtaText()
    {
        return $this->ctaText;
    }

    /**
     * @param string $ctaText
     */
    public function setCtaText($ctaText)
    {
        $this->ctaText = $ctaText;
    }

    /**
     * @return string
     */
    public function getCtaBackgroundColour()
    {
        return $this->ctaBackgroundColour;
    }

    /**
     * @param string $ctaBackgroundColour
     */
    public function setCtaBackgroundColour($ctaBackgroundColour)
    {
        $this->ctaBackgroundColour = $ctaBackgroundColour;
    }

    /**
     * @return string
     */
    public function getCtaTextColour()
    {
        return $this->ctaTextColour;
    }

    /**
     * @param string $ctaTextColour
     */
    public function setCtaTextColour($ctaTextColour)
    {
        $this->ctaTextColour = $ctaTextColour;
    }

    /**
     * @return string
     */
    public function getCtaUrl()
    {
        return $this->ctaUrl;
    }

    /**
     * @param string $ctaUrl
     */
    public function setCtaUrl($ctaUrl)
    {
        $this->ctaUrl = $ctaUrl;
    }


    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image1;
    }

    /**
     * @param string $image1
     */
    public function setImage($image1)
    {
        $this->image1 = $image1;
    }

    /**
     * @return string
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * @param string $image2
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    }


    /**
     * @return string
     */
    public function getImageAlt1()
    {
        return $this->imageAlt1;
    }

    /**
     * @param string $imageAlt1
     */
    public function setImageAlt1($imageAlt1)
    {
        $this->imageAlt1 = $imageAlt1;
    }

    /**
     * @return string
     */
    public function getCtaUrlTitle()
    {
        return $this->ctaUrlTitle;
    }

    /**
     * @param string $ctaUrlTitle
     */
    public function setCtaUrlTitle($ctaUrlTitle)
    {
        $this->ctaUrlTitle = $ctaUrlTitle;
    }

    /**
     * Detect is block is empty with minimum checks like if mandatory fields are null
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->getImage()) || is_null($this->getImage())) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getImageAlt2()
    {
        return $this->imageAlt2;
    }

    /**
     * @param string $imageAlt2
     */
    public function setImageAlt2($imageAlt2)
    {
        $this->imageAlt2 = $imageAlt2;
    }

    /**
     * @return File
     */
    public function getFile2()
    {
        return $this->file2;
    }

    /**
     * @param File $file2
     */
    public function setFile2($file2)
    {
        $this->file2 = $file2;
    }

    /**
     * @return string
     */
    public function getCtaUrl2()
    {
        return $this->ctaUrl2;
    }

    /**
     * @param string $ctaUrl2
     */
    public function setCtaUrl2($ctaUrl2)
    {
        $this->ctaUrl2 = $ctaUrl2;
    }

    /**
     * @return string
     */
    public function getImage3()
    {
        return $this->image3;
    }

    /**
     * @param string $image3
     */
    public function setImage3($image3)
    {
        $this->image3 = $image3;
    }

    /**
     * @return string
     */
    public function getImageAlt3()
    {
        return $this->imageAlt3;
    }

    /**
     * @param string $imageAlt3
     */
    public function setImageAlt3($imageAlt3)
    {
        $this->imageAlt3 = $imageAlt3;
    }

    /**
     * @return File
     */
    public function getFile3()
    {
        return $this->file3;
    }

    /**
     * @param File $file3
     */
    public function setFile3($file3)
    {
        $this->file3 = $file3;
    }

    /**
     * @return string
     */
    public function getCtaUrl3()
    {
        return $this->ctaUrl3;
    }

    /**
     * @param string $ctaUrl3
     */
    public function setCtaUrl3($ctaUrl3)
    {
        $this->ctaUrl3 = $ctaUrl3;
    }

}
<?php

namespace EmailGeneratorBundle\Service;

use AppBundle\Service\Uploader;
use AuthenticationBundle\Entity\BusinessUnit;
use EmailGeneratorBundle\Entity\Block;
use EmailGeneratorBundle\Entity\Email;
use Leafo\ScssPhp\Compiler;

/**
 * Following service in charge to manipulate files and upload/Override/Delete and create structure on the CDN it extends uploader
 * IMPORTANT: Path passed here NOT are not finishing with /
 *
 *
 * Class CDNManager
 * @package EmailGeneratorBundle\Service
 */
class CDNManager extends Uploader
{

    /**
     * CDNManager constructor.
     */
    public function __construct($cacheDir, $ftp, $host, $username, $password, $destination)
    {
        parent::__construct($cacheDir, $ftp, $host, $username, $password, $destination);
    }

    /**
     * This function take a sourcePath, sourceFilename, destinationPath and destinationName and upload to the CDN,
     * If path does not exist is created
     * If file exists, is overrated
     * If sourceFilename is not given, it will understand that it passed the filename on the sourcePath
     * DestinationPath and destinationName must be separated
     *
     * @param string $sourcePath
     * @param string|null $sourceFilename
     * @param string $destinationPath
     * @param string $destinationFilename
     *
     * @return boolean True if OK, False if Error
     */
    public function uploadToCDN(string $sourcePath, string $sourceFilename = null, string  $destinationPath, string $destinationFilename)
    {
        try {
            if (!is_null($sourceFilename)) {
                $sourcePath = $sourcePath . self::DS . $sourceFilename;
            }

            $this->uploadToFTP($sourcePath, $destinationPath, $destinationFilename);

            return true;
        }catch (FtpException $e) {
            return false;
        }
    }

    /**
     * It delete file in CDN if exists if file or folder on CDN
     * iF file is null, It will delete all the files in the path
     * @param string $path FTP path
     * @param string $file filename on FTP
     *
     * @return boolean True if OK, False if Error
     */
    public function deletePreviousUpload($path='',$file = null)
    {
        try {
            // No record on DB so no file exists
            if (is_null($file)) {
                return true;
            }

            if ($this->existOnDestination($path,$file)) {
                return $this->deleteOnDestination($path, $file);
            }

            return true;

        } catch (FtpException $e) {
            return false;
        }


    }


}
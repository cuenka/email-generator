<?php

namespace EmailGeneratorBundle\Service;


use AppBundle\Service\ProductCrawler;
use AuthenticationBundle\Entity\BusinessUnit;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use EmailGeneratorBundle\Entity\Block;
use EmailGeneratorBundle\Entity\Email;
use EmailGeneratorBundle\Entity\Layout;
use Leafo\ScssPhp\Compiler;
use Symfony\Component\HttpKernel\Kernel;
use Twig\Environment;

class Generator
{
    const DS = DIRECTORY_SEPARATOR;
    const COMMON = 'common';
    /**
     * @var Kernel
     */
    private $kernel;

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var Compiler
     */
    private $scss;

    /**
     * @var string CachePath
     */
    private $cachePath;

    /**
     * @var string CachePath
     */
    private $imagePath;

    /**
     * @var ProductCrawler
     */
    private $productCrawler;

    /**
     * @var string CachePath
     */
    private $isElab;

    /**
     * @var Email $email
     */
    private $email;

    /**
     * @var Layout|boolean
     */
    private $layout = false;

    /**
     * FinderGenerator constructor.
     */
    public function __construct($kernel, EntityManagerInterface $doctrine, Environment $twig, $cachePath, $imagePath, $productCrawler, $isElab)
    {
        $this->kernel = $kernel;
        $this->em = $doctrine;
        $this->twig = $twig;
        $this->cachePath = realpath($cachePath);
        $this->imagePath = realpath($imagePath);
        $this->scss = new Compiler();
        $this->productCrawler = $productCrawler;
        $this->isElab = $isElab;
        $this->layout = false;
    }

    /**
     * @return Kernel
     */
    public function getKernel()
    {
        return $this->kernel;
    }

    /**
     * @param Email $email
     * @return $this
     */
    public function setEmail(Email $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @return string
     */
    public function getIsElab()
    {
        return $this->isElab;
    }


    /**
     * We do this in case the layout change by user and we want to keep records on cache...
     * @param string $type Standard, vip...
     * @param string $bu Business unit
     * @return array
     */
    public function getTemplatePath(string $type = null, string $bu = null)
    {
        $buPath = null;
        $commonPath = null;

        try {
            $commonPath = $this->kernel->locateResource('@EmailGeneratorBundle/Resources/templates/' . $type . self::DS . self::COMMON);
            $buPath = $this->kernel->locateResource('@EmailGeneratorBundle/Resources/templates/' . $type . self::DS . $bu);

        } catch (\Exception $e) {

        }finally {
            return [
                'bu' => $buPath,
                'common' => $commonPath
            ];
        }
    }

    /**
     * @param integer $emailID Email ID
     * @param string $type Standard, vip...
     * @param string $bu Business unit
     * @return bool
     */
    public function preview($emailID = 0, $type = null, $bu = null)
    {
        try {
            $templatePath = $this->getTemplatePath($type, $bu);
            return true;
        } catch (\Exception $exception) {
            echo 'Code: ' . $exception->getCode() . 'Message: ' . $exception->getMessage();
        }
    }

    public function generateHeader($blocksPath, $parameters = null)
    {
        // If layout is false means that nothing was found on DB so legacy search on code is trigger
        if ($this->layout === false) {
            $pathToHtml = $blocksPath . self::DS . '_header.html.twig';
            $html = $this->twig->render($pathToHtml,
                [
                    'parameters' => $parameters
                ]
            );

            return $html;
        } else {
            // Means that template was found so it is taken from DB
            $twig = $this->twig->createTemplate($this->layout->getHeader());
            return $twig->render(
                [
                    'parameters' => $parameters
                ]
            );
        }
    }

    /**
     * @param string|null$blocksPath
     * @param array|null $parameters
     * @return false|string
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function generateFooter($blocksPath, $parameters = null)
    {
        // If layout is false means that nothing was found on DB so legacy search on code is trigger
        if ($this->layout === false) {
        $pathToHtml = $blocksPath . self::DS . '_footer.html.twig';
        $html = $this->twig->render($pathToHtml,
            [
                'parameters' => $parameters
            ]
        );

        return $html;
        } else {
            // Means that template was found so it is taken from DB
            $twig = $this->twig->createTemplate($this->layout->getFooter());
            return $twig->render(
                [
                    'parameters' => $parameters
                ]
            );
        }
    }

    /**
     * @param Block $block
     * @param string $blocksPath
     * @param null $parameters
     * @return mixed
     */
    public function generateHtml(Block $block, string $blocksPath, $parameters = null)
    {
        $pathToHtml = $blocksPath . self::DS . $block->getCode() . '.html.twig';
        $html = $this->twig->render($pathToHtml,
            [
                'block' => $block,
                'parameters' => $parameters
            ]
        );

        return $html;
    }

    public function getParameters(Email $email)
    {
        $params =
            [
                'domain' => BusinessUnit::getDomain($email->getBusinessUnit()->getCode()),
                'bu' => $email->getBusinessUnit()->getCode(),
                'MCHCodes' => $email->getBusinessUnit()->getMCHCodes(),
                'utm' => $email->getUtm(),
                'backgroundColour' => $email->getBackgroundColour(),
                'preHeader' => $email->getPreHeader(),
                'termsAndConditions' => $email->getTermsAndConditions(),
                'imagespath' => '/images/'
            ];

        // CDN PATH
        if ($this->getKernel()->getContainer()->getParameter('is_elab')) {
            $params['imagespath'] = BusinessUnit::getDomain($email->getBusinessUnit()->getCode()) . '/elab' .
                $this->getKernel()->getContainer()->getParameter('ftp_destination') . self::DS . 'email_generator' . self::DS . $email->getId() . self::DS;
        }

        return $params;
    }

    /**
     * @deprecated No need of check this anymore as no extraparams are needed
     * @param Block $block
     * @return boolean
     */
    public function requireExtraInfo(Block $block = null)
    {
        $isTrue = false;

        if (is_null($block) === false) {
            if (property_exists($block, 'product1')) {
                $isTrue = true;
            }
            if (property_exists($block, 'product2')) {
                $isTrue = true;
            }
            if (property_exists($block, 'product3')) {
                $isTrue = true;
            }
        }

        return $isTrue;
    }

    /**
     * @deprecated Now it crawl the website before rendering, It will be removed soon
     * @param Block $block
     * @return boolean
     */
    public function getExtraInfo(Block $block = null, string $bu)
    {
        $extraParams = [];
        if (is_null($block) === false) {
            if (property_exists($block, 'product1')) {
                $extraParams['product1'] = $this->crawlProduct($block->getProduct1(), $bu);
            }
            if (property_exists($block, 'product2')) {
                $extraParams['product2'] = $this->crawlProduct($block->getProduct2(), $bu);
            }
            if (property_exists($block, 'product3')) {
                $extraParams['product3'] = $this->crawlProduct($block->getProduct3(), $bu);
            }
        }
        return $extraParams;
    }

    private function crawlProduct($id, $bu)
    {
        $url = BusinessUnit::amendURL($id, $bu);

        if ($this->productCrawler->setUrl($url)) {
            return $this->productCrawler->marionnaudProduct();
        }
        return false;

    }

    /**
     * @return string
     */
    public function createHtml()
    {
        $blocksPath = $this->getTemplatePath($this->getEmail()->getTemplate()->getFilename(), $this->getEmail()->getBusinessUnit()->getCode());
        // Getting Layout even if does not exist
        $this->layout = $this->em->getRepository(Layout::class)
            ->getLayoutByBuAndTemplate($this->email->getBusinessUnit(), $this->email->getTemplate());

        $html = $this->generateHeader($blocksPath['bu'], $this->getParameters($this->getEmail()));
        foreach ($this->getEmail()->getBlocks() as $block) {
            $html .= $this->generateHtml($block, $blocksPath['common'], $this->getParameters($this->getEmail()));
        }
        $html .= $this->generateFooter($blocksPath['bu'], $this->getParameters($this->getEmail()));

        return $html;
    }
}
<?php

namespace EmailGeneratorBundle\Service;

use AuthenticationBundle\Entity\BusinessUnit;
use Doctrine\ORM\EntityManagerInterface;
use EmailGeneratorBundle\Entity\Block;
use EmailGeneratorBundle\Entity\Email;
use Leafo\ScssPhp\Compiler;

class DownloadGenerator extends Generator
{

    /**
     * PreviewGenerator constructor.
     */
    public function __construct($kernel, EntityManagerInterface $doctrine, $twig, $cachePath, $imagePath, $productCrawler, $isElab)
    {
        parent::__construct($kernel, $doctrine, $twig, $cachePath, $imagePath, $productCrawler, $isElab);
    }

    /**
     * @return string
     */
    public function getZipPath()
    {
        return $this->getCachePath() . self::DS . $this->getEmail()->getId() . self::DS;
    }


    public function MoveFilesToCache()
    {
        // Variables
        $cachePath = $this->getCachePath() . self::DS . $this->getEmail()->getId();
        $imagePath = $this->getImagePath() . self::DS . $this->getEmail()->getId();
        //creating and making directores
        if (!is_dir($cachePath)) {
            mkdir($cachePath);
        }
        if (!is_dir($cachePath . self::DS . 'images')) {
            mkdir($cachePath . self::DS . 'images');
        }
        // Scanning Files
        if (file_exists($imagePath)) {
            $images = array_diff(scandir($imagePath), array('..', '.', '.DS_Store'));
            // Coping files to cache
            foreach ($images as $image) {
                copy($imagePath . self::DS . $image, $cachePath . self::DS . 'images' . self::DS . $image);
            }
        }
        file_put_contents($cachePath . self::DS . 'index.html', $this->createHtml());

        return $this;
    }

    public function createZip()
    {
        // Variables
        $emailPath = $this->getCachePath() . self::DS . $this->getEmail()->getId();
        $zip = new \ZipArchive();
        $zipName = 'email-' . time() . ".zip";
        $zipPath = $emailPath . self::DS . $zipName;
        $zip->open($zipPath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $zip->addFile($emailPath . self::DS . 'index.html', 'index.html');
        $zip->addEmptyDir('images');
        $images = array_diff(scandir($emailPath . self::DS . 'images'), array('..', '.', '.DS_Store'));
        foreach ($images as $image) {
            $zip->addFile(
                $emailPath . self::DS . 'images' . self::DS . $image,
                'images' . self::DS . $image
            );
        }
        $zip->close();

        return $zipName;
    }

    public function getParameters(Email $email)
    {
        $params =
            [
                'domain' => BusinessUnit::getDomain($email->getBusinessUnit()->getCode()),
                'bu' => $email->getBusinessUnit()->getCode(),
                'MCHCodes' => $email->getBusinessUnit()->getMCHCodes(),
                'utm' => $email->getUtm(),
                'backgroundColour' => $email->getBackgroundColour(),
                'preHeader' => $email->getPreHeader(),
                'termsAndConditions' => $email->getTermsAndConditions(),
                'imagespath' => 'http://apps.elabeurope.co.uk/images/'. $email->getId().'/'
            ];

        // CDN PATH
        if ($this->getKernel()->getContainer()->getParameter('is_elab')) {
            $params['imagespath'] =  BusinessUnit::getDomain($email->getBusinessUnit()->getCode()).'/elab' .
                $this->getKernel()->getContainer()->getParameter('ftp_destination')  . self::DS . 'email_generator' . self::DS . $email->getId(). self::DS;
        }

        return $params;
    }
}
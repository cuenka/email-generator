<?php

namespace EmailGeneratorBundle\Service;


use AuthenticationBundle\Entity\BusinessUnit;
use Doctrine\ORM\EntityManagerInterface;
use EmailGeneratorBundle\Entity\Block;
use EmailGeneratorBundle\Entity\Email;
use Leafo\ScssPhp\Compiler;
use Symfony\Component\HttpKernel\Kernel;

class PreviewGenerator extends Generator
{
    /**
     * PreviewGenerator constructor.
     */
    public function __construct($kernel, EntityManagerInterface $doctrine, $twig, $cachePath, $imagePath, $productCrawler, $isElab)
    {
        parent::__construct($kernel, $doctrine, $twig, $cachePath, $imagePath,$productCrawler, $isElab);
    }

    public function getParameters(Email $email)
    {
        $params =
            [
                'domain' => BusinessUnit::getDomain($email->getBusinessUnit()->getCode()),
                'bu' => $email->getBusinessUnit()->getCode(),
                'MCHCodes' => $email->getBusinessUnit()->getMCHCodes(),
                'utm' => $email->getUtm(),
                'backgroundColour' => $email->getBackgroundColour(),
                'preHeader' => $email->getPreHeader(),
                'termsAndConditions' => $email->getTermsAndConditions(),
                'imagespath' => '/images/'. $email->getId(). self::DS
            ];

        // CDN PATH
        if ($this->getKernel()->getContainer()->getParameter('is_elab')) {
            $params['imagespath'] =  BusinessUnit::getDomain($email->getBusinessUnit()->getCode()).'/elab' .
                $this->getKernel()->getContainer()->getParameter('ftp_destination')  . self::DS . 'email_generator' . self::DS . $email->getId(). self::DS;
        }

        return $params;
    }
}
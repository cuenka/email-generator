<?php

namespace EmailGeneratorBundle\Helper;


use AuthenticationBundle\Entity\BusinessUnit;
use EmailGeneratorBundle\Entity\Template;
use EmailGeneratorBundle\Repository\TemplateRepository;

/**
 * Class TemplateHelper
 * @package EmailGeneratorBundle\Helper
 */
class TemplateHelper
{
    const MARIONNAUD_VIP = 'marionnaud-vip';
    const MARIONNAUD_STANDARD = 'marionnaud-standard';
    const MARIONNAUD_RESPONSIVE = 'marionnaud-responsive';
    const DROGAS_AND_WATSONS = 'drogas-watsons';
    const MARIONNAUD_RESPONSIVE_20 = 'marionnaud-responsive-20';

    /**
     * So if user is not Admin or SuperAdmin, templates are limited
     * @param $bu
     * @return array
     */
    public static function getTemplates($bu)
    {
        switch ($bu) {
            case BusinessUnit::DLV:
            case BusinessUnit::DLT:
            case BusinessUnit::WRU:
            case BusinessUnit::WUA:
                return [self::DROGAS_AND_WATSONS];
            default:
                return [self::MARIONNAUD_VIP, self::DROGAS_AND_WATSONS, self::MARIONNAUD_RESPONSIVE, self::MARIONNAUD_STANDARD, self::MARIONNAUD_RESPONSIVE_20];
        }
    }


    /**
     * @param Template $template
     * @return array
     */
    public static function getAvaiableBlocks(Template $template)
    {
        $blocks =
            [
                BlockHelper::BLOCK_HERO => 'Hero block',
                BlockHelper::BLOCK_GAP => 'Gap',
                BlockHelper::BLOCK_TEXT => 'Free HTML block',
                BlockHelper::BLOCK_HR => 'Horizontal line separator',
                BlockHelper::BLOCK_TITLE_FULL_LINE => 'Title with Full with underline',
                BlockHelper::BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_RIGHT => '2 blocks with text and cta on right',
                BlockHelper::BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_LEFT => '2 blocks with text and cta on left',
                BlockHelper::BLOCK_TWO_BLOCK_FULL_PRODUCTS => '2 full products in a row',
                BlockHelper::BLOCK_THREE_BLOCK_FULL_PRODUCTS => '3 full products in a row',
                BlockHelper::BLOCK_FOUR_BLOCK_FULL_PRODUCTS => '4 full products in a row',
                BlockHelper::BLOCK_FULL_PRODUCT_IMAGE_LEFT => 'Full product info with price and image on left',
                BlockHelper::BLOCK_FULL_PRODUCT_IMAGE_RIGHT => 'Full product info with price and image on right',

            ];

        switch ($template->getFilename()) {
            case self::MARIONNAUD_RESPONSIVE_20:
                $blocks =
                    [
                        BlockHelper::BLOCK_GAP => 'Gap',
                        BlockHelper::BLOCK_HEADER_OPTION_ONE  => 'Header option 1',
                        BlockHelper::BLOCK_HEADER_OPTION_TWO  => 'Header option 2',
                        BlockHelper::BLOCK_INTRO_TEXT_AND_CTA => 'Intro text and cta',
                        BlockHelper::BLOCK_FULL_WIDTH_IMAGE   => 'Full Width Image',
                        BlockHelper::BLOCK_PRODUCT_IMAGE_WITH_CTA_AND_IMAGE_LEFT => 'Product image with CTA and Image Left',
                        BlockHelper::BLOCK_PRODUCT_IMAGE_WITH_CTA_AND_IMAGE_RIGHT => 'Product image with CTA and Image right',
                        BlockHelper::BLOCK_MLIFE_IMAGE_LEFT => 'M-life',
                        BlockHelper::BLOCK_GWP_SECTION => 'GWP Section',
                        BlockHelper::BLOCK_THREE_TITLES_AND_IMAGES => '3x Titles and images',
                        BlockHelper::BLOCK_THREE_PRODUCT_GRID_WITH_TITLE => '3x Product Grid With Title',
                        BlockHelper::BLOCK_TWO_PRODUCT_GRID_WITH_COLOUR_BACKGROUND => '2x Product grid with Colour Background',
                        BlockHelper::BLOCK_TWO_OFFSET_TEXT => '2x Offset text, CTA and Image',
                        BlockHelper::BLOCK_THREE_OFFSET_TEXT => '3x Offset text, CTA and Image',
                    ];
                break;
            case self::MARIONNAUD_RESPONSIVE:
                $blocks[BlockHelper::BLOCK_MLIFE_IMAGE_RIGHT] = 'M-life block with custom image on right';
                $blocks[BlockHelper::BLOCK_TWO_BLOCK_ONLY_CTA] = '2 images in a row with only CTA';
                $blocks[BlockHelper::BLOCK_THREE_BLOCK_ONLY_CTA] = '3 images in a row with only CTA';
                $blocks[BlockHelper::BLOCK_THREE_BLOCK_ONLY_CTA] = '3 images in a row with only CTA';
                break;
            case self::DROGAS_AND_WATSONS:
                $blocks =
                    [
                        BlockHelper::BLOCK_GAP => 'Gap',
                        BlockHelper::BLOCK_FULL_WIDTH_IMAGE => 'Full width Image',
                        BlockHelper::BLOCK_TWO_IMAGES_WITH_CTA => '2 images with Cta\'s',
                        BlockHelper::BLOCK_THREE_IMAGES_WITH_CTA => '3 images with Cta\'s',
                        BlockHelper::BLOCK_CONTENT_IMAGE_LEFT => 'Content Block Image Left',
                        BlockHelper::BLOCK_CONTENT_IMAGE_RIGHT => 'Content Block Image right',
                        BlockHelper::BLOCK_FOUR_PRODUCTS_MEMBERS => '4 Products Members',
                        BlockHelper::BLOCK_FOUR_PRODUCTS => '4 Products',
                        BlockHelper::BLOCK_THREE_PRODUCTS_MEMBERS => '3 Products Members',
                        BlockHelper::BLOCK_THREE_PRODUCTS => '3 Products',
                    ];
            case self::MARIONNAUD_VIP:
            case self::MARIONNAUD_STANDARD:
            default:
        }

        return $blocks;
    }

}
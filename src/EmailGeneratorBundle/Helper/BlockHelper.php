<?php

namespace EmailGeneratorBundle\Helper;


use EmailGeneratorBundle\Entity\Block;
use EmailGeneratorBundle\Entity\BlockFourBlockFullProducts;
use EmailGeneratorBundle\Entity\BlockFullProductImage;
use EmailGeneratorBundle\Entity\BlockGap;
use EmailGeneratorBundle\Entity\BlockGwpSection;
use EmailGeneratorBundle\Entity\BlockHeaderOptionOne;
use EmailGeneratorBundle\Entity\BlockHero;
use EmailGeneratorBundle\Entity\BlockIntroTextAndCta;
use EmailGeneratorBundle\Entity\BlockLineSeparator;
use EmailGeneratorBundle\Entity\BlockMlife;
use EmailGeneratorBundle\Entity\BlockMlifeImageLeft;
use EmailGeneratorBundle\Entity\BlockProductImageWithctaAndImage;
use EmailGeneratorBundle\Entity\BlockSimple2BlockTextCta;
use EmailGeneratorBundle\Entity\BlockSimple2BlockTextCtaWithTitle;
use EmailGeneratorBundle\Entity\BlockText;
use EmailGeneratorBundle\Entity\BlockThreeBlockFullProducts;
use EmailGeneratorBundle\Entity\BlockThreeBlockOnlyCta;
use EmailGeneratorBundle\Entity\BlockThreeOffsetText;
use EmailGeneratorBundle\Entity\BlockThreeProductGridWithTitle;
use EmailGeneratorBundle\Entity\BlockTitleFullLine;
use EmailGeneratorBundle\Entity\BlockTwoBlockFullProducts;
use EmailGeneratorBundle\Entity\BlockTwoBlockOnlyCta;
use EmailGeneratorBundle\Entity\BlockTwoOffsetText;
use EmailGeneratorBundle\Entity\BlockTwoProductGridWithColourBackground;
use EmailGeneratorBundle\Form\BlockFourBlockFullProductsType;
use EmailGeneratorBundle\Form\BlockFullProductImageType;
use EmailGeneratorBundle\Form\BlockGapType;
use EmailGeneratorBundle\Form\BlockGwpSectionType;
use EmailGeneratorBundle\Form\BlockHeaderOptionOneType;
use EmailGeneratorBundle\Form\BlockHeroType;
use EmailGeneratorBundle\Form\BlockIntroTextAndCtaType;
use EmailGeneratorBundle\Form\BlockLineSeparatorType;
use EmailGeneratorBundle\Form\BlockMlifeImageLeftType;
use EmailGeneratorBundle\Form\BlockMlifeType;
use EmailGeneratorBundle\Form\BlockProductImageWithctaAndImageType;
use EmailGeneratorBundle\Form\BlockSimple2BlockTextCtaType;
use EmailGeneratorBundle\Form\BlockSimple2BlockTextCtaWithTitleType;
use EmailGeneratorBundle\Form\BlockTextType;
use EmailGeneratorBundle\Form\BlockThreeBlockFullProductsType;
use EmailGeneratorBundle\Form\BlockThreeBlockOnlyCtaType;
use EmailGeneratorBundle\Form\BlockThreeOffsetTextType;
use EmailGeneratorBundle\Form\BlockThreeProductGridWithTitleType;
use EmailGeneratorBundle\Form\BlockTitleFullLineType;
use EmailGeneratorBundle\Form\BlockTwoBlockFullProductsType;
use EmailGeneratorBundle\Form\BlockTwoBlockOnlyCtaType;
use EmailGeneratorBundle\Form\BlockTwoOffsetTextType;
use EmailGeneratorBundle\Form\BlockTwoProductGridWithColourBackgroundType;

/**
 * Class BlockHelper
 * @package EmailGeneratorBundle\Helper
 */
class BlockHelper
{
    const BLOCK_HERO = 'hero_banner';
    const BLOCK_HR = 'separator';
    const BLOCK_GAP = 'just_gap';
    const BLOCK_TEXT = 'just_text';
    const BLOCK_TITLE_FULL_LINE = 'title_with_full_line';
    const BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_RIGHT = 'simple_two_block_with_text_and_cta_right';
    const BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_LEFT = 'simple_two_block_with_text_and_cta_left';
    const BLOCK_TWO_BLOCK_FULL_PRODUCTS = 'two_block_full_products';
    const BLOCK_THREE_BLOCK_FULL_PRODUCTS = 'three_block_full_products';
    const BLOCK_FOUR_BLOCK_FULL_PRODUCTS = 'four_block_full_products';
    const BLOCK_FULL_PRODUCT_IMAGE_LEFT = 'full_product_image_left';
    const BLOCK_FULL_PRODUCT_IMAGE_RIGHT = 'full_product_image_right';
    const BLOCK_MLIFE_IMAGE_RIGHT = 'mlife_image_right';
    const BLOCK_MLIFE_IMAGE_LEFT = 'm-life';
    const BLOCK_TWO_BLOCK_ONLY_CTA = 'two_block_only_cta';
    const BLOCK_THREE_BLOCK_ONLY_CTA = 'three_block_only_cta';
    const BLOCK_FULL_WIDTH_IMAGE = 'full_width_image';
    const BLOCK_TWO_IMAGES_WITH_CTA = 'two_images_with_cta';
    const BLOCK_THREE_IMAGES_WITH_CTA = 'three_images_with_cta';
    const BLOCK_CONTENT_IMAGE_LEFT = 'content_image_left';
    const BLOCK_CONTENT_IMAGE_RIGHT = 'content_image_right';
    const BLOCK_FOUR_PRODUCTS_MEMBERS = 'four_products_members';
    const BLOCK_FOUR_PRODUCTS = 'four_products';
    const BLOCK_THREE_PRODUCTS_MEMBERS = 'three_products_members';
    const BLOCK_THREE_PRODUCTS = 'three_products';
    const BLOCK_HEADER_OPTION_ONE = 'header_option_one';
    const BLOCK_HEADER_OPTION_TWO = 'header_option_two';
    const BLOCK_INTRO_TEXT_AND_CTA = 'intro_text_and_cta';
    const BLOCK_PRODUCT_IMAGE_WITH_CTA_AND_IMAGE_LEFT = 'product_image_with_cta_and_image_left';
    const BLOCK_PRODUCT_IMAGE_WITH_CTA_AND_IMAGE_RIGHT = 'product_image_with_cta_and_image_right';
    const BLOCK_GWP_SECTION = 'gwp_section';
    const BLOCK_THREE_TITLES_AND_IMAGES = 'three_titles_and_images';
    const BLOCK_THREE_PRODUCT_GRID_WITH_TITLE = 'three_product_grid_with_title';
    const BLOCK_TWO_PRODUCT_GRID_WITH_COLOUR_BACKGROUND = 'two_product_grid_with_colour_background';
    const BLOCK_TWO_OFFSET_TEXT = 'two_offset_text';
    const BLOCK_THREE_OFFSET_TEXT = 'three_offset_text';

    /**
     * @param $block
     * @param integer $order
     * @return Block|BlockHero|BlockTitleFullLine|BlockThreeBlockFullProducts
     */
    public static function newInstance($block, $order = 1)
    {

        switch ($block) {
            case self::BLOCK_HERO:
            case self::BLOCK_FULL_WIDTH_IMAGE:
                $instance = new BlockHero($block, $order);
                break;
            case self::BLOCK_TEXT:
                $instance = new BlockText($block, $order);
                break;
            case self::BLOCK_TITLE_FULL_LINE:
                $instance = new BlockTitleFullLine($block, $order);
                break;
            case self::BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_RIGHT:
            case self::BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_LEFT:
                $instance = new BlockSimple2BlockTextCta($block, $order);
            break;
            case self::BLOCK_CONTENT_IMAGE_LEFT:
            case self::BLOCK_CONTENT_IMAGE_RIGHT:
                $instance = new BlockSimple2BlockTextCtaWithTitle($block, $order);
                break;
            case self::BLOCK_TWO_BLOCK_FULL_PRODUCTS:
                $instance = new BlockTwoBlockFullProducts($block, $order);
                break;
            case self::BLOCK_THREE_BLOCK_FULL_PRODUCTS:
            case self::BLOCK_THREE_PRODUCTS_MEMBERS:
            case self::BLOCK_THREE_PRODUCTS:
                $instance = new BlockThreeBlockFullProducts($block, $order);
                break;
            case self::BLOCK_FOUR_BLOCK_FULL_PRODUCTS:
            case self::BLOCK_FOUR_PRODUCTS_MEMBERS:
            case self::BLOCK_FOUR_PRODUCTS:
                $instance = new BlockFourBlockFullProducts($block, $order);
                break;
            case self::BLOCK_FULL_PRODUCT_IMAGE_RIGHT:
            case self::BLOCK_FULL_PRODUCT_IMAGE_LEFT:
                $instance = new BlockFullProductImage($block, $order);
                break;
            case self::BLOCK_HR:
                $instance = new BlockLineSeparator($block, $order);
                break;
            case self::BLOCK_GAP:
                $instance = new BlockGap($block, $order);
                break;
            case self::BLOCK_MLIFE_IMAGE_RIGHT:
                $instance = new BlockMlife($block, $order);
                break;
            case self::BLOCK_MLIFE_IMAGE_LEFT:
                $instance = new BlockMlifeImageLeft($block, $order);
                break;
            case self::BLOCK_TWO_BLOCK_ONLY_CTA:
            case self::BLOCK_TWO_IMAGES_WITH_CTA:
                $instance = new BlockTwoBlockOnlyCta($block, $order);
                break;
            case self::BLOCK_THREE_BLOCK_ONLY_CTA:
            case self::BLOCK_THREE_IMAGES_WITH_CTA:
            case self::BLOCK_THREE_TITLES_AND_IMAGES:
                $instance = new BlockThreeBlockOnlyCta($block, $order);
                break;
            case self::BLOCK_HEADER_OPTION_ONE:
            case self::BLOCK_HEADER_OPTION_TWO:
                $instance = new BlockHeaderOptionOne($block, $order);
                break;
            case self::BLOCK_INTRO_TEXT_AND_CTA:
                $instance = new BlockIntroTextAndCta($block, $order);
                break;
            case self::BLOCK_PRODUCT_IMAGE_WITH_CTA_AND_IMAGE_LEFT:
            case self::BLOCK_PRODUCT_IMAGE_WITH_CTA_AND_IMAGE_RIGHT:
                $instance = new BlockProductImageWithctaAndImage($block, $order);
                break;
            case self::BLOCK_GWP_SECTION:
                $instance = new BlockGwpSection($block, $order);
                break;
            case self::BLOCK_THREE_PRODUCT_GRID_WITH_TITLE:
                $instance = new BlockThreeProductGridWithTitle($block, $order);
                break;
            case self::BLOCK_TWO_PRODUCT_GRID_WITH_COLOUR_BACKGROUND:
                $instance = new BlockTwoProductGridWithColourBackground($block, $order);
                break;
            case self::BLOCK_TWO_OFFSET_TEXT:
                $instance = new BlockTwoOffsetText($block, $order);
                break;
            case self::BLOCK_THREE_OFFSET_TEXT:
                $instance = new BlockThreeOffsetText($block, $order);
                break;
            default:
                $instance = new Block($block, $order);
        }

        return $instance;
    }

    /**
     * @param string $code
     * @return string or null
     */
    public function getFormType(string $code)
    {
        switch ($code) {
            case self::BLOCK_TITLE_FULL_LINE:
                return BlockTitleFullLineType::class;
                break;
            case self::BLOCK_HERO:
            case self::BLOCK_FULL_WIDTH_IMAGE:
                return BlockHeroType::class;
                break;
            case self::BLOCK_TEXT:
                return BlockTextType::class;
                break;
            case self::BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_RIGHT:
            case self::BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_LEFT:
                return BlockSimple2BlockTextCtaType::class;
                break;
            case self::BLOCK_CONTENT_IMAGE_LEFT:
            case self::BLOCK_CONTENT_IMAGE_RIGHT:
                return BlockSimple2BlockTextCtaWithTitleType::class;
                break;
            case self::BLOCK_TWO_BLOCK_FULL_PRODUCTS:
                return BlockTwoBlockFullProductsType::class;
                break;
            case self::BLOCK_THREE_BLOCK_FULL_PRODUCTS:
            case self::BLOCK_THREE_PRODUCTS_MEMBERS:
            case self::BLOCK_THREE_PRODUCTS:
                return BlockThreeBlockFullProductsType::class;
                break;
            case self::BLOCK_FOUR_BLOCK_FULL_PRODUCTS:
            case self::BLOCK_FOUR_PRODUCTS_MEMBERS:
            case self::BLOCK_FOUR_PRODUCTS:
            return BlockFourBlockFullProductsType::class;
                break;
            case self::BLOCK_FULL_PRODUCT_IMAGE_RIGHT:
            case self::BLOCK_FULL_PRODUCT_IMAGE_LEFT:
                return BlockFullProductImageType::class;
                break;
            case self::BLOCK_GAP:
                return BlockGapType::class;
                break;
            case self::BLOCK_MLIFE_IMAGE_RIGHT:
                return BlockMlifeType::class;
                break;
            case self::BLOCK_MLIFE_IMAGE_LEFT:
                return BlockMlifeImageLeftType::class;
                break;
            case self::BLOCK_HR:
                return BlockLineSeparatorType::class;
                break;
            case self::BLOCK_TWO_BLOCK_ONLY_CTA:
            case self::BLOCK_TWO_IMAGES_WITH_CTA:
                return BlockTwoBlockOnlyCtaType::class;
                break;
            case self::BLOCK_THREE_BLOCK_ONLY_CTA:
            case self::BLOCK_THREE_IMAGES_WITH_CTA:
            case self::BLOCK_THREE_TITLES_AND_IMAGES:
                return BlockThreeBlockOnlyCtaType::class;
                break;
            case self::BLOCK_HEADER_OPTION_ONE:
            case self::BLOCK_HEADER_OPTION_TWO:
                return BlockHeaderOptionOneType::class;
                break;
            case self::BLOCK_INTRO_TEXT_AND_CTA:
                return BlockIntroTextAndCtaType::class;
                break;
            case self::BLOCK_PRODUCT_IMAGE_WITH_CTA_AND_IMAGE_LEFT:
            case self::BLOCK_PRODUCT_IMAGE_WITH_CTA_AND_IMAGE_RIGHT:
                return BlockProductImageWithctaAndImageType::class;
                break;
            case self::BLOCK_GWP_SECTION:
                return BlockGwpSectionType::class;
                break;
            case self::BLOCK_THREE_PRODUCT_GRID_WITH_TITLE:
                return BlockThreeProductGridWithTitleType::class;
                break;
            case self::BLOCK_TWO_PRODUCT_GRID_WITH_COLOUR_BACKGROUND:
                return BlockTwoProductGridWithColourBackgroundType::class;
                break;
            case self::BLOCK_TWO_OFFSET_TEXT:
                return BlockTwoOffsetTextType::class;
                break;
            case self::BLOCK_THREE_OFFSET_TEXT:
                return BlockThreeOffsetTextType::class;
                break;
            default:
                return null;
        }

    }

    /**
     * It return the array with width and height
     * @param string $code
     * @return array
     *
     */
    public function getCorrectImageDimensions($code)
    {
        $dimensions['width'] = null;
        $dimensions['height'] = null;
        switch ($code) {
            case self::BLOCK_HERO:
            case self::BLOCK_FULL_WIDTH_IMAGE:
                $dimensions['width'] = 640;
                $dimensions['height'] = 320;
                break;
            case self::BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_LEFT:
            case self::BLOCK_SIMPLE_2_BLOCK_TEXT_CTA_RIGHT:
            case self::BLOCK_CONTENT_IMAGE_LEFT:
            case self::BLOCK_CONTENT_IMAGE_RIGHT:
                $dimensions['width'] = 320;
                $dimensions['height'] = 320;
                break;
            case self::BLOCK_MLIFE_IMAGE_RIGHT:
                $dimensions['width'] = 320;
                $dimensions['height'] = 280;
                break;
            case self::BLOCK_TWO_BLOCK_ONLY_CTA:
            case self::BLOCK_TWO_IMAGES_WITH_CTA:
                $dimensions['width'] = 272;
                $dimensions['height'] = 291;
                break;
            case self::BLOCK_THREE_BLOCK_ONLY_CTA:
            case self::BLOCK_THREE_IMAGES_WITH_CTA:
                $dimensions['width'] = 170;
                $dimensions['height'] = 190;
                break;
        }

        return $dimensions;

    }

    public static function getFonts()
    {
        $fonts =
            [
                'Arial, Helvetica, sans-serif' => 'Arial, Helvetica, sans-serif',
                '"Times New Roman", Times, serif' => '"Times New Roman", Times, serif',
                '"Times New Roman", Times, serif in italic' => '"Times New Roman", Times, serif;font-style: italic;'
            ];

        return $fonts;
    }

    public static function getAlign()
    {
        $align =
            [
                'left' => 'left',
                'right' => 'right',
                'center' => 'center',
                'justify' => 'justify'
            ];

        return $align;
    }

    public static function getHeights()
    {
        $heights =
            [
                'Small  (10px)' => '10',
                'Medium (30px)' => '30',
                'Large  (50px)' => '50',
                'Extra large  (80px)' => '80',
            ];

        return $heights;
    }
}
<?php

namespace EmailGeneratorBundle\Form;

use AppBundle\Form\Type\SkuType;
use EmailGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockTwoBlockFullProductsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'product1',
            SkuType::class,
            array(
                'label' => 'Product SKU 1',
                'required' => true,
                'attr' => array(
                    'data-context' => 'product1',
                    'placeholder' => 'Product ID 1',
                    'data-block' => $options['dataBlock'],
                    'data-email' => $options['dataEmail']
                ),
            )
        )->add(
            'product1Brand',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1Brand',
                    'placeholder' => 'Product 1 brand',
                ),
            )
        )->add(
            'product1Name',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1Name',
                    'placeholder' => 'Product 1 Name',
                ),
            )
        )->add(
            'product1Image',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1Image',
                    'placeholder' => 'Product 1 image',
                ),
            )
        )->add(
            'product1Price',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1Price',
                    'placeholder' => 'Product 1 price',
                ),
            )
        )->add(
            'product1PromoPrice',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1PromoPrice',
                    'placeholder' => 'Product 1 promo price',
                ),
            )
        )->add(
            'product2',
            SkuType::class,
            array(
                'label' => 'Product SKU 2',
                'required' => true,
                'attr' => array(
                    'data-context' => 'product2',
                    'class' => 'mt-1 product2',
                    'placeholder' => 'Product ID 2',
                    'data-block' => $options['dataBlock'],
                    'data-email' => $options['dataEmail']
                ),
            )
        )->add(
            'product2Brand',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product2Brand',
                    'placeholder' => 'Product 2 brand',
                ),
            )
        )->add(
            'product2Name',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product2Name',
                    'placeholder' => 'Product 2 Name',
                ),
            )
        )->add(
            'product2Image',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product2Image',
                    'placeholder' => 'Product 2 image',
                ),
            )
        )->add(
            'product2Price',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product2Price',
                    'placeholder' => 'Product 2 price',
                ),
            )
        )->add(
            'product2PromoPrice',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product2PromoPrice',
                    'placeholder' => 'Product 2 promo price',
                ),
            )
        )->add(
            'priceColour',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'font colour of price Hex code',
                ),
            )
        )->add(
            'ctaText',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'ctaBackgroundColour',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'background Hex code',
                ),
            )
        )->add(
            'ctaTextColour',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'Cta Hex code',
                ),
            )
        )->add(
            'fontFamily',
            ChoiceType::class,
            array(
                'label' => 'Price font family',
                'choices'  => BlockHelper::getFonts(),
                'required' => true,
                'attr' => array(
                    'class' => 'form-control mt-1',
                ),
            )
        )->add(
            'save',
            SubmitType::class,
            [
                'label' => 'Save',
                'attr' =>
                    [
                        'class' => 'btn btn-success mt-1',
                    ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'EmailGeneratorBundle\Entity\BlockTwoBlockFullProducts',
                'dataBlock' => null,
                'dataEmail' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_BlockTwoBlockFullProducts';
    }


}

<?php

namespace EmailGeneratorBundle\Form;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use EmailGeneratorBundle\Entity\Template;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreHeaderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('preHeader', TextType::class, array(
            'required' => true,
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Type job title if you know it',
            )
        ))->add('save', SubmitType::class,
            array(
                'label' => 'Save',
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EmailGeneratorBundle\Entity\Email',
            'businessUnit' => null,
            'user' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_preheader';
    }


}

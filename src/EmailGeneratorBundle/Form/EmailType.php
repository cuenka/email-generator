<?php

namespace EmailGeneratorBundle\Form;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use EmailGeneratorBundle\Entity\Template;
use EmailGeneratorBundle\Helper\TemplateHelper;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $bu = $options['businessUnit']->getCode();
        $builder->add('backgroundColour',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'font colour of price Hex code',
                ),
            )
        )->add('jobTitle', TextType::class, array(
            'label' => 'Campaign name',
            'required' => true,
            'attr' => array(
                'class' => '',
                'placeholder' => 'Write your campaign name',
            )
        ))->add('dateRequest', DateType::class, array(
            'required' => true,
            'data' => new \DateTime(),
            'format' => 'dd-MM-yyyy',
            'widget' => 'single_text',
            // do not render as type="date", to avoid HTML5 date pickers
            'html5' => false,
            'attr' => array(
                'type' => 'date',
                'class' => 'disabled',
                'disabled' => 'disabled'
            )
        ))->add('dateDelivered', DateType::class, array(
            'required' => true,
            'widget' => 'single_text',
            'format' => 'dd-MM-yyyy',
            // do not render as type="date", to avoid HTML5 date pickers
            'html5' => false,
            'attr' => array(
                'type' => 'date',
                'class' => 'js-datepicker',
                'placeholder' => 'When needs to be deliver?'
            )
        ))->add('dateGoLive', DateType::class, array(
            'required' => true,
            'widget' => 'single_text',
            'format' => 'dd-MM-yyyy',
            // do not render as type="date", to avoid HTML5 date pickers
            'html5' => false,
            'attr' => array(
                'type' => 'date',
                'class' => 'js-datepicker',
                'placeholder' => 'When is going to be send?',

            )
        ))->add('objective', TextType::class, array(
            'required' => false,
            'attr' => array(
                'class' => '',
                'placeholder' => 'Purpose of this project',
            )
        ))->add('utm', TextType::class, array(
            'required' => true,
            'attr' => array(
                'class' => '',
                'placeholder' => 'URI utm of this email',
            )
        ))->add('description', TextareaType::class, array(
            'required' => false,
            'attr' => array(
                'class' => '',
                'placeholder' => 'What is this email about?',
            )
        ))->add('subject', TextType::class, array(
            'required' => false,
            'attr' => array(
                'class' => '',
                'placeholder' => 'Email subject',
            )
        ));
        if ($bu == BusinessUnit::ASW) {
            $builder->add('businessUnit', EntityType::class,
                [
                    'required' => true,
                    'class' => 'AuthenticationBundle:BusinessUnit',
                    'choice_label' => function (BusinessUnit $entity = null) {
                        return $entity ? $entity->getNameAndBu() : '';
                    },
                    'choice_value' => function (BusinessUnit $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr' => array(
                        'class' => ''
                    )
                ]);
            $builder->add('user', EntityType::class,
                [
                    'class' => 'AuthenticationBundle:User',
                    'choice_label' => function (User $entity = null) {
                        return $entity ? $entity->getName() : '';
                    },
                    'choice_value' => function (User $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'data' => $options['user'],
                    'attr' =>
                        [
                            'class' => 'disabled',
                            'disabled' => 'disabled'
                        ]
                ]);
        }

        $builder->add('template', EntityType::class,
            [
                'required' => true,
                'class' => 'EmailGeneratorBundle:Template',
                'query_builder' => function (EntityRepository $er) use($bu) {
                    switch ($bu) {
                        case BusinessUnit::DLV:
                        case BusinessUnit::DLT:
                        case BusinessUnit::WRU:
                        case BusinessUnit::WUA:
                            return $er->createQueryBuilder('t')
                                ->where("t.filename = '".TemplateHelper::DROGAS_AND_WATSONS."'");
                            break;
                        default:
                            return $er->createQueryBuilder('t');

                    }},
            'attr' => array(
        'class' => ''
    )
        ])->add('preHeader', TextType::class, array(
        'required' => true,
        'attr' => array(
            'class' => '',
            'placeholder' => 'Text that will show up on email clients such us gmail, outlook...',
        )
    ))->add('termsAndConditions', CKEditorType::class, array(
        'required' => false,
        'config_name' => 'my_config',
        'autoload' => true,
        'attr' => array(
            'class' => '',
            'placeholder' => 'This is optional, It will show on the footer of the email',

        )
    ))->add('saveAndReturn', SubmitType::class,
        array(
            'label' => 'Save and return',
            'attr' => array(
                'class' => 'btn btn-success mt-3'
            )
        ))->add('saveAndGoToBuild', SubmitType::class,
        array(
            'label' => 'Save go to Edit Layout',
            'attr' => array(
                'class' => 'btn btn-success mt-3'
            )
        ))->add('saveAndGoToEdit', SubmitType::class,
        array(
            'label' => 'Save go to Edit content',
            'attr' => array(
                'class' => 'btn btn-success mt-3'
            )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EmailGeneratorBundle\Entity\Email',
            'businessUnit' => null,
            'user' => null,
            'templates' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_email';
    }


}

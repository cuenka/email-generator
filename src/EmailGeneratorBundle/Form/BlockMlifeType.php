<?php

namespace EmailGeneratorBundle\Form;

use EmailGeneratorBundle\Entity\BlockMlife;
use EmailGeneratorBundle\Helper\BlockHelper;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockMlifeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', FileType::class, array(
            'required' => true,
            'label' => 'Image',
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt1',
            TextType::class,
            array(
                'label' => 'Alt text',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of image',
                ),
            )
        )->add(
                'backgroundColourBlock',
                TextType::class,
                array(
                    'label' => 'Background colour Block',
                    'required' => true,
                    'attr' => array(
                        'class' => 'mt-1 pickAColor',
                        'placeholder' => 'Alt text of image',
                    ),
                )
            )->add('text', CKEditorType::class, array(
            'required' => true,
            'config_name' => 'my_config',
            'attr' => array(
                'class' => 'form-control mt-1 limit-text-max',
                'data-length' => 50,
                'placeholder' => 'Block text',
                'id' => $this->GetId()
            )
        ))->add(
            'ctaText',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => 'form-control mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add('ctaUrl', UrlType::class, array(
            'required' => true,
            'label' => 'URL',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Cta and image link',
            )
        ))->add(
            'ctaBackgroundColour',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'background Hex code',
                ),
            )
        )->add(
            'ctaTextColour',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'Cta Hex code',
                ),
            )
        )->add(
            'ctaUrlTitle',
            TextType::class,
            array(
                'label' => 'Cta title',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Title text for CTA',
                ),
            )
        )->add(
            'save',
            SubmitType::class,
            [
                'label' => 'Save',
                'attr' =>
                    [
                        'class' => 'btn btn-success mt-1',
                    ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => BlockMlife::class,
                'dataBlock' => null,
                'dataEmail' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function GetId()
    {
        return $this->getBlockPrefix(). '_text_'. random_int(10,9999);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_BlockMlife';
    }


}

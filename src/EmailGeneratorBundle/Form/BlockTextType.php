<?php

namespace EmailGeneratorBundle\Form;

use EmailGeneratorBundle\Helper\BlockHelper;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockTextType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', CKEditorType::class, array(
            'required' => true,
            'config_name' => 'my_config',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block text',
                'id' => $this->GetId()
            )
        ))->add('backgroundColourBlock', TextType::class, array(
            'required' => false,
            'attr' => array(
                'label' => 'Block background colour',
                'class' => 'mt-1 pickAColor',
                'placeholder' => 'background colour, only allow Hex code',
            )
        ))->add(
            'ctaTextColour',
            TextType::class,
            array(
                'label' => 'Text Colour',
                'required' => true,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'Cta Hex code',
                ),
            )
        )->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' =>
                        [
                            'class' => 'btn btn-success mt-1'
                        ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'EmailGeneratorBundle\Entity\BlockText',
                'dataBlock' => null,
                'dataEmail' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function GetId()
    {
        return $this->getBlockPrefix(). '_text_'. random_int(10,9999);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_BlockText';
    }


}

<?php

namespace EmailGeneratorBundle\Form;

use AppBundle\Form\Type\SkuType;
use EmailGeneratorBundle\Helper\BlockHelper;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockFullProductImageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'product1',
            SkuType::class,
            array(
                'label' => 'Product SKU',
                'required' => true,
                'attr' => array(
                    'data-context' => 'product1',
                    'placeholder' => 'Product ID 1',
                    'data-block' => $options['dataBlock'],
                    'data-email' => $options['dataEmail']
                ),
            )
        )->add(
            'product1Brand',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1Brand',
                    'placeholder' => 'Product 1 brand',
                ),
            )
        )->add(
            'product1Name',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1Name',
                    'placeholder' => 'Product 1 Name',
                ),
            )
        )->add(
            'product1Image',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1Image',
                    'placeholder' => 'Product 1 image',
                ),
            )
        )->add(
            'product1Price',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1Price',
                    'placeholder' => 'Product 1 price',
                ),
            )
        )->add(
            'product1PromoPrice',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 product1PromoPrice',
                    'placeholder' => 'Product 1 promo price',
                ),
            )
        )->add(
            'productExtraInformation',
            TextType::class,
            array(
                'label' => 'Product extra information (price per ML)',
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 ',
                    'placeholder' => 'Product 1 extra information',
                ),
            )
        )->add('text', CKEditorType::class, array(
            'required' => true,
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Block text',
                'id' => $this->GetId()
            )
        ))->add(
            'ctaText',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => 'form-control mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'ctaBackgroundColour',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'background Hex code',
                ),
            )
        )->add(
            'ctaTextColour',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'Cta Hex code',
                ),
            )
        )->add(
            'ctaUrlTitle',
            TextType::class,
            array(
                'label' => 'CTA title',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Title text for CTA',
                ),
            )
        )->add(
            'priceColour',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'font colour of price Hex code',
                ),
            )
        )->add(
            'fontFamily',
            ChoiceType::class,
            array(
                'label' => 'Price font family',
                'choices'  => BlockHelper::getFonts(),
                'required' => true,
                'attr' => array(
                    'class' => 'form-control mt-1',
                ),
            )
        )->add(
            'save',
            SubmitType::class,
            [
                'label' => 'Save',
                'attr' =>
                    [
                        'class' => 'btn btn-success mt-1',
                    ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'EmailGeneratorBundle\Entity\BlockFullProductImage',
                'dataBlock' => null,
                'dataEmail' => null

            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_BlockFullProductImage';
    }


    /**
     * {@inheritdoc}
     */
    public function GetId()
    {
        return $this->getBlockPrefix(). '_text_'. random_int(10,9999);
    }

}

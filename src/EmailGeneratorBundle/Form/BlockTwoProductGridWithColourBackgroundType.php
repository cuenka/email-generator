<?php

namespace EmailGeneratorBundle\Form;

use EmailGeneratorBundle\Entity\BlockTwoProductGridWithColourBackground;
use EmailGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockTwoProductGridWithColourBackgroundType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, array(
            'required' => true,
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Block title',
            )
        ))->add('file', FileType::class, array(
            'required' => true,
            'label' => 'Left image to upload',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt1',
            TextType::class,
            array(
                'label' => 'Alt text of left image',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of  image',
                ),
            )
        )->add(
            'ctaUrl',
            UrlType::class,
            array(
                'required' => true,
                'label' => 'URL of left section',
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'product1Title',
            TextType::class,
            array(
                'required' => true,
                'label' => 'Left product title',
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Left product title',
                ),
            )
        )->add(
            'product1Description',
            TextType::class,
            array(
                'required' => true,
                'label' => 'Left product description',
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta description',
                ),
            )
        )->add(
            'product1Label',
            TextType::class,
            array(
                'required' => true,
                'label' => 'Left cta label',
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add('file2', FileType::class, array(
            'required' => true,
            'label' => 'Image to upload',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt2',
            TextType::class,
            array(
                'label' => 'Alt text image 2',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of  image',
                ),
            )
        )->add(
            'ctaUrl2',
            UrlType::class,
            array(
                'required' => true,
                'label' => 'URL of Right section',
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'product2Title',
            TextType::class,
            array(
                'required' => true,
                'label' => 'Right product title',
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Left product title',
                ),
            )
        )->add(
            'product2Description',
            TextType::class,
            array(
                'required' => true,
                'label' => 'Right product description',
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta description',
                ),
            )
        )->add(
            'product2Label',
            TextType::class,
            array(
                'required' => true,
                'label' => 'Right cta label',
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add('backgroundColourBlock', TextType::class, array(
            'required' => false,
            'label' => 'Block Background colour',
            'attr' => array(
                'class' => 'mt-1 pickAColor',
                'value' => '#f9f8f8',
                'placeholder' => 'background colour of block, only allow Hex code',
            )
        ))->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' =>
                        [
                            'class' => 'btn btn-success mt-1'
                        ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => BlockTwoProductGridWithColourBackground::class,
                'dataBlock' => null,
                'dataEmail' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_BlockTwoProductGridWithColourBackground';
    }


}

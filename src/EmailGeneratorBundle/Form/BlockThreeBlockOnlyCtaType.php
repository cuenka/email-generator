<?php

namespace EmailGeneratorBundle\Form;

use EmailGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockThreeBlockOnlyCtaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', FileType::class, array(
            'required' => true,
            'label' => 'Image to upload',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt1',
            TextType::class,
            array(
                'label' => 'Alt text or Title',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text or title of image',
                ),
            )
        )->add(
            'ctaUrl',
            UrlType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add('file2', FileType::class, array(
            'required' => true,
            'label' => 'Image to upload',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt2',
            TextType::class,
            array(
                'label' => 'Alt text image 2 or Title 2',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text or title of image',
                ),
            )
        )->add(
            'ctaUrl2',
            UrlType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add('file3', FileType::class, array(
            'required' => true,
            'label' => 'Image 3 to upload',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Upload image 3',
            )
        ))->add(
            'imageAlt3',
            TextType::class,
            array(
                'label' => 'Alt text image 3  or Title 3',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text or title of image',
                ),
            )
        )->add(
            'ctaUrl3',
            UrlType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'ctaText',
            TextType::class,
            array(
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'ctaUrlTitle',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Common URL title',
                ),
            )
        )->add(
            'ctaBackgroundColour',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'background Hex code',
                ),
            )
        )->add(
            'ctaTextColour',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => ' mt-1 pickAColor',
                    'placeholder' => 'Cta Hex code',
                ),
            )
        )->add(
            'save',
            SubmitType::class,
            [
                'label' => 'Save',
                'attr' =>
                    [
                        'class' => 'btn btn-success mt-1',
                    ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'EmailGeneratorBundle\Entity\BlockThreeBlockOnlyCta',
                'dataBlock' => null,
                'dataEmail' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_BlockThreeBlockOnlyCta';
    }


}

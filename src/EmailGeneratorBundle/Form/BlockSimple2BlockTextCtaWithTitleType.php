<?php

namespace EmailGeneratorBundle\Form;

use AppBundle\Form\Type\SwitchType;
use EmailGeneratorBundle\Helper\BlockHelper;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockSimple2BlockTextCtaWithTitleType extends AbstractType
{
    protected $blockPrefix = 'emailgeneratorbundle_BlockSimple2BlockTextCtawithTitle';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $targetClass = 'ctaInputs2Block'. random_int(10,9999);

        $builder->add('title', TextType::class, array(
            'required' => true,
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Block title',
            )
        ))->add('file', FileType::class, array(
            'required' => true,
            'label' => 'Image',
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt1',
            TextType::class,
            array(
                'label' => 'Alt text',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of image',
                ),
            )
        )->add(
            'alignBlock',
            ChoiceType::class,
            array(
                'choices' => BlockHelper::getAlign(),
                'required' => false,
                'label' => 'Text and cta alignment',
                'placeholder' => 'Option chosen will override alignment',
                'attr' => array(
                    'class' => 'mt-1',
                ),
            )
        )->add('backgroundColourBlock', TextType::class, array(
            'required' => false,
            'label' => 'Block Background colour',
            'attr' => array(
                'class' => 'mt-1 pickAColor',
                'placeholder' => 'background colour of block, only allow Hex code',
            )
        ))->add('text', TextareaType::class, array(
            'required' => true,
//            'config_name' => 'my_config',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block text',
                'id' => $this->GetId()
            )
        ))->add(
            'needCTa',
            SwitchType::class,
            array(
                'required' => true,
                'label' => 'Do you need a cta?',
                'choices'  => array(
                    'OFF' => 0,
                    'ON' => 1,
                ),
                'expanded' => true,
                'multiple' => false,
                'attr' => array(
                    'class' => ' mt-1',
                    'id' => 'ctaInputs2Block'.random_int(10,9999),
                    'targetClass' => '.'.$targetClass,
                ),
            )
        )->add(
            'ctaText',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => "mt-1 ". $targetClass,
                    'placeholder' => 'Cta text',
                ),
            )
        )->add('ctaUrl', UrlType::class, array(
            'required' => false,
            'label' => 'URL',
            'attr' => array(
                'class' => "mt-1 ". $targetClass,
                'placeholder' => 'Cta and image link',
            )
        ))->add(
            'ctaUrlTitle',
            TextType::class,
            array(
                'label' => 'Cta title',
                'required' => false,
                'attr' => array(
                    'class' => "mt-1 ". $targetClass,
                    'placeholder' => 'Title text for CTA',
                ),
            )
        )->add(
            'ctaBackgroundColour',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => " mt-1 pickAColor ". $targetClass,
                    'placeholder' => 'background Hex code',
                ),
            )
        )->add(
            'ctaTextColour',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => " mt-1 pickAColor ". $targetClass,
                    'placeholder' => 'Cta Hex code',
                ),
            )
        )->add(
            'save',
            SubmitType::class,
            [
                'label' => 'Save',
                'attr' =>
                    [
                        'class' => 'btn btn-success mt-1',
                    ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'EmailGeneratorBundle\Entity\BlockSimple2BlockTextCtaWithTitle',
                'dataBlock' => null,
                'dataEmail' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function GetId()
    {
        return $this->getBlockPrefix(). '_text_'. random_int(10,9999);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_BlockSimple2BlockTextCtaWithTitle';
    }


}

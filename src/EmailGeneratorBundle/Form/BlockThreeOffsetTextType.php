<?php

namespace EmailGeneratorBundle\Form;

use AppBundle\Form\Type\SwitchType;
use EmailGeneratorBundle\Entity\BlockThreeOffsetText;
use EmailGeneratorBundle\Helper\BlockHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockThreeOffsetTextType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $target1Class = 'ctaInputs2Block1'. random_int(10,9999);
        $target2Class = 'ctaInputs2Block2'. random_int(10,9999);
        $target3Class = 'ctaInputs2Block3'. random_int(10,9999);

        $builder->add('text', TextareaType::class, array(
            'required' => false,
            'label' => 'Free html top left',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block text left',
                'id' => $this->GetId()
            )
        ))->add('text2', TextareaType::class, array(
            'required' => false,
            'label' => 'Free html middle right',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block text right',
                'id' => $this->GetId()
            )
        ))->add('text3', TextareaType::class, array(
            'required' => false,
            'label' => 'Free html bottom left',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block text bottom left',
                'id' => $this->GetId()
            )
        ))->add('file', FileType::class, array(
            'required' => true,
            'label' => 'Image to upload (top right)',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt1',
            TextType::class,
            array(
                'label' => 'Alt text for top right image',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of  image',
                ),
            )
        )->add('file2', FileType::class, array(
            'required' => true,
            'label' => 'Image to upload (middle left)',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt2',
            TextType::class,
            array(
                'label' => 'Alt text image for middle left',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of  image',
                ),
            )
        )->add('file3', FileType::class, array(
            'required' => true,
            'label' => 'Image to upload (bottom right)',
            'attr' => array(
                'class' => 'mt-1',
                'placeholder' => 'Block title',
            )
        ))->add(
            'imageAlt3',
            TextType::class,
            array(
                'label' => 'Alt text image for bottom right',
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'placeholder' => 'Alt text of  image',
                ),
            )
        )->add(
            'needCTa1',
            SwitchType::class,
            array(
                'required' => true,
                'label' => 'Do you need a cta on top left?',
                'choices'  => array(
                    'OFF' => 0,
                    'ON' => 1,
                ),
                'expanded' => true,
                'multiple' => false,
                'attr' => array(
                    'class' => ' mt-1',
                    'id' => 'ctaInputs2Block'.random_int(10,9999),
                    'targetClass' => '.'.$target1Class,
                ),
            )
        )->add(
            'cta1Url',
            UrlType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 '. $target1Class,
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'cta1Label',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 '. $target1Class,
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'needCTa2',
            SwitchType::class,
            array(
                'required' => true,
                'label' => 'Do you need a cta on middle right?',
                'choices'  => array(
                    'OFF' => 0,
                    'ON' => 1,
                ),
                'expanded' => true,
                'multiple' => false,
                'attr' => array(
                    'class' => ' mt-1',
                    'id' => 'ctaInputs2Block'.random_int(10,9999),
                    'targetClass' => '.'.$target2Class,
                ),
            )
        )->add(
            'cta2Url',
            UrlType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 '. $target2Class,
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'cta2Label',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 '. $target2Class,
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'needCTa3',
            SwitchType::class,
            array(
                'required' => true,
                'label' => 'Do you need a cta on bottom left?',
                'choices'  => array(
                    'OFF' => 0,
                    'ON' => 1,
                ),
                'expanded' => true,
                'multiple' => false,
                'attr' => array(
                    'class' => ' mt-1',
                    'id' => 'ctaInputs2Block'.random_int(10,9999),
                    'targetClass' => '.'.$target3Class,
                ),
            )
        )->add(
            'cta3Url',
            UrlType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 '. $target3Class,
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
            'cta3Label',
            TextType::class,
            array(
                'required' => false,
                'attr' => array(
                    'class' => 'mt-1 '. $target3Class,
                    'placeholder' => 'Cta text',
                ),
            )
        )->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' =>
                        [
                            'class' => 'btn btn-success mt-1'
                        ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => BlockThreeOffsetText::class,
                'dataBlock' => null,
                'dataEmail' => null
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function GetId()
    {
        return $this->getBlockPrefix(). '_text_'. random_int(10,9999);
    }
    /**
     * {@inheritdoc}
     */

    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_BlockThreeOffsetText';
    }


}

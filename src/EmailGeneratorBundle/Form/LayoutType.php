<?php

namespace EmailGeneratorBundle\Form;

use AuthenticationBundle\Entity\BusinessUnit;
use EmailGeneratorBundle\Entity\Template;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LayoutType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('businessUnit', EntityType::class,
                [
                    'required' => true,
                    'class' => 'AuthenticationBundle:BusinessUnit',
                    'choice_label' => function (BusinessUnit $entity = null) {
                        return $entity ? $entity->getName() : '';
                    },
                    'choice_value' => function (BusinessUnit $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr' => array(
                        'class' => 'form-control mt-1'
                    )
                ])
            ->add('template', EntityType::class,
                [
                    'required' => true,
                    'class' => 'EmailGeneratorBundle:Template',
                    'choice_label' => function (Template $entity = null) {
                        return $entity ? $entity->getName() : '';
                    },
                    'choice_value' => function (Template $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr' => array(
                        'class' => 'form-control mt-1'
                    )
                ])
            ->add('header', TextareaType::class, array(
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'rows' => '12',
                    'placeholder' => 'header'
                )
            ))
            ->add('footer', TextareaType::class, array(
                'required' => true,
                'attr' => array(
                    'class' => 'mt-1',
                    'rows' => '12',
                    'placeholder' => 'header'
                )
            ))
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' =>
                        [
                            'class' => 'btn btn-success mt-1',
                        ],
                ]
            );
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EmailGeneratorBundle\Entity\Layout'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emailgeneratorbundle_layout';
    }


}

<?php

namespace AuthenticationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="authentication_users", indexes={@ORM\Index(name="search_idx", columns={"id"})})
 * @ORM\Entity(repositoryClass="AuthenticationBundle\Repository\UserRepository")
 * @UniqueEntity(fields="username", message="Username is already taken.")
 * @UniqueEntity(fields="email", message="Email is already taken.")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=false, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\Valid()
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=false)
     * @Assert\Valid()
     */
    private $email;

    /**
     * @ORM\Column(type="text", unique=false, nullable=true)
     * @Assert\Valid()
     */
    private $extraEmails;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = array();

    /**
     * Many users have 1 Business Unit
     * @ORM\ManyToOne(targetEntity="AuthenticationBundle\Entity\BusinessUnit")
     * @ORM\JoinColumn(name="businessUnit", referencedColumnName="id")
     */
    private $businessUnit;


    public function __construct()
    {
        $this->isActive = false;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }


    public function eraseCredentials()
    {
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }



    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password
        ));
    }

    /**
     * @see \Serializable::unserialize()\
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password
            ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }


    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getRoles()
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;

        // allows for chaining
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBusinessUnit()
    {
        return $this->businessUnit;
    }

    /**
     * @param mixed $businessUnit
     */
    public function setBusinessUnit($businessUnit)
    {
        $this->businessUnit = $businessUnit;
    }

    /**
     * @return mixed
     */
    public function getExtraEmails()
    {
        return $this->extraEmails;
    }

    /**
     * @param mixed $extraEmails
     */
    public function setExtraEmails($extraEmails)
    {
        $this->extraEmails = $extraEmails;
    }

    /**
     * @TODO This is hardcoded and nasty, improve this in the future
     * @return array
     */
    public function getAllRoles()
    {
        return ["ROLE_USER","ROLE_SUPERUSER","ROLE_ADMIN","ROLE_SUPERADMIN"];

    }


}

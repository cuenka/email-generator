<?php

namespace AuthenticationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * self
 *
 * @ORM\Table(name="authentication_business_unit", indexes={@ORM\Index(name="search_idx", columns={"id"})})
 * @ORM\Entity(repositoryClass="AuthenticationBundle\Repository\BusinessUnitRepository")
 * @UniqueEntity(fields="code", message="Code is already taken.")

 */
class BusinessUnit
{
    CONST ASW = 'ASW';
    CONST MCH_DE = 'MCH-DE';
    CONST MCH_FR = 'MCH-FR';
    CONST MCH_IT = 'MCH-IT';
    CONST MES = 'MES';
    CONST MIT = 'MIT';
    CONST MSK = 'MSK';
    CONST MRO = 'MRO';
    CONST MFR = 'MFR';
    CONST MAT = 'MAT';
    CONST MCZ = 'MCZ';
    CONST TPSUK = 'TPS-UK';
    CONST TPSROI = 'TPS-ROI';
    CONST WRU = 'WRU';
    CONST WUA = 'WUA';
    CONST DLT = 'DLT';
    CONST DLV = 'DLV';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40)
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=15, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="AuthenticationBundle\Entity\Language")
     * @ORM\JoinColumn(name="language", referencedColumnName="id")
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity="AuthenticationBundle\Entity\Country")
     * @ORM\JoinColumn(name="country", referencedColumnName="id")
     */
    private $country;

    /**
     * One Business Unit has Many Users.
     * @ORM\OneToMany(targetEntity="AuthenticationBundle\Entity\User", mappedBy="id")
     */
    private $user;

    /**
     * self constructor.
     */
    public function __construct()
    {
        $this->user = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param Language $language
     */
    public function setLanguage(Language $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param ArrayCollection $user
     */
    public function setUser(ArrayCollection $users)
    {
        $this->user = $users;

        return $this;
    }

    /**
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    public function getNameAndBu()
    {
        return $this->getName().' | '.$this->getCode();
    }

    /**
     * It returns the BU which allows access to more elements
     * @return string
     */
    public function getAdminBU()
    {
        return self::ASW;
    }
    public function hasLargeCurrency()
    {
        $largeCurrency = ['MRO','MCH'];

        return in_array($this->getCode(), $largeCurrency);
    }

    public static function getDomain($bu)
    {
        switch ($bu) {
            case (self::MAT):
                $url = "https://www.marionnaud.at";
                break;
            case (self::MFR):
                $url = "https://www.marionnaud.fr";
                break;
            case (self::MES):
                $url = "https://www.marionnaud.es";
                break;
            case (self::MRO):
                $url = "https://www.marionnaud.ro";
                break;
            case (self::MIT):
                $url = "https://www.marionnaud.it";
                break;
            case (self::MCH_FR):
            case (self::MCH_IT):
            case (self::MCH_DE):
                $url = "https://www.marionnaud.ch";
                break;
            case (self::MSK):
                $url = "https://www.marionnaud.sk";
                break;
            case (self::TPSUK):
            case (self::TPSROI):
                $url = "https://www.theperfumeshop.com";
                break;
            case (self::MCZ):
                $url = "https://www.marionnaud.cz";
                break;
            case (self::WRU):
                $url = "https://www.watsons.com.ru";
                break;
            case (self::WUA):
                $url = "https://www.watsons.ua";
                break;
            case (self::DLV):
                $url = "https://www.drogas.lv";
                break;
            case (self::DLT):
                $url = "https://www.drogas.lt";
                break;
        }
        return $url;
    }

    public static function amendURL($id, $bu)
    {
        switch ($bu) {
            case (self::MAT):
                $url = "https://www.marionnaud.at/p/" . $id;
                break;
            case (self::MES):
                $url = "https://www.marionnaud.es/p/" . $id;
                break;
            case (self::MRO):
                $url = "https://www.marionnaud.ro/p/" . $id;
                break;
            case (self::MIT):
                $url = "https://www.marionnaud.it/p/" . $id;
                break;
            case (self::MCH_DE):
                $url = "https://www.marionnaud.ch/de/p/" . $id;
                break;
            case (self::MCH_FR):
                $url = "https://www.marionnaud.ch/fr/p/" . $id;
                break;
            case (self::MCH_IT):
                $url = "https://www.marionnaud.ch/fr/p/" . $id; // Italy MCH retrieve french products
                break;
            case (self::MSK):
                $url = "https://www.marionnaud.sk/p/" . $id;
                break;
            case (self::TPSUK):
                $url = "https://www.theperfumeshop.com/p/" . $id;
                break;
            case (self::TPSROI):
                $url = "https://www.theperfumeshop.com/ie/p/" . $id;
                break;
            case (self::WRU):
                $url = "https://www.watsons.com.ru/p/" . $id;
                break;
            case (self::WUA):
                $url = "https://www.watsons.ua/p/" . $id;
                break;
            case (self::DLV):
                $url = "https://www.drogas.lv/p/" . $id;
                break;
            case (self::DLT):
                $url = "https://www.drogas.lt/p/" . $id;
                break;
        }

        return $url;
    }

    public function getMCHCodes()
    {
        return
            [
                self::MCH_DE,
                self::MCH_FR,
                self::MCH_IT
            ];
    }
}


<?php

namespace AuthenticationBundle\Controller;

use AuthenticationBundle\AuthenticationBundle;
use AuthenticationBundle\Entity\User;
use AuthenticationBundle\Form\PasswordType;
use AuthenticationBundle\Form\UserDataType;
use AuthenticationBundle\Form\UserPasswordType;
use AuthenticationBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AuthenticationBundle:User')->getInfoForTable();

        $actions = $this->getActions();

        return $this->render('AuthenticationBundle:User:index.html.twig', array(
            'data' => $users,
            'actions' => $actions,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }
        $actions = $this->getActions();

        return $this->render('AuthenticationBundle:User:new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
            'actions' => $actions
        ));
    }


    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        // Own user can update their own details
        if (($user->getId() !== $this->getUser()->getId())
            && (!in_array('ROLE_ADMIN', $this->getUser()->getRoles()))) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        }

        $editForm = $this->createForm(UserType::class, $user);
        $editForm->handleRequest($request);
        // If new password was not inserted ignore
        $post = $request->request->get('authenticationbundle_user');

        if ($editForm->isSubmitted() && $editForm->isValid()) {
                $passwordEncoder = $this->get('security.password_encoder');
                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('info', 'User updated successfully!');

            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }
        $actions = $this->getActions();

        return $this->render('AuthenticationBundle:User:edit.html.twig', array(
            'user' => $user,
            'form' => $editForm->createView(),
            'actions' => $actions,
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/user-password", name="user_edit_password")
     * @Method({"GET", "POST"})
     */
    public function passwordAction(Request $request, User $user)
    {
        // Own user can update their own details
        if (($user->getId() !== $this->getUser()->getId())
            && (!in_array('ROLE_ADMIN', $this->getUser()->getRoles()))) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        }

        $editForm = $this->createForm(UserPasswordType::class, $user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $passwordEncoder = $this->get('security.password_encoder');
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('info', 'User updated successfully!');

            return $this->redirectToRoute('user_index');
        }
        $actions = $this->getActions();

        return $this->render('AuthenticationBundle:User:edit.html.twig', array(
            'user' => $user,
            'form' => $editForm->createView(),
            'actions' => $actions,
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/user-information", name="user_edit_information")
     * @Method({"GET", "POST"})
     */
    public function editInformationAction(Request $request, User $user)
    {
        // Own user can update their own details
        if (($user->getId() !== $this->getUser()->getId())
            && (!in_array('ROLE_ADMIN', $this->getUser()->getRoles()))) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        }

        $editForm = $this->createForm(UserDataType::class, $user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('info', 'User updated successfully!');

            return $this->redirectToRoute('user_index');
        }
        $actions = $this->getActions();

        return $this->render('AuthenticationBundle:User:edit.html.twig', array(
            'user' => $user,
            'form' => $editForm->createView(),
            'actions' => $actions,
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('user_index');
    }


    /**
     * This function is normally used to get the endpoints for actions for table display
     * @return array
     */
    private function getActions()
    {
        $actions =
            [
                'list' =>
                    [
                        'path' => 'user_index',
                        'name' => 'Check users',
                        'btn' => 'primary',
                    ],
                'edit' =>
                    [
                        'path' => 'user_edit',
                        'name' => 'Edit user',
                        'btn' => 'info',
                    ],
                'password' =>
                    [
                        'path' => 'user_edit_password',
                        'name' => 'Update user password',
                        'btn' => 'warning',
                    ],
                'info' =>
                    [
                        'path' => 'user_edit_information',
                        'name' => 'Update user information',
                        'btn' => 'info',
                    ],
                'delete' =>
                    [
                        'path' => 'user_delete',
                        'name' => 'Delete user',
                        'btn' => 'danger',
                    ]
            ];

        return $actions;

    }
}

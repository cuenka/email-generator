<?php

namespace AuthenticationBundle\Controller;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Form\BusinessUnitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Businessunit controller.
 *
 * @Route("business-unit")
 */
class BusinessUnitController extends Controller
{
    /**
     * Lists all businessUnit entities.
     *
     * @Route("/", name="business-unit_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        $em = $this->getDoctrine()->getManager();

        $businessUnits = $em->getRepository('AuthenticationBundle:BusinessUnit')->getInfoForTable();

        $actions = $this->getActions();

        return $this->render('AuthenticationBundle:BusinessUnit:index.html.twig', array(
            'data' => $businessUnits,
            'actions' => $actions,
        ));
    }

    /**
     * Creates a new businessUnit entity.
     *
     * @Route("/new", name="business-unit_new")
     * @Method({"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        $businessUnit = new BusinessUnit();
        $form = $this->createForm(BusinessUnitType::class, $businessUnit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $businessUnit->addUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($businessUnit);
            $em->flush();
            $this->addFlash('success', '<strong>Success!</strong> New Business Unit created');
            return $this->redirectToRoute('business-unit_edit', array('id' => $businessUnit->getId()));
        }
        $actions = $this->getActions();

        return $this->render('AuthenticationBundle:BusinessUnit:new.html.twig', array(
            'businessUnit' => $businessUnit,
            'form' => $form->createView(),
            'actions' => $actions,
        ));
    }


    /**
     * Displays a form to edit an existing businessUnit entity.
     *
     * @Route("/{id}/edit", name="business-unit_edit")
     * @Method({"GET","POST"})
     */
    public function editAction(Request $request, BusinessUnit $businessUnit)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        $editForm = $this->createForm(BusinessUnitType::class, $businessUnit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('business-unit_edit', array('id' => $businessUnit->getId()));
        }

        $actions = $this->getActions();

        return $this->render('AuthenticationBundle:BusinessUnit:edit.html.twig', array(
            'businessUnit' => $businessUnit,
            'form' => $editForm->createView(),
            'actions' => $actions,
        ));
    }

    /**
     * Deletes a businessUnit entity.
     *
     * @Route("/{id}", name="business-unit_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, BusinessUnit $businessUnit)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        $em = $this->getDoctrine()->getManager();
        $em->remove($businessUnit);
        $em->flush();

        return $this->redirectToRoute('business-unit_index');
    }

    /**
     * This function is normally used to get the endpoints for actions for table display
     * @return array
     */
    private function getActions()
    {
        $actions =
            [
                'list' =>
                    [
                        'path' => 'business-unit_index',
                        'name' => 'Check business units',
                        'btn' => 'primary',
                    ],
                'edit' =>
                    [
                        'path' => 'business-unit_edit',
                        'name' => 'Edit business unit',
                        'btn' => 'info',
                    ],
                'delete' =>
                    [
                        'path' => 'business-unit_delete',
                        'name' => 'Delete business unit',
                        'btn' => 'danger',
                    ]
            ];

        return $actions;

    }

}

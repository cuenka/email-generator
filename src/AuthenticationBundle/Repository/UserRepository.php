<?php

namespace AuthenticationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class UserRepository extends EntityRepository
{
    CONST LIMIT = 100;

    /* 
     * Retrieving data for tables
     */
    public function getInfoForTable()
    {
        return $this->createQueryBuilder('u')
            ->select('u.id, u.name, u.username, bu.code as BU')
            ->leftJoin('AuthenticationBundle\Entity\BusinessUnit','bu', \Doctrine\ORM\Query\Expr\Join::WITH, 'u.businessUnit = bu.id')
            ->setMaxResults( self::LIMIT )
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
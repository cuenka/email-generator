<?php

namespace AuthenticationBundle\Form;

use AuthenticationBundle\Entity\Country;
use AuthenticationBundle\Entity\Language;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BusinessUnitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',TextType::class,
            array (
                'required' => true,
                'attr' => array(
                    'class' => 'form-control mt-2',
                    'placeholder' => 'Business unit name',
                )
            )
        )->add('code',TextType::class,
            array (
                'required' => true,
                'attr' => array (
                    'class' => 'form-control mt-2',
                    'placeholder' => 'Business unit code',
                )
            )
        )->add('country', EntityType::class,
            array (
                'required'   => true,
                'class' => 'AuthenticationBundle:Country',
                'choice_label' => function (Country $entity = null) {
                    return $entity ? $entity->getName() : '';
                },
                'choice_value' => function (Country $entity = null) {
                    return $entity ? $entity->getId() : '';
                },
                'attr' => array (
                    'class' => 'form-control mt-2'
                )
            )
        )->add('language', EntityType::class,
            array (
                'required'   => true,
                'class' => 'AuthenticationBundle:Language',
                'choice_label' => function (Language $entity = null) {
                    return $entity ? $entity->getName() : '';
                },
                'choice_value' => function (Language $entity = null) {
                    return $entity ? $entity->getId() : '';
                },
                'attr' => array (
                    'class' => 'form-control mt-2'
                )
            )
        )->add('save',SubmitType::class,
            array(
                'label' => 'Save',
                'attr' => array (
                    'class' => 'btn btn-success'
                )
            )
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AuthenticationBundle\Entity\BusinessUnit'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'projectbundle_businessunit';
    }


}

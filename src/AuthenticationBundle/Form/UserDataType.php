<?php

namespace AuthenticationBundle\Form;

use AuthenticationBundle\Entity\BusinessUnit;
use AuthenticationBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserDataType extends AbstractType
{
    /**
     * @TODO REFACTOR Consistancy on Array definitions, must me [] instead of array()
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array(
            'required' => true,
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Full user name',
            )
        ))->add('username', TextType::class, array(
            'required' => true,
            'attr' => array(
                'class' => 'form-control mt-1',
                'placeholder' => 'Username',
            )
        ))
            ->add('email', EmailType::class, array(
                'required' => true,
                'attr' => array(
                    'class' => 'form-control mt-1',
                    'placeholder' => 'User company email',
                )
            ))

            ->add('isActive', ChoiceType::class,
                [
                    'required' => true,
                    'label' => 'Activate extra emails functionality',
                    'choices' => ["NO" => 0, "YES" => 1],
                    'attr' => array(
                        'class' => 'form-control mt-1',
                    )
                ])
            ->add('extraEmails', TextareaType::class, array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control mt-1',
                    'placeholder' => 'Extra emails separated by comma and no space',
                )
            ))
            ->add('roles', ChoiceType::class,
                [
                    'required' => true,
                    'choices' => [
                        "ROLE USER" => "ROLE_USER",
                        "ROLE SUPERUSER" => "ROLE_SUPERUSER",
                        "ROLE ADMIN" => "ROLE_ADMIN",
                        "ROLE SUPER ADMIN" => "ROLE_SUPERADMIN"
                    ],
                    'multiple' => true,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'form-control mt-1',
                    )
                ])
            ->add('businessUnit', EntityType::class,
                [
                    'required' => true,
                    'class' => 'AuthenticationBundle:BusinessUnit',
                    'choice_label' => function (BusinessUnit $entity = null) {
                        return $entity ? $entity->getName() : '';
                    },
                    'choice_value' => function (BusinessUnit $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr' => array(
                        'class' => 'form-control mt-1'
                    )
                ])
            ->add('save', SubmitType::class,
                array(
                    'label' => 'Save',
                    'attr' => array(
                        'class' => 'btn btn-success'
                    )
                ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AuthenticationBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'authenticationbundle_user';
    }


}
